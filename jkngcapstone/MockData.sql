INSERT INTO Category (CategoryDescription)
VALUES ('Wine'), ('Beer'), ('Whiskey'), ('Vodka')

INSERT INTO [Status] (StatusDescription)
VALUES ('IN-PROGRESS'), ('SUBMITTED'),('PUBLISHED'), ('INACTIVE')

INSERT INTO Tag (TagDescription)
VALUES ('Cabernet'), ('IPA'), ('Merlot')

INSERT INTO [User] (UserName)
VALUES ('Sarah D'), ('Greg'), ('Jake'), ('Nate'), ('Kari')

INSERT INTO Post (PostTitle, PostBody, DateCreated, DatePublished, IsStatic, UserID, PostStatusID)
VALUES ('First Post', 'Lorizzle ipsizzle dolizzle sit shizzlin dizzle, im in the shizzle adipiscing daahng dawg. Nullam sapizzle velizzle, check out this volutpizzle, suscipit quizzle, gravida vel, arcu. Pellentesque gangster tortizzle. Fo shizzle erizzle. Fusce izzle dolizzle away shiznit tempizzle thats the shizzle. Maurizzle pellentesque nibh et turpizzle. Vestibulum in fizzle.', 
'04/20/15', '04/21/15', 0, 3, 3),
('Second Post', 'Lorizzle ipsizzle dolizzle sit shizzlin dizzle, im in the shizzle adipiscing daahng dawg. Nullam sapizzle velizzle, check out this volutpizzle, suscipit quizzle, gravida vel, arcu. Pellentesque gangster tortizzle. Fo shizzle erizzle. Fusce izzle dolizzle away shiznit tempizzle thats the shizzle. Maurizzle pellentesque nibh et turpizzle. Vestibulum in fizzle.', 
'04/25/15', '04/28/15', 0, 1, 3),
('Third Post', 'Lorizzle ipsizzle dolizzle sit shizzlin dizzle, im in the shizzle adipiscing daahng dawg. Nullam sapizzle velizzle, check out this volutpizzle, suscipit quizzle, gravida vel, arcu. Pellentesque gangster tortizzle. Fo shizzle erizzle. Fusce izzle dolizzle away shiznit tempizzle thats the shizzle. Maurizzle pellentesque nibh et turpizzle. Vestibulum in fizzle.', 
'04/20/15', '04/29/15', 0, 3, 3),
('Fourth Post', 'Lorizzle ipsizzle dolizzle sit shizzlin dizzle, im in the shizzle adipiscing daahng dawg. Nullam sapizzle velizzle, check out this volutpizzle, suscipit quizzle, gravida vel, arcu. Pellentesque gangster tortizzle. Fo shizzle erizzle. Fusce izzle dolizzle away shiznit tempizzle thats the shizzle. Maurizzle pellentesque nibh et turpizzle. Vestibulum in fizzle.', 
'04/25/15', '04/30/15', 0, 1, 3),
('Fifth Post', 'Lorizzle ipsizzle dolizzle sit shizzlin dizzle, im in the shizzle adipiscing daahng dawg. Nullam sapizzle velizzle, check out this volutpizzle, suscipit quizzle, gravida vel, arcu. Pellentesque gangster tortizzle. Fo shizzle erizzle. Fusce izzle dolizzle away shiznit tempizzle thats the shizzle. Maurizzle pellentesque nibh et turpizzle. Vestibulum in fizzle.', 
'04/20/15', '05/01/15', 0, 3, 3),
('Sixth Post', 'Lorizzle ipsizzle dolizzle sit shizzlin dizzle, im in the shizzle adipiscing daahng dawg. Nullam sapizzle velizzle, check out this volutpizzle, suscipit quizzle, gravida vel, arcu. Pellentesque gangster tortizzle. Fo shizzle erizzle. Fusce izzle dolizzle away shiznit tempizzle thats the shizzle. Maurizzle pellentesque nibh et turpizzle. Vestibulum in fizzle.', 
'04/25/15', '05/02/15', 0, 1, 3)

INSERT INTO PostTag (PostID, PostTagID) VALUES(1, 1), (2, 2), (3, 1), (4, 2), (5, 1), (6, 3)

INSERT INTO PostCategory (PostID, CategoryID) VALUES(1, 1), (2, 2), (3, 1), (4, 2), (5, 1), (6, 3)