USE [aspnet-JKNGCapstone.UI-20160422091724]
GO

/****** Object:  StoredProcedure [dbo].[DeleteSinglePost]    Script Date: 4/23/2016 6:11:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DeleteSinglePost]
(
	@PostID int
) AS

DELETE FROM Post
WHERE PostID = @PostID

GO


