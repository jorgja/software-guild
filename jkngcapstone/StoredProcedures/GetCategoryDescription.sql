USE [aspnet-JKNGCapstone.UI-20160422091724]
GO

/****** Object:  StoredProcedure [dbo].[GetCategoryDescription]    Script Date: 4/23/2016 6:12:40 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetCategoryDescription]
(
	@PostCategory int
) AS

SELECT CategoryID, CategoryDescription
FROM Category
WHERE CategoryID = @PostCategory
GO


