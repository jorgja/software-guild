USE [aspnet-JKNGCapstone.UI-20160422091724]
GO

/****** Object:  StoredProcedure [dbo].[GetAllPosts]    Script Date: 4/23/2016 2:30:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAllPosts] AS

SELECT PostID, PostTitle, PostBody, DateCreated, DatePublished, IsStatic, UserId, PostStatusId, 
PostCategory, PostTagId, Name, s.[Description], CategoryDescription, t.[Description]
FROM Post
	LEFT JOIN Author
ON Post.UserId = Author.AuthorID
	LEFT JOIN [Status] as s
ON Post.PostStatusId = s.StatusID
	LEFT JOIN Category
ON Post.PostCategory = Category.CategoryID
	LEFT JOIN Tag as t
ON Post.PostTagId = t.TagID
GO


