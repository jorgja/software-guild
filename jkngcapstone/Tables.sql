CREATE TABLE Tag(
	[PostTagID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[TagDescription] [varchar](30) NOT NULL
	)

	CREATE TABLE [Status](
	[PostStatusID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[StatusDescription] [varchar](20) NULL
	)

	CREATE TABLE PostCategory(
	[PostID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL
	)

	CREATE TABLE PostTag(
	[PostID] [int] NOT NULL,
	[PostTagID] [int] NOT NULL
	)

	CREATE TABLE Category(
	[CategoryID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[CategoryDescription] [varchar](30) NOT NULL
	)

	CREATE TABLE [User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL
	)

	CREATE TABLE Post(
	[PostID] [int] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[PostTitle] [varchar](50) NOT NULL,
	[PostBody] [varchar](500) NOT NULL,
	[DateCreated] [date] NOT NULL,
	[DatePublished] [date] NULL,
	[IsStatic] [bit] NOT NULL,
	[UserID] [int] NOT NULL,
	[PostStatusID] [int] NOT NULL
	)