﻿using Dapper;
using JKNGCapstone.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace JKNGCapstone.Data
{
    public class BlogRepository : IBlogRepository
    {

        public void DeletePost(int id)
        {
            int PostId = id;
            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                sqlConnection.Execute("DeleteSinglePost", new { PostId }, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeleteStaticPage(int id)
        {
            int PostId = id;
            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                sqlConnection.Execute("DeleteSingleStaticPage", new { PostId }, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeactivatePage(int id)
        {
            int PostId = id;
            DateTime DatePublished = DateTime.Now;

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                sqlConnection.Execute("DeactivatePage", new { PostId, DatePublished }, commandType: CommandType.StoredProcedure);
            }
        }

        public void PublishPost(int id)
        {
            int PostId = id;
            DateTime DatePublished = DateTime.Now.Date;

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                sqlConnection.Execute("PublishPost", new { PostId, DatePublished }, commandType: CommandType.StoredProcedure);
            }
        }

        public Post GetStaticPageById(int id)
        {
            int PostId = id;
            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                var result = sqlConnection.Query("GetStaticPageById", new { PostId }, commandType: CommandType.StoredProcedure).Single();

                Post post = new Post
                {
                    PostId = result.PostId,
                    PostTitle = result.PostTitle,
                    PostBody = result.PostBody,
                    DateCreated = result.DateCreated,
                    DatePublished = result.DatePublished,
                    IsStatic = result.IsStatic,
                    PostStatus = new PostStatus
                    {
                        StatusDescription = result.StatusDescription
                    },
                    User = new User
                    {
                        UserName = result.UserName
                    },
                };
                return post;
            }

        }

        public void EditPost(Post post)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                const string postQuery = "UPDATE Post SET PostTitle = @postTitle, PostBody = @postBody, PostStatusId = 1 WHERE PostID = @postID";
                sqlConnection.Execute(postQuery, new { postBody = post.PostBody, postTitle = post.PostTitle, post.PostId });

                const string removeCategories = "DELETE FROM PostCategory WHERE PostID = @postID";
                sqlConnection.Execute(removeCategories, new { postID = post.PostId });

                const string removeTags = "DELETE FROM PostTag WHERE PostID = @postID";
                sqlConnection.Execute(removeTags, new { postID = post.PostId });

                foreach (var id in post.TagIds)
                {
                    const string postTagQuery = "INSERT INTO PostTag (PostID, PostTagID) VALUES (@postID, @postTagId)";
                    sqlConnection.Execute(postTagQuery, new { postID = post.PostId, postTagId = id });
                }

                foreach (var id in post.CategoryIds)
                {
                    const string categoryQuery = "INSERT INTO PostCategory (PostID, CategoryID) VALUES (@postID, @CategoryId)";
                    sqlConnection.Execute(categoryQuery, new { postID = post.PostId, CategoryId = id });
                }
            }
        }

        public void EditStatic(Post post)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                const string postQuery = "UPDATE Post SET PostTitle = @postTitle, PostBody = @postBody WHERE PostID = @postID";
                sqlConnection.Execute(postQuery, new { postBody = post.PostBody, postTitle = post.PostTitle, post.PostId });
            }
        }

        public List<User> GetAllUsers()
        {
            List<User> users = new List<User>();

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();

                cmd.CommandText = "SELECT * FROM AspNetUsers";
                cmd.Connection = sqlConnection;

                sqlConnection.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        User user = new User
                        {
                            UserId = dr["Id"].ToString(),
                            UserName = dr["UserName"].ToString(),
                            Role = dr["Email"].ToString()
                        };
                        users.Add(user);
                    }
                }
            }
            return users;
        }

        public List<Post> GetAllActiveStaticPages()
        {
            List<Post> posts = new List<Post>();

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT Post.PostID, PostTitle, PostBody, DateCreated, DatePublished, IsStatic, [User].UserId, s.PostStatusId, StaticPage.StaticPageId, StaticPageDescription, " +
                                  "UserName, s.StatusDescription FROM Post LEFT JOIN[User] ON Post.UserId = [User].UserID " +
                                  "LEFT JOIN[Status] as s ON Post.PostStatusId = s.PostStatusID LEFT JOIN StaticPage ON Post.StaticPageId = StaticPage.StaticPageId " +
                                  "WHERE IsStatic = 1 AND Post.PostStatusId = 3";

                cmd.Connection = sqlConnection;
                sqlConnection.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        if (posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).Count() > 0)
                        {
                            var post = posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).FirstOrDefault();
                        }
                        else
                        {
                            Post post = new Post
                            {
                                PostId = int.Parse(dr["PostID"].ToString()),
                                PostTitle = dr["PostTitle"].ToString(),
                                PostBody = dr["PostBody"].ToString(),
                                DateCreated = DateTime.Parse(dr["DateCreated"].ToString()),
                                DatePublished = DateTime.Parse(dr["DatePublished"].ToString()),
                                IsStatic = Convert.ToBoolean(dr["IsStatic"].ToString()),
                                PostStatus = new PostStatus
                                {
                                    StatusDescription = dr["StatusDescription"].ToString()
                                },
                                User = new User
                                {
                                    UserName = dr["UserName"].ToString()
                                },
                            };
                            posts.Add(post);
                        }
                    }
                }
            }
            return posts;
        }

        public List<Post> GetAllStaticPages()
        {
            List<Post> posts = new List<Post>();

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT Post.PostID, PostTitle, PostBody, DateCreated, DatePublished, IsStatic, [User].UserId, s.PostStatusId, StaticPage.StaticPageId, StaticPageDescription, " +
                                  "UserName, s.StatusDescription FROM Post LEFT JOIN[User] ON Post.UserId = [User].UserID " +
                                  "LEFT JOIN[Status] as s ON Post.PostStatusId = s.PostStatusID LEFT JOIN StaticPage ON Post.StaticPageId = StaticPage.StaticPageId " +
                                  "WHERE IsStatic = 1 and post.PostStatusId != 5";

                cmd.Connection = sqlConnection;
                sqlConnection.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        if (posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).Count() > 0)
                        {
                            var post = posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).FirstOrDefault();
                        }
                        else
                        {
                            Post post = new Post
                            {
                                PostId = int.Parse(dr["PostID"].ToString()),
                                PostTitle = dr["PostTitle"].ToString(),
                                PostBody = dr["PostBody"].ToString(),
                                DateCreated = DateTime.Parse(dr["DateCreated"].ToString()),
                                DatePublished = DateTime.Parse(dr["DatePublished"].ToString()),
                                IsStatic = Convert.ToBoolean(dr["IsStatic"].ToString()),
                                PostStatus = new PostStatus
                                {
                                    StatusDescription = dr["StatusDescription"].ToString()
                                },
                                User = new User
                                {
                                    UserName = dr["UserName"].ToString()
                                },                                
                            };
                            posts.Add(post);
                        }
                    }
                }
            }
            return posts;
        }

        public List<Post> GetAllPostsNotPublished()
        {
            List<Post> posts = new List<Post>();

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT Post.PostID, PostTitle, PostBody, DateCreated, DatePublished, IsStatic, [User].UserId, Post.PostStatusId, " +
                                  "Tag.PostTagId, UserName, s.StatusDescription, CategoryDescription, tag.TagDescription FROM Post LEFT JOIN[User] ON Post.UserId = [User].UserID " +
                                  "LEFT JOIN[Status] as s ON Post.PostStatusId = s.PostStatusID LEFT JOIN PostCategory ON Post.PostID = PostCategory.PostID " +
                                  "INNER JOIN Category ON  PostCategory.CategoryID = Category.CategoryID LEFT JOIN PostTag ON Post.PostID = PostTag.PostID " +
                                  "INNER JOIN Tag ON PostTag.PostTagID = Tag.PostTagID " +
                                  "WHERE IsStatic = 0 and post.PostStatusId != 5";

                cmd.Connection = sqlConnection;
                sqlConnection.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        if (posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).Count() > 0)
                        {
                            var post = posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).FirstOrDefault();

                            if (post.PostCategoryDescription.Where(c => c.CategoryDescription == dr["CategoryDescription"].ToString()).Count() == 0)
                            {
                                var category = new Category
                                {
                                    CategoryDescription = dr["CategoryDescription"].ToString()
                                };
                                post.PostCategoryDescription.Add(category);
                            }

                            if (post.PostTag.Where(t => t.TagDescription == dr["TagDescription"].ToString()).Count() == 0)
                            {
                                var tag = new PostTag
                                {
                                    TagDescription = dr["TagDescription"].ToString()
                                };
                                post.PostTag.Add(tag);
                            }
                        }
                        else
                        {
                            Post post = new Post
                            {
                                PostId = int.Parse(dr["PostID"].ToString()),
                                PostTitle = dr["PostTitle"].ToString(),
                                PostBody = dr["PostBody"].ToString(),
                                DateCreated = DateTime.Parse(dr["DateCreated"].ToString()),
                                //DatePublished = DateTime.Parse(dr["DatePublished"].ToString()),
                                IsStatic = Convert.ToBoolean(dr["IsStatic"].ToString()),
                                PostStatus = new PostStatus
                                {
                                    StatusDescription = dr["StatusDescription"].ToString()
                                },
                                User = new User
                                {
                                    UserName = dr["UserName"].ToString()
                                },
                                PostCategoryDescription = new List<Category>
                                {
                                    new Category
                                    {
                                        CategoryDescription = dr["CategoryDescription"].ToString()
                                    }
                                },
                                PostTag = new List<PostTag>
                                {
                                    new PostTag
                                    {
                                    TagDescription = dr["TagDescription"].ToString()
                                    }
                                }
                            };

                            if (!string.IsNullOrEmpty(dr["DatePublished"].ToString()))
                            {
                                post.DatePublished = DateTime.Parse(dr["DatePublished"].ToString());
                            }


                            posts.Add(post);
                        }
                    }
                }
            }
            return posts;
        }
        public List<Post> GetAllPosts()
        {
            List<Post> posts = new List<Post>();

            DateTime DateCreated = DateTime.Now.Date;

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT Post.PostID, PostTitle, PostBody, DateCreated, DatePublished, IsStatic, [User].UserId, Post.PostStatusId, " +
                                  "Tag.PostTagId, UserName, s.StatusDescription, CategoryDescription, tag.TagDescription FROM Post LEFT JOIN[User] ON Post.UserId = [User].UserID " +
                                  "LEFT JOIN[Status] as s ON Post.PostStatusId = s.PostStatusID LEFT JOIN PostCategory ON Post.PostID = PostCategory.PostID " +
                                  "INNER JOIN Category ON  PostCategory.CategoryID = Category.CategoryID LEFT JOIN PostTag ON Post.PostID = PostTag.PostID " +
                                  "INNER JOIN Tag ON PostTag.PostTagID = Tag.PostTagID " +
                                  "WHERE IsStatic = 0 and post.PostStatusId != 5 AND Post.PostStatusId = 3";

                cmd.Connection = sqlConnection;
                sqlConnection.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        if (posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).Count() > 0)
                        {
                            var post = posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).FirstOrDefault();

                            if (post.PostCategoryDescription.Where(c => c.CategoryDescription == dr["CategoryDescription"].ToString()).Count() == 0)
                            {
                                var category = new Category
                                {
                                    CategoryDescription = dr["CategoryDescription"].ToString()
                                };
                                post.PostCategoryDescription.Add(category);
                            }

                            if (post.PostTag .Where(t => t.TagDescription == dr["TagDescription"].ToString()).Count() == 0)
                            {
                                var tag = new PostTag
                                {
                                    TagDescription = dr["TagDescription"].ToString()
                                };
                                post.PostTag.Add(tag);
                            }
                        }
                        else
                        {
                            Post post = new Post
                            {
                                PostId = int.Parse(dr["PostID"].ToString()),
                                PostTitle = dr["PostTitle"].ToString(),
                                PostBody = dr["PostBody"].ToString(),
                                DateCreated = DateTime.Parse(dr["DateCreated"].ToString()),
                                //DatePublished = DateTime.Parse(dr["DatePublished"].ToString()),
                                IsStatic = Convert.ToBoolean(dr["IsStatic"].ToString()),
                                PostStatus = new PostStatus
                                {
                                    StatusDescription = dr["StatusDescription"].ToString()
                                },
                                User = new User
                                {
                                    UserName = dr["UserName"].ToString()
                                },
                                PostCategoryDescription = new List<Category>
                                {
                                    new Category
                                    {
                                        CategoryDescription = dr["CategoryDescription"].ToString()
                                    }
                                },
                                PostTag = new List<PostTag>
                                {
                                    new PostTag
                                    {
                                    TagDescription = dr["TagDescription"].ToString()
                                    }
                                }
                            };

                            if (!string.IsNullOrEmpty(dr["DatePublished"].ToString()))
                            {
                                //post.DatePublished = DateTime.Parse(dr["DatePublished"].ToString());
                                post.DatePublished = DateCreated;
                            }


                            posts.Add(post);
                        }
                    }
                }
            }
            return posts;
        }

        public Post GetPostByPostId(int id)
        {
           List<Post> posts = new List<Post>();

            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT Post.PostID, PostTitle, PostBody, DateCreated, DatePublished, IsStatic, [User].UserId, Post.PostStatusId, " +
                                  "Tag.PostTagId, UserName, s.StatusDescription, CategoryDescription, tag.TagDescription FROM Post LEFT JOIN[User] ON Post.UserId = [User].UserID " +
                                  "LEFT JOIN[Status] as s ON Post.PostStatusId = s.PostStatusID LEFT JOIN PostCategory ON Post.PostID = PostCategory.PostID " +
                                  "INNER JOIN Category ON  PostCategory.CategoryID = Category.CategoryID LEFT JOIN PostTag ON Post.PostID = PostTag.PostID " +
                                  "INNER JOIN Tag ON PostTag.PostTagID = Tag.PostTagID " + 
                                  "WHERE Post.PostID = " + id;

                cmd.Connection = sqlConnection;
                sqlConnection.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        if (posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).Count() > 0)
                        {
                            var post = posts.Where(p => p.PostId == int.Parse(dr["PostID"].ToString())).FirstOrDefault();

                            if (post.PostCategoryDescription.Where(c => c.CategoryDescription == dr["CategoryDescription"].ToString()).Count() == 0)
                            {
                                var category = new Category
                                {
                                    CategoryDescription = dr["CategoryDescription"].ToString()
                                };
                                post.PostCategoryDescription.Add(category);
                            }

                            if (post.PostTag.Where(t => t.TagDescription == dr["TagDescription"].ToString()).Count() == 0)
                            {
                                var tag = new PostTag
                                {
                                    TagDescription = dr["TagDescription"].ToString()
                                };
                                post.PostTag.Add(tag);
                            }
                        }
                        else
                        {
                            Post post = new Post
                            {
                                PostId = int.Parse(dr["PostID"].ToString()),
                                PostTitle = dr["PostTitle"].ToString(),
                                PostBody = dr["PostBody"].ToString(),
                                DateCreated = DateTime.Parse(dr["DateCreated"].ToString()),
                                IsStatic = Convert.ToBoolean(dr["IsStatic"].ToString()),
                                PostStatus = new PostStatus
                                {
                                    StatusDescription = dr["StatusDescription"].ToString()
                                },
                                User = new User
                                {
                                    UserName = dr["UserName"].ToString()
                                },
                                PostCategoryDescription = new List<Category>
                                {
                                    new Category
                                    {
                                        CategoryDescription = dr["CategoryDescription"].ToString()
                                    }
                                },
                                PostTag = new List<PostTag>
                                {
                                    new PostTag
                                    {
                                    TagDescription = dr["TagDescription"].ToString()
                                    }
                                }
                            };

                            if (!string.IsNullOrEmpty(dr["DatePublished"].ToString()))
                            {
                                post.DatePublished = DateTime.Parse(dr["DatePublished"].ToString());
                            }

                            posts.Add(post);
                        }
                    }
                }
            }
            return posts.FirstOrDefault();
        }

        public List<Post> GetPostsByPostCategory(string category)
        {
            throw new NotImplementedException();
        }

        public List<Post> GetPostsByPostTag(string tag)
        {
            throw new NotImplementedException();
        }

        public void Insert(Post post)
        {
            //TODO: Stored Procs
            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                const string postQuery = "INSERT INTO Post (PostTitle, PostBody, DateCreated, DatePublished, IsStatic, UserId, PostStatusId) VALUES (@postTitle, @postBody, @DateCreated, @DatePublished, 0, 1, 1)";
                sqlConnection.Execute(postQuery, new { postBody = post.PostBody, postTitle = post.PostTitle, DateCreated = DateTime.Now.ToString(), DatePublished = DateTime.Now.ToString() });

                var newPost = sqlConnection.Query<Post>("SELECT PostID, PostTitle, PostBody, DateCreated, DatePublished, IsStatic, UserId, PostStatusId FROM Post").Last();

                if (post.PostCategoryDescription[0].CategoryDescription != null )
                {
                    string [] categories = SplitUserString(post.PostCategoryDescription[0].CategoryDescription);

                    foreach (var category in categories)
                    {
                        const string newCatQuery = "INSERT INTO Category (CategoryDescription) VALUES (@newCatDescription)";
                        sqlConnection.Execute(newCatQuery, new { newCatDescription = category });
                        Category newCat = sqlConnection.Query<Category>("SELECT CategoryID, CategoryDescription FROM Category").Last();
                        const string postNewCategoryQuery = "INSERT INTO PostCategory (PostID, CategoryID) VALUES (@PostID, @CategoryID)";
                        sqlConnection.Execute(postNewCategoryQuery, new { PostID = newPost.PostId, CategoryID = newCat.CategoryID });
                    }
                }

                if (post.PostTag[0].TagDescription != null)
                {
                    string[] tags = SplitUserString(post.PostTag[0].TagDescription);

                    foreach (var tag in tags)
                    {
                        const string newTagQuery = "INSERT INTO Tag (TagDescription) VALUES (@newTagDescription)";
                        sqlConnection.Execute(newTagQuery, new { newTagDescription = tag});
                        PostTag newTag = sqlConnection.Query<PostTag>("SELECT PostTagID, TagDescription FROM Tag").Last();
                        const string postNewTagQuery = "INSERT INTO PostTag (PostID, PostTagID) VALUES (@postID, @postTagId)";
                        sqlConnection.Execute(postNewTagQuery, new { postID = newPost.PostId, PostTagId = newTag.PostTagId });
                    }
                }

                foreach (var id in post.TagIds)
                {
                    const string postTagQuery = "INSERT INTO PostTag (PostID, PostTagID) VALUES (@postID, @postTagId)";
                    sqlConnection.Execute(postTagQuery, new { postID = newPost.PostId, postTagId = id });
                }

                foreach (var id in post.CategoryIds)
                {
                    const string categoryQuery = "INSERT INTO PostCategory (PostID, CategoryID) VALUES (@postID, @CategoryId)";
                    sqlConnection.Execute(categoryQuery, new { postID = newPost.PostId, CategoryId = id });
                }
            }
        }

        public string[] SplitUserString(string userString)
        {
            char[] delimiters = new char[] { ',', ' ' };
            return userString.ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
        }

        public void InsertStaticPage(Post post)
        {
            using (SqlConnection sqlConnection = new SqlConnection(Settings.ConnectionString))
            {
                const string postQuery = "INSERT INTO Post (PostTitle, PostBody, DateCreated, DatePublished, IsStatic, UserId, PostStatusId) VALUES (@postTitle, @postBody, @DateCreated, @DatePublished, 1, 1, 1)";
                sqlConnection.Execute(postQuery, new { PostBody = post.PostBody, PostTitle = post.PostTitle, DateCreated = DateTime.Now.ToString(), DatePublished = DateTime.Now.ToString(),IsStatic = 1, UserId = 1, PostStatusId = 1 });

                var newPost = sqlConnection.Query<Post>("SELECT PostID FROM Post").Last();

                const string postTagQuery = "INSERT INTO PostTag (PostID, PostTagID) VALUES (@postID, @postTagId)";
                sqlConnection.Execute(postTagQuery, new { postID = newPost.PostId, postTagId = 1 });

                const string postCatQuery = "INSERT INTO PostCategory (PostID, CategoryID) VALUES (@postID, @CategoryId)";
                sqlConnection.Execute(postCatQuery, new { postID = newPost.PostId, CategoryId = 5 });
            }
        }
    }
}
