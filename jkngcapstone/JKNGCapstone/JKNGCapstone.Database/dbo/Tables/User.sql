﻿CREATE TABLE [dbo].[User] (
    [UserID]   INT          IDENTITY (1, 1) NOT NULL,
    [UserName] VARCHAR (50) NULL,
    CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED ([UserID] ASC)
);

