﻿CREATE PROCEDURE [dbo].[DeactivatePage]
(
	@PostId int,
	@DatePublished date
) AS

UPDATE Post
set PostStatusId = 4, DatePublished = @DatePublished
WHERE PostID = @PostID