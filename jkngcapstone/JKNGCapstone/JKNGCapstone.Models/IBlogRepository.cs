﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JKNGCapstone.Models
{
    public interface IBlogRepository
    {
        List<Post> GetAllPosts();
        Post GetPostByPostId(int id);
        void Insert(Post post);
        void EditPost(Post post);
        void DeletePost(int id);
        List<Post> GetPostsByPostCategory(string category); //parameters?
        List<Post> GetPostsByPostTag(string tag); //parameters?
        List<User> GetAllUsers();
        void InsertStaticPage(Post post);
        void DeleteStaticPage(int id);
        void PublishPost(int id);
        void DeactivatePage(int id);
        Post GetStaticPageById(int id);
        void EditStatic(Post post);
        List<Post> GetAllStaticPages();
        List<Post> GetAllPostsNotPublished();
    }
}
