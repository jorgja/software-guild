﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JKNGCapstone.Models
{
    public class StaticPage
    {
        public StaticPage() { }
        public int StaticPageID { get; set; }

        public string StaticPageDescription { get; set; }
    }
}
