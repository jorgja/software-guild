﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKNGCapstone.Models
{
    public class Post
    {
        public int PostId { get; set; }

        [Required(ErrorMessage = "Please enter a title")]
        [AllowHtml]
        public string PostTitle { get; set; }

        [Required(ErrorMessage = "Please enter a body")]
        [AllowHtml]
        public string PostBody { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DatePublished { get; set; }
        public PostStatus PostStatus { get; set; }
        public int PostStatusId { get; set; }   
        public int? PostCategory { get; set; }
       
        [AllowHtml]
        public List<Category> PostCategoryDescription { get; set; }
        public int? PostTagId { get; set; }
        
        [AllowHtml]
        public List<PostTag> PostTag { get; set; }

        [Required(ErrorMessage = "Please choose at lease one category")]
        public string[] CategoryIds { get; set; }

        [Required(ErrorMessage = "Please choose at lease one tag")]
        public string[] TagIds { get; set; }

        public bool IsStatic { get; set; }
        public StaticPage StaticPage { get; set; }
    }
}