﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JKNGCapstone.Models
{
    public class Category
    {
        public int CategoryID { get; set; }
       
        public string CategoryDescription { get; set; }
    }
}
