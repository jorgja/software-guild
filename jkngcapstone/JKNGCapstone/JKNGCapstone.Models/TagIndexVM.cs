﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JKNGCapstone.Models
{
    public class TagIndexVM
    {
            public List<Post> TaggedPostList { get; set; }

            public Post post { get; set; }
            public int Count { get; set; }
        
    }
}
