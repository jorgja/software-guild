﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace JKNGCapstone.Models
{
    public class PostTag
    {
        public int PostTagId { get; set; }
        public string TagDescription { get; set; }
    }
}