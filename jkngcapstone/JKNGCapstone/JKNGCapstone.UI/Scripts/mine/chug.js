﻿var code = "chug"
var input = ""
$(document).ready(function () {
    $(document).keypress(function (e) {
        if (String.fromCharCode(e.which) == code[input.length]) {
            input += String.fromCharCode(e.which);
            if (input == code) {
                $("*").fadeOut(3000);
                setTimeout(function () { $("*").fadeIn(5000) }, 5000);
                input = "";
            }
        }
        else {
            input = "";
        }
    });
});