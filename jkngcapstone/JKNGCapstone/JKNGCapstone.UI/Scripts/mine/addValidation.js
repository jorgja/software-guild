﻿$(document).ready(function () {
    $('#addForm').validate({
        rules: {
            PostId: {
                required: true
            },
            PostTitle: {
                required: true
            },
            CategoryIds: {
                required: true
            },
            TagIds: {
                required: true
            }
        }
    });
});