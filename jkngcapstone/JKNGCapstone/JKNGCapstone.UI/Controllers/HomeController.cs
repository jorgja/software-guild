﻿using JKNGCapstone.Data;
using JKNGCapstone.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKNGCapstone.UI.Controllers
{
    public class HomeController : Controller
    {
        public HomeController() { }
        //HomeController constructor implements IBlogRepository
        IBlogRepository _blogRepo = new BlogRepository();
        public HomeController(IBlogRepository repo)
        {
            _blogRepo = repo;
        }

        //public ActionResult Static(int id)
        //{
        //    var model = _blogRepo.GetStaticPageById(id);
        //    return View(model);
        //}

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}