﻿using JKNGCapstone.Data;
using JKNGCapstone.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKNGCapstone.UI.Controllers
{
    public class UserController : Controller
    {
        public UserController() { }

        IBlogRepository _blogRepo = new BlogRepository();
        public UserController(IBlogRepository repo)
        {
            _blogRepo = repo;
        }

        public ActionResult Index()
        {
            var model = _blogRepo.GetAllUsers();

            return View(model);
        }
    }
}