﻿using JKNGCapstone.Data;
using JKNGCapstone.Models;
using JKNGCapstone.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKNGCapstone.UI.Controllers
{
    public class BlogController : Controller
    {
        public BlogController() { }

        //BlogController constructor implements IBlogRepository
        IBlogRepository _blogRepo=new BlogRepository();
        public BlogController(IBlogRepository repo)
        {
            _blogRepo = repo;
        }

        // GET: Post
        public ActionResult Add()
        {
            var model = new AddPostVM();
            return View(model);
        }

        [HttpPost]
        public ActionResult Add(AddPostVM model)
        {
            if (ModelState.IsValid)
            {
                _blogRepo.Insert(model.Post);

                return RedirectToAction("Index", "Blog");            
            }
            return View(model);
        }

        public ActionResult AddStaticPage()
        {
            var model = new AddStaticPageVM();
            return View(model);
        }

        [HttpPost]
        public ActionResult AddStaticPage(AddStaticPageVM model)
        {
            ModelState["post.TagIds"].Errors.Clear();
            if (ModelState.IsValid)
            {
                _blogRepo.InsertStaticPage(model.post);
                return RedirectToAction("StaticIndex", "Blog");
            }
            return View(model);
        }

        // GET: Index
        public ActionResult Index(int id)
        {
            IndexVM model = new IndexVM()
            {
                Posts = _blogRepo.GetAllPosts(),
            };
            model.Count = model.Posts.Count();
            model.Posts = model.Posts.OrderByDescending(p => p.DatePublished.Date).ToList();
            model.Posts = model.Posts.Skip(5 * (id - 1)).Take(5).ToList();
            return View(model);
        }

        public ActionResult StaticIndex(int id)
        {
            AddStaticPageVM model = new AddStaticPageVM();

            model.StaticPageList = _blogRepo.GetAllStaticPages();
            model.Count = model.StaticPageList.Count();
            model.StaticPageList = model.StaticPageList.Skip(5 * (id - 1)).Take(5).ToList();

            return View(model);
        }

        public ActionResult Details(int id)
        {
            Post model = _blogRepo.GetPostByPostId(id);
            return View(model);
        }

        // GET: AdminIndex page
        public ActionResult AdminIndex()
        {
            List<Post> model = _blogRepo.GetAllPostsNotPublished();
            return View(model);
        }
       
        [HttpPost]
        public ActionResult DeleteStaticPage(int id)
        {
            _blogRepo.DeleteStaticPage(id);
            return RedirectToAction("StaticIndex", "Blog");
        }

        [HttpPost]
        public ActionResult PublishStaticPage(int id)
        {
            _blogRepo.PublishPost(id);
            return RedirectToAction("StaticIndex", "Blog");
        }

        [HttpPost]
        public ActionResult DeactivatePage(int id)
        {
            _blogRepo.DeactivatePage(id);
            return RedirectToAction("StaticIndex", "Blog");
        }

        [HttpPost]
        public ActionResult DeleteBlogPost(int id)
        {
            _blogRepo.DeletePost(id);
            return RedirectToAction("AdminIndex", "Blog");
        }

        [HttpPost]
        public ActionResult PublishBlogPost(int id)
        {
            _blogRepo.PublishPost(id);
            return RedirectToAction("AdminIndex", "Blog");
        }

        public ActionResult Edit(int id)
        {
            AddPostVM model = new AddPostVM()
            {
                Post = _blogRepo.GetPostByPostId(id)
            };

            foreach(var category in model.Category)
            {
                if(model.Post.PostCategoryDescription.Where(c=>c.CategoryDescription == category.Text).Count() > 0)
                {
                    category.Selected = true;
                }
            }

            foreach (var tag in model.Tag)
            {
                if (model.Post.PostTag.Where(t => t.TagDescription == tag.Text).Count() > 0)
                {
                    tag.Selected = true;
                }
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AddPostVM model)
        {
            if (ModelState.IsValid)
            {
                _blogRepo.EditPost(model.Post);
                return RedirectToAction("AdminIndex", "Blog");
            }
            return View(model);
        }

        public ActionResult EditStatic(int id)
        {
            AddStaticPageVM model = new AddStaticPageVM()
            {
                post = _blogRepo.GetStaticPageById(id)
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult EditStatic(AddStaticPageVM model)
        {
            //ModelState["post.TagIds"].Errors.Clear();
            if (ModelState.IsValid)
            {
                _blogRepo.EditStatic(model.post);
                return RedirectToAction("StaticIndex", "Blog");
            }
            return View(model);
        } 

        public ActionResult GetStaticPage(int id)
        {
            return RedirectToAction("Details", "Blog", new { id });
        }

        public ActionResult Static(int id)
        {
            var model = _blogRepo.GetStaticPageById(id);
            return View(model);
        }
    }
}