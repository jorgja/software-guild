﻿using JKNGCapstone.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace JKNGCapstone.UI.Controllers
{
    public class PartialViewController : Controller
    {
        // GET: PartialView
        public PartialViewResult Action()
        {
            BlogRepository _blogRepo = new BlogRepository();
            var model = _blogRepo.GetAllActiveStaticPages();
            return PartialView(model);
        }
    }
}