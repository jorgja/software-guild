﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(JKNGCapstone.UI.Startup))]
namespace JKNGCapstone.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
