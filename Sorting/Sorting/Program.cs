﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> nums = MakeList(100);

            foreach(int i in nums)
            {
                Console.WriteLine(i);
            }
            Console.ReadLine();

            SelectionSort(nums);
            foreach(int i in nums)
            {
                Console.WriteLine(i);
            }
            Console.ReadLine();
        }

        private static List<int> MakeList(int n)
        {
            List<int> nums = new List<int>();
            Random r = new Random();
            for(int i = 1; i <= n; i++)
            {
                nums.Add(i);
            }

            for (int i = 1; i < n; i++)
            {
                int rand = r.Next(1, n);
                int temp = nums[i-1];
                nums[i-1] = nums[rand];
                nums[rand] = temp;
            }
            return nums;
        }

        private static List<int> BubbleSort(List<int> nums)
        {
            while (1 != 2)
            {
                int swaps = 0;
                for (int i = 1; i < nums.Count(); i++)
                {
                    if (nums[i - 1] > nums[i])
                    {
                        int temp = nums[i - 1];
                        nums[i - 1] = nums[i];
                        nums[i] = temp;
                        swaps++;
                    }
                }
                if (swaps == 0)
                {
                    return nums;
                }
            }
        }

        private static List<int> SelectionSort(List<int> nums)
        {
            for(int j = 0; j < nums.Count(); j++)
            {
                int min = 0;
                int temp = -1;
                for (int i = j; i < nums.Count(); i++)
                {
                    if (i == j)
                    {
                        min = nums[i];
                    }
                    if (nums[i] < min)
                    {
                        min = nums[i];
                        temp = i;
                    }
                    if(i == nums.Count() - 1 && temp >= 0)
                    {
                        nums[temp] = nums[j];
                        nums[j] = min;
                    }
                }
                if (min == nums.Count())
                {
                    return nums;
                }
            }
            throw new Exception();
        }
    }
}
