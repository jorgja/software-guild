﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularReviewSession.Models
{
    public class Team
    {
        public int TeamId { get; set; }
        public string City { get; set; }
        public string TeamName { get; set; }
        public string URL { get; set; }
    }
}
