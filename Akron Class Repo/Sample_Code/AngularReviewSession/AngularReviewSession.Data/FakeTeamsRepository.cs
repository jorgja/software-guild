﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngularReviewSession.Models;

namespace AngularReviewSession.Data
{
    public class FakeTeamsRepository
    {
        private static readonly List<Team> _teams = new List<Team>();

        static FakeTeamsRepository()
        {
            _teams.AddRange(new[]
            {
                new Team() {TeamId = 1, City = "Cleveland", TeamName = "Indians", URL = "http://content.sportslogos.net/logos/53/57/full/720.png"},
                new Team() {TeamId = 2, City = "Detroit", TeamName = "Tigers", URL = "http://content.sportslogos.net/logos/53/59/full/6n8ewtl7lzamf20eohyv4aav2.png"},
                new Team() {TeamId = 3, City = "Chicago", TeamName = "White Sox", URL = "http://content.sportslogos.net/logos/53/55/full/oxvkprv7v4inf5dgqdebp0yse.png"},
                new Team() {TeamId = 4, City = "Chicago", TeamName = "Cubs", URL = "http://content.sportslogos.net/logos/54/54/full/q9gvs07u72gc9xr3395u6jh68.png"},
                new Team() {TeamId = 5, City = "Minnesota", TeamName = "Twins", URL = "http://content.sportslogos.net/logos/53/65/full/peii986yf4l42v3aa3hy0ovlf.png"},
                new Team() {TeamId = 6, City = "Pittsburgh", TeamName = "Pirates", URL = "http://content.sportslogos.net/logos/54/71/full/uorovupw0jagctt6iu1huivi9.png"}
            });
        }

        public List<Team> GetAll()
        {
            return _teams;
        }

        public void Add(Team newTeam)
        {
            _teams.Add(newTeam);
        }
    }
}
