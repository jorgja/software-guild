﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DynamicSectionExample.UI.Models;

namespace DynamicSectionExample.Data
{
    public class GameSystemRepository
    {
        private static List<GameSystem> _gameSystems;

        public GameSystemRepository()
        {
            _gameSystems = new List<GameSystem>()
            {
                new GameSystem()
                {
                    Manufacturer = "Nintendo",
                    Name = "WII",
                    Games = new List<VideoGame>()
                    {
                        new VideoGame() { Title = "WII Sports", Genre = "Sports", ReleaseDate = new DateTime(2006, 11,19)},
                        new VideoGame() { Title = "Penguins of Madagascar", Genre = "Adventrue", ReleaseDate = new DateTime(2014, 11,11)},
                        new VideoGame() { Title = "Angry Birds Star Wars", Genre = "Puzzle", ReleaseDate = new DateTime(2013, 11,1)},
                    }
                }
            };
        }

        public List<GameSystem> GetAllGameSystems()
        {
            return _gameSystems;
        }

        public void AddGameSystem(GameSystem newSystem)
        {
            _gameSystems.Add(newSystem);
        }
    }
}
