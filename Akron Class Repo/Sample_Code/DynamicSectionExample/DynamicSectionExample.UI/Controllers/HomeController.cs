﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebSockets;
using DynamicSectionExample.Data;
using DynamicSectionExample.UI.Models;

namespace DynamicSectionExample.UI.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var repo = new GameSystemRepository();
            return View(repo.GetAllGameSystems());
        }

        public ActionResult AddSystem()
        {
            return View(new GameSystem() {Games = new List<VideoGame>() {new VideoGame()} });
        }

        [HttpPost]
        public ActionResult AddSystem(GameSystem newSystem)
        {
            // here we are checking which of the buttons was clicked.
            // when a button is clicked it is added to the post data
            // only the button clicked will be in the post data
            // hence we can check if Save or Add is clicked. 
            if (Request.Form.AllKeys.Contains("Save"))
            {
                // EXTRA CHECK TO ENSURE THEY TYPED IN A GAME TITLE
                // NO GAME TITLE, NO GAME IN LIST
                newSystem.Games.RemoveAll(g => string.IsNullOrEmpty(g.Title));

                // save clicked - add system to repository
                var repo = new GameSystemRepository();
                repo.AddGameSystem(newSystem);

                // display the index page with the new system's games
                return View("Index", repo.GetAllGameSystems());

            }
            else
            {
                // add clicked - add a new video game instance to the object. 
                newSystem.Games.Add(new VideoGame());
                return View(newSystem);
            }
        }
    }
}