﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DynamicSectionExample.UI.Models
{
    public class GameSystem
    {
        public string Name { get; set; }
        public string Manufacturer { get; set; }
        public List<VideoGame> Games { get; set; } 
    }
}