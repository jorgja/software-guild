﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            Program prog = new Program();
            prog.IterateString();
            prog.SplitString();
            prog.SimpleArray();
            prog.ReverseString();
            prog.DeclareImplicitArrays();
            prog.ArrayOfObjects();
            prog.RectMultiDimensionalArray();
            prog.JaggedMuliDimensionalArray();

            Console.ReadLine();
        }

        public void IterateString()
        {
            string s1 = "this is a string of characters.";

            // write each character of a string on a new line
            foreach (char c in s1)
            {
                Console.WriteLine(c);
            }

            Console.WriteLine("The character at index 3 is {0}", s1[3]);
            Console.WriteLine("The length of s1 is {0}", s1.Length);
        }

        public void SplitString()
        {
            // separate out each word of the sentence
            string[] words = "This is a sentence.".Split(' ');
            foreach (string s in words)
            {
                Console.WriteLine(s);
            }
        }

        public void SimpleArray()
        {
            // simple array type
            int[] myInts = new int[3];
            myInts[0] = 100;
            myInts[1] = 200;

            // Hey, what's at index 2
            foreach (int i in myInts)
            {
                Console.WriteLine(i);
            }
        }

        public void ReverseString()
        {
            // *********************************************
            // YOU NEED TO KNOW THIS!
            // *********************************************
            string myString = "String to Reverse";

            for (int i = 0; i < myString.Length; i++)
            {
                Console.Write(myString[myString.Length - 1 - i]);
            }

            Console.WriteLine();

            for (int i = myString.Length - 1; i >= 0; i--)
            {
                Console.Write(myString[i]);
            }
        }

        public void DeclareImplicitArrays()
        {
            // You mean I can ToString an Array? 
            var a = new[] {1, 10, 100, 1000};
            Console.WriteLine("a is a: {0}", a.ToString());

            var b = new[] {1, 1.5, 2, 2.5};
            Console.WriteLine("b is a: {0}", b.ToString());

            var c = new[] {"hello", null, "world"};
            Console.WriteLine("c is a: {0}", c.ToString());
        }

        public void ArrayOfObjects()
        {
            // Yes you can even put objects in the array. 
            object[] myObjects = new object[4];
            myObjects[0] = 10;
            myObjects[1] = false;
            myObjects[2] = new DateTime(2015, 09, 21);
            myObjects[3] = "THis is a string";

            foreach (object myObject in myObjects)
            {
                Console.WriteLine("Type: {0}, Value: {1}", myObject.GetType(), myObject);
            }
        }

        public void RectMultiDimensionalArray()
        {
            // NICE arrays
            int[,] myGrid = new int[5,6];

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    myGrid[i, j] = i*j;
                }
            }

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    Console.Write(myGrid[i, j] + "\t");
                }

                Console.WriteLine();
            }
        }

        public void JaggedMuliDimensionalArray()
        {
            // Know these exist and how they work.
            int[][] myJaggedArray = new int[5][];

            for (int i = 0; i < myJaggedArray.Length; i++)
            {
                myJaggedArray[i] = new int[i + 3];
            }

            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < myJaggedArray[i].Length; j++)
                {
                    Console.Write(myJaggedArray[i][j] + " ");
                }

                Console.WriteLine();
            }
        }
    }
}
