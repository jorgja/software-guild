﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculatorKata
{
    public class KataCalculator
    {
        public int Add(string numbers)
        {
            int sum = 0;

            if (String.IsNullOrEmpty(numbers))
            {
                return sum;
            }

            string[] del;
            if (numbers.StartsWith("//"))
            {
                // take //[delimiter]\n[numbers]
                // separate //[delimiter] and [numbers]
                string[] parts = numbers.Split('\n');

                if (parts[0].Length == 3)
                {
                    del = new string[] {parts[0].Remove(0,2)};
                }
                else
                {
                    List<string> delims = new List<string>();
                    string d = "";
                    foreach (char c in parts[0])
                    {
                        switch (c)
                        {
                            case '[':
                                d = "";
                                break;
                            case ']':
                                delims.Add(d);
                                break;
                            case '/':
                                break;
                            default:
                                d += c;
                                break;
                        }
                    }
                    //del = new string[]
                    //{
                    //    parts[0].Substring(3,
                    //        parts[0].Length - 4)
                    //};
                    del = delims.ToArray();
                }

                numbers = parts[1];
            }
            else
            {
                del = new string[] {",", "\n"};
            }

            string[] arrNums = numbers.Split(del, StringSplitOptions.RemoveEmptyEntries);

            if (arrNums.Length == 1)
            {
                sum = int.Parse(numbers);
            }
            else
            {
                int number = 0;
                bool isNegativeFound = false;
                for (int i = 0; i < arrNums.Length; i++)
                {
                    number = int.Parse(arrNums[i]);
                    if ((number > 0) && !isNegativeFound)
                    {
                        if (number < 1000)
                        {
                            sum += number;
                        }
                    }
                    else
                    {
                        if (!isNegativeFound)
                        {
                            Console.Write("Negatives not allowed: {0}", number);
                        }
                        else
                        {
                            Console.Write(",{0}", number);
                        }

                        isNegativeFound = true;
                        sum = -1;
                    }
                     
                }
            }

            return sum;
        }
    }
}
