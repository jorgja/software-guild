﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FunWithDapper.Models;

namespace FunWithDapper
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindRepository repo = new NorthwindRepository();
            List<Employee> employees = repo.GetAll();

            foreach (var employee in employees)
            {
                Console.WriteLine("{0}, {1} - {2}",
                    employee.LastName, employee.FirstName,
                    employee.EmployeeId);
            }

            Employee emp = repo.GetEmployeeByID(5);

            Console.WriteLine();
            Console.WriteLine("{0}, {1} - {2}",
                    emp.LastName, emp.FirstName,
                    emp.EmployeeId);

            int id = repo.InsertRegion("OHIO");

            Console.WriteLine();
            Console.WriteLine("New Region ID is {0}", id);

            Console.ReadLine();

        }
    }
}
