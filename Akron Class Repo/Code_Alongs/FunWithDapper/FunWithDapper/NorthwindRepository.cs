﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using FunWithDapper.Models;

namespace FunWithDapper
{
    public class NorthwindRepository
    {
        public List<Employee> GetAll()
        {
            List<Employee> employees = new List<Employee>();


            using (SqlConnection cn = new SqlConnection(
                ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString))
            {
                // ADO.NET
                // We need to create the command object
                // use the data reader
                // and then map the date ourselves
                
                //using (SqlCommand cmd = new SqlCommand())
                //{
                //    cmd.CommandText = "SELECT e1.EmployeeID, e1.LastName, e1.FirstName, " +
                //                      "e1.Title, e1.BirthDate, e1.ReportsTo, " +
                //                      "e2.LastName as ManagerName " +
                //                      "FROM Employees e1 " +
                //                      "LEFT JOIN Employees e2 " +
                //                      "ON e1.ReportsTo = e2.EmployeeID ";

                //    cmd.Connection = cn;
                //    cn.Open();

                //    using (SqlDataReader dr = cmd.ExecuteReader())
                //    {
                //        while (dr.Read())
                //        {
                //            employees.Add(PopulateFromDataReader(dr));
                //        }
                //    }
                //}

                // DAPPER
                // we call the Dapper Query method and provide the object
                // and voila, it's mapped. 
                employees = cn.Query<Employee>("SELECT e1.EmployeeID, e1.LastName, e1.FirstName, " +
                                               "e1.Title, e1.BirthDate, e1.ReportsTo, " +
                                               "e2.LastName as 'ManagerName' " +
                                               "FROM Employees e1 " +
                                               "left join Employees e2 " +
                                               "on e2.EmployeeID = e1.ReportsTo")
                    .ToList();
            }

            return employees;
        }

        private Employee PopulateFromDataReader(SqlDataReader dr)
        {
            // Manual mapping of columns to object
            Employee employee = new Employee();

            employee.EmployeeId = (int) dr["EmployeeID"];
            employee.LastName = dr["LastName"].ToString();
            employee.FirstName = dr["FirstName"].ToString();
            employee.Title = dr["Title"].ToString();

            if (dr["BirthDate"] != DBNull.Value)
            {
                employee.BirthDate = (DateTime) dr["BirthDate"];
            }

            if (dr["ReportsTo"] != DBNull.Value)
            {
                employee.ReportsTo = (int) dr["ReportsTo"];
                employee.ManagerName = dr["ManagerName"].ToString();
            }

            return employee;

        }

        public Employee GetEmployeeByID(int employeeId)
        {
            using (SqlConnection cn = new SqlConnection(
                ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString))
            {
                // using DynamicParameters object to set parameters
                var parameters = new DynamicParameters();
                parameters.Add("EmployeeId", employeeId);

                Employee employee = cn.Query<Employee>("SELECT * FROM Employees " +
                                                       "WHERE EmployeeID = @EmployeeId",
                    //Create new anonymous type and set parameters in-line
                    //new {EmployeeId = employeeId})
                    parameters)
                    .FirstOrDefault();

                return employee;
            }
        }

        public int InsertRegion(string description)
        {
            using (SqlConnection cn = new SqlConnection(
                ConfigurationManager.ConnectionStrings["Northwind"].ConnectionString))
            {
                var parameters = new DynamicParameters();
                parameters.Add("RegionDescription", description);

                // setting up an output parameter
                parameters.Add("RegionId", DbType.Int32, 
                    direction: ParameterDirection.Output);

                // executing a stored procedure. 
                cn.Execute("RegionInsert", parameters,
                    commandType: CommandType.StoredProcedure);

                int regionId = parameters.Get<int>("RegionId");

                return regionId;
            }
        }
    }
}
