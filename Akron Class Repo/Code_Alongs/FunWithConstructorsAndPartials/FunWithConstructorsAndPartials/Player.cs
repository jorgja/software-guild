﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithConstructorsAndPartials
{
    public partial class Player
    {
        // property
        public string Name { get; private set; }

        // property
        public int Score { get; set; }

        // constructor
        public Player()
        {
            Name = "New Player";
        }
    }
}
