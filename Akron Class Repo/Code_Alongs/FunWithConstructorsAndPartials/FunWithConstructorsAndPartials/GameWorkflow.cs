﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithConstructorsAndPartials
{
    public class GameWorkflow
    {
        // properies that are objects
        public Player Player1 { get; set; }
        public Player Player2 { get; set; }

        // private static readonly field
        // private - only inside of the class
        // static - there is only one instance EVER
        // readonly - set in the constructor and then can't change
        private static readonly Random _randomKey;

        // static constructor 
        // can initialize the static property here
        // Note: it cannot interact with the other properties (ie Player1, Player2)
        static GameWorkflow()
        {
            _randomKey = new Random();
        }

        // private method that will only be called within the class
        private int RollDice()
        {
            return _randomKey.Next(1, 7);
        }

        public void PlayGame()
        {
            while (Player1.Score < 100 && Player2.Score < 100)
            {
                Player1.Score += RollDice();
                Player2.Score += RollDice();
            }

            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("GAME OVER");
            Console.WriteLine("{0}: {1} vs {2}: {3}", Player1.Name, Player1.Score, Player2.Name, Player2.Score);
        }
    }
}
