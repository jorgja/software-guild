﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithConstructorsAndPartials
{
    public partial class Player
    {
        // constructor
        public Player(string Name)
        {
            // use of the this keyword to distinguish parameter from property
            // this.Name is property
            // Name is the parameter from the constructor
            this.Name = Name;
        }
    }
}
