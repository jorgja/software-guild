﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringCalculatorKata
{
    public class StringCalculatorKata
    {
        public int Add(string numbers)
        {
            int sum = 0;

            if (numbers == "")
            {
                return sum;
            }

            char[] delimeters;
            if (numbers.StartsWith("//"))
            {
                string[] inputArray = numbers.Split('\n');
                string del = inputArray[0].Substring(2);
                char delimeter = char.Parse(del);
                delimeters = new char[] {delimeter};

                numbers = inputArray[1];
            }
            else
            {
                delimeters = new char[] {',','\n'};
            }

            string[] array = numbers.Split(delimeters);

            // This will give you the number of numbers that were parsed
            // Example: numbers = "1,2" then numbersInArray = 2
            //int numbersInArray = array.Length;

            foreach (string number in array)
            {
                sum += int.Parse(number);
            }

            return sum;
        }
    }
}
