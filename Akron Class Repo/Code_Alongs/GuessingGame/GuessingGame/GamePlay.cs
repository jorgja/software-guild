﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    public class GamePlay
    {
        public bool PlayRound(string name)
        {
            // determine the number to guess
            Random random = new Random();
            int theAnswer = random.Next(1, 21);

            bool isNumberGuessed = false;
            do
            {
                // get the player's guess
                Console.Write("{0}, Enter your guess: ", name);
                string playerInput = Console.ReadLine();

                // need to make sure we got a number from the user
                int playerGuess;
                if (int.TryParse(playerInput, out playerGuess))
                {
                    if (theAnswer == playerGuess)
                    {
                        Console.WriteLine("{0}, YOU GOT IT!!!", name.ToUpper());
                        isNumberGuessed = true;
                    }
                }
            } while (!isNumberGuessed); 


            // Ask User if they want to play again
            bool playAnotherRound = false;
            Console.Write("{0}, would you like to play Again? (y/n): ", name);
            string response = Console.ReadLine();

            if (response.ToUpper() == "Y")
            {
                playAnotherRound = true;
            }

            return playAnotherRound;
        }
    }
}
