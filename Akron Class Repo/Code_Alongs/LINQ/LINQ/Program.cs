﻿using System;
using System.Globalization;
using System.Linq;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            //PrintOutOfStock();
            //InStockMoreThan3();
            //ReturnPairsBLessThanC();
            //First3OrdersInWash();
            //ReturnValuesLessThanPosition();
            //GroupThemByYearMonth();
            //AllProductsInCategoryInStock();
            GetLowestProductInCategory();

            Console.ReadLine();
        }

        //1. Find all products that are out of stock.
        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            //var results = products.Where(p => p.UnitsInStock == 0);
            var results = from p in products
                where p.UnitsInStock == 0
                select p;

            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }

        //2. Find all products that are in stock and cost more than 3.00 per unit.
        private static void InStockMoreThan3()
        {
            var products = DataLoader.LoadProducts();

            //var results = products.Where(p => p.UnitsInStock > 0 && p.UnitPrice > 3);
            var results = from p in products
                where p.UnitsInStock > 0 && p.UnitPrice > 3
                select p;

            foreach (var product in results)
            {
                Console.WriteLine("{0} has {1} in stock with unit price {2}", product.ProductName,
                    product.UnitsInStock, product.UnitPrice);
            }
        }

        //9. Make a query that returns all pairs of numbers from both arrays such 
        //   that the number from numbersB is less than the number from numbersC.
        private static void ReturnPairsBLessThanC()
        {
            int[] numbersB = DataLoader.NumbersB;
            int[] numbersC = DataLoader.NumbersC;

            //var results = from b in numbersB
            //    from c in numbersC
            //    where b < c
            //    select new {b, c};
            //var results = numbersB.SelectMany(b => numbersC, (b, c) => new {b, c})
            //    .Where(x => x.b < x.c);

            //int i = -1;
            //var results = from b in numbersB
            //    let index = i++
            //    where b < numbersC[i]
            //    select new {b = b, c = numbersC[i]};
            var results = numbersB.Select((number, index) => new {b = number, c = numbersC[index]})
                .Where(x => x.b < x.c);

            foreach (var result in results)
            {
                Console.WriteLine("b = {0}, c = {1}", result.b, result.c);
            }
        }

        //12. Get only the first 3 orders from customers in Washington.
        private static void First3OrdersInWash()
        {
            var customers = DataLoader.LoadCustomers();

            //var results = (from c in customers
            //               from o in c.Orders
            //               where c.Region == "WA"
            //               select new { c.CompanyName, o.OrderDate, o.OrderID }).Take(3);
            var results = customers.SelectMany(cust => cust.Orders, (cust, order) => new { cust, order })
                .Where(x => x.cust.Region == "WA")
                .Select(wa => new { wa.cust.CompanyName, wa.order.OrderDate, wa.order.OrderID })
                .Take(3);

            foreach (var o in results)
            {
                Console.WriteLine("{0} {1} - {2}", o.OrderDate, o.OrderID, o.CompanyName);
            }
        }

        //16. Return elements starting from the beginning of NumbersC until a 
        //    number is hit that is less than its position in the array.
        private static void ReturnValuesLessThanPosition()
        {
            var numbersC = DataLoader.NumbersC;

            var results = numbersC.TakeWhile((number, index) => number >= index);

            foreach (var result in results)
            {
                Console.WriteLine(result);
            }
        }

        //24. Group customer orders by year, then by month.
        private static void GroupThemByYearMonth()
        {
            var customers = DataLoader.LoadCustomers();

            var results = from c in customers
                select new
                {
                    CompanyName = c.CompanyName,
                    YearGroups = from o in c.Orders
                        group o by o.OrderDate.Year
                        into yearGroup
                        select new
                        {
                            Year = yearGroup.Key,
                            monthGroups = from order in yearGroup
                                group order by order.OrderDate.Month
                                into monthGroup
                                select new
                                {
                                    Month = monthGroup.Key,
                                    Orders = monthGroup
                                }
                        }
                };

            foreach (var result in results)
            {
                Console.WriteLine(result.CompanyName);

                foreach (var yearGroup in result.YearGroups)
                {
                    Console.WriteLine("\t{0}", yearGroup.Year);

                    foreach (var monthGroup in yearGroup.monthGroups)
                    {
                        Console.WriteLine("\t\t{0}", CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(monthGroup.Month));

                        foreach (var order in monthGroup.Orders)
                        {
                            Console.WriteLine("\t\t\t{0} - {1}", order.OrderDate, order.OrderID);
                        }
                    }
                }
            }
        }

        //33. Get a grouped a list of products only for categories that have all of their products in stock.
        private static void AllProductsInCategoryInStock()
        {
            var products = DataLoader.LoadProducts();

            //var results = from p in products
            //    group p by p.Category
            //    into pCats
            //    where pCats.All(x => x.UnitsInStock > 0)
            //    select new {pCats.Key, coll = pCats};
            var results = products.GroupBy(p => p.Category)
                .Where(p => p.All(x => x.UnitsInStock > 0))
                .Select(prod => new {prod.Key, coll= prod});

            foreach (var result in results)
            {
                Console.WriteLine(result.Key);
                foreach (var product in result.coll)
                {
                    Console.WriteLine("\t{0} - {1}", product.UnitsInStock, product.ProductName);
                }
            }
        }

        //38. Display the lowest priced product in each category.
        private static void GetLowestProductInCategory()
        {
            var products = DataLoader.LoadProducts();

            //var results = from p in products
            //    group p by p.Category
            //    into pCats
            //    select new
            //    {
            //        pCats.Key,
            //        minprod = pCats.Min(prod => prod.UnitPrice),
            //        prodName = pCats.First(prod => prod.UnitPrice == pCats.Min(p => p.UnitPrice))
            //    };
            var results = products.GroupBy(prod => prod.Category)
                .Select(x => new
                {
                    x.Key,
                    minprod = x.Min(p => p.UnitPrice),
                    prodName = x.First(prod => prod.UnitPrice == x.Min(p => p.UnitPrice))
                });

            foreach (var result in results)
            {
                Console.WriteLine("{0} - {1:C} {2}", result.Key, result.minprod, result.prodName.ProductName);
                //Console.WriteLine($"{result.minprod:C}");
            }
        }
    }
}
