﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace URLRouting.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("DL", "DonkeyLobster",
                new {controller = "Admin", action = "Index"});

            routes.MapRoute("ShopSchema", "Shop/{action}",
                new {controller = "Home"});

            routes.MapRoute("MyRoute", "{controller}/{action}/{id}",
                new {controller = "Customer", action = "Index", id = UrlParameter.Optional});
        }
    }
}
