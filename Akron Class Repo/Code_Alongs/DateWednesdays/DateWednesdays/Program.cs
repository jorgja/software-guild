﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateWednesdays
{
    class Program
    {
        static void Main(string[] args)
        {
            // Prompt the user for the Date
            Console.Write("Please enter the date you wish to start with: ");
            string dateString = Console.ReadLine();
            DateTime startDate = DateTime.Parse(dateString);

            // Prompt the user for number of Wednesdays
            Console.Write("How many Wednesdays should I print? ");
            string numWedString = Console.ReadLine();
            int numWed = int.Parse(numWedString);

            // Find first Wednesday starting at date
            while (startDate.DayOfWeek != DayOfWeek.Wednesday)
            {
                startDate = startDate.AddDays(1);
            }

            // Loop for NoOfWednesdays - Print Wednesday
            int i = 0;
            do
            {
                Console.WriteLine(startDate.ToShortDateString());
                startDate = startDate.AddDays(7);
                i++;

            } while (i < numWed);

            Console.ReadLine();
        }
    }
}
