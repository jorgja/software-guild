﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirst.Data.Models
{
    public class TaskList
    {
        [Key]
        public int ListId { get; set; }
        public string ListName { get; set; }
        public string Description { get; set; }

        public virtual List<TaskItem> TaskItems { get; set; } 
    }
}
