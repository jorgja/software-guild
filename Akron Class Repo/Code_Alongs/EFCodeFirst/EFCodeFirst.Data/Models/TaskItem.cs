﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirst.Data.Models
{
    public class TaskItem
    {
        [Key]
        public int TaskId { get; set; }
        public string TaskName { get; set; }
        public string TaskDetails { get; set; }

        public int ListId { get; set; }
        public virtual TaskList TaskList { get; set; }

        public virtual List<Tag> Tags { get; set; } 
    }
}
