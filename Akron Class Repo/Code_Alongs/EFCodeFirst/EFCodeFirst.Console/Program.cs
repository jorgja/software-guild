﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFCodeFirst.Data.Models;

namespace EFCodeFirst.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new TaskListContext())
            {
                System.Console.Write("Enter a name for your TaskList: ");
                var name = System.Console.ReadLine();

                var taskList = new TaskList() {ListName = name};
                db.TaskLists.Add(taskList);
                db.SaveChanges();

                foreach (var list in db.TaskLists)
                {
                    System.Console.WriteLine("{0} {1}", list.ListId, list.ListName);
                }
            }

            System.Console.ReadLine();
        }
    }
}
