﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FunWithLINQ.Models;
using FunWithLINQ.Repositories;

namespace FunWithLINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            AnonymousTypes();
            Joins();
            GroupBy();

            Console.ReadLine();
        }

        public static void AnonymousTypes()
        {
            Console.WriteLine("Anonymous Types");

            List<Student> students = StudentRepository.GetAllStudents();

            // Query Syntax
            //var ladies = from student in students
            //    where student.Gender == "F"
            //    select new
            //    {
            //        Name = student.FirstName + " " + student.LastName,
            //        student.Major
            //    };

            // Method Syntax
            var ladies =
                students.Where(s => s.Gender == "F").Select(x => new {Name = x.FirstName + " " + x.LastName, x.Major});

            foreach (var lady in ladies)
            {
                Console.WriteLine("{0} is majoring in {1}", lady.Name, lady.Major);
            }

            Console.WriteLine();
        }

        public static void Joins()
        {
            Console.WriteLine("Joins");

            List<Student> students = StudentRepository.GetAllStudents();
            List<StudentCourse> courses = StudentRepository.GetAllStudentCourses();

            // join students to courses
            // return student name and course name

            // Query Syntax
            //var results = from student in students
            //    join course in courses
            //        on student.ID equals course.StudentID
            //    select new
            //    {
            //        course.CourseName,
            //        StudentName = student.FirstName + " " + student.LastName
            //    };

            // Method Syntax
            var results = students.Join(courses, student => student.ID, course => course.StudentID,
                (student, course) => new
                {
                    course.CourseName,
                    StudentName = student.FirstName + " " + student.LastName
                });

            foreach (var result in results)
            {
                Console.WriteLine("{0} is taking {1}", result.StudentName, result.CourseName);
            }

            Console.WriteLine();
        }

        public static void GroupBy()
        {
            Console.WriteLine("GroupBy");

            var students = StudentRepository.GetAllStudents();

            // Query Syntax
            //var results = from student in students
            //              where student.Major != "Chemistry"
            //              orderby student.Major, student.LastName
            //              group student by student.Major;

            // Method Syntax
            var results =
                students.Where(student => student.Major != "Chemistry")
                    .OrderBy(student => student.Major)
                    .ThenBy(student => student.LastName)
                    .GroupBy(student => student.Major);

            foreach (var group in results)
            {
                Console.WriteLine(group.Key);

                foreach (var student in group)
                {
                    Console.WriteLine("\t{0} {1} - {2}", student.FirstName, student.LastName, student.Major);
                }
            }

            Console.WriteLine();
        }
    }
}
