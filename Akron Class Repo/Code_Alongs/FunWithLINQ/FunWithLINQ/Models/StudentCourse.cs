﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithLINQ.Models
{
    public class StudentCourse
    {
        public int StudentID { get; set; }
        public string CourseName { get; set; }
    }
}
