﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SystemDotObject
{
    class Program
    {
        static void Main(string[] args)
        {
            // create a point and write it out.
            Point newPoint = new Point(5,6);
            Console.WriteLine(newPoint);

            // create a new point and compare
            Point anotherPoint = new Point(5,6);
            Console.WriteLine(Object.Equals(newPoint, anotherPoint));
            Console.WriteLine(Object.ReferenceEquals(newPoint, anotherPoint));

            // use our copy method and create a new point and compare. 
            Point thirdPoint = newPoint.Copy();
            Console.WriteLine(Object.Equals(newPoint, thirdPoint));
            Console.WriteLine(Object.ReferenceEquals(newPoint, thirdPoint));
            Console.ReadLine();
        }
    }
}
