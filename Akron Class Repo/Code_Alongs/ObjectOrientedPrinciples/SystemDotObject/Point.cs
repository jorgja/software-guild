﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace SystemDotObject
{
    public class Point
    {
        // public fields
        public int x { get; set; }
        public int y { get; set; }

        // constructor for Point
        // notice no default constructur means we have to use this one. 
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        // anytime we give the object to print, it will use this method
        public override string ToString()
        {
            return $"Point as ({x}, {y})";
        }

        // allows us to determine the comparison
        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType())
            {
                return false;
            }

            Point otherPoint = (Point) obj;

            return (this.x == otherPoint.x) && (this.y == otherPoint.y);
        }

        // we can do a member-wise copy
        // create a new object and copy only the values of properties.
        public Point Copy()
        {
            return (Point) this.MemberwiseClone();
        }
    }
}
