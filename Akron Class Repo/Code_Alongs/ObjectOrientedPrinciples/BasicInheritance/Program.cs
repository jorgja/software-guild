﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            // constructor that will call Car(45)
            MiniVan myVan = new MiniVan();
            Console.WriteLine("myVan max speed is {0}", myVan.MaxSpeed);

            // constructor that will call corresponding Car(75,0)
            MiniVan secondVan = new MiniVan(75, 0);
            Console.WriteLine("secondVan max speed is {0}", secondVan.MaxSpeed);

            // constructor that will call default Car() because we didnt specify
            MiniVan thirdVan = new MiniVan(100);
            Console.WriteLine("thirdVan max speed is {0}", thirdVan.MaxSpeed);

            Console.ReadLine();
        }
    }
}
