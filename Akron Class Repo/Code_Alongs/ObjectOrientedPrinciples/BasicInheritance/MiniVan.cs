﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicInheritance
{
    public class MiniVan : Car
    {
        // default constructor of MiniVan calling Car(int)
        public MiniVan() : base(45) { }

        // constructor calling Car(int, int)
        public MiniVan(int max, int min) : base(max, min) { }

        // constructor doesn't specify Car constructor and hence calls default Car()
        public MiniVan(int max) { }
    }
}
