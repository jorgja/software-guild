﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape shape = new Shape();
            Console.WriteLine(shape.Draw());

            Circle circle = new Circle();
            circle.Radius = (decimal) 17.11;
            Console.WriteLine(circle.Draw());

            Shape[] shapes = new Shape[2];
            shapes[0] = shape;

            // notice we can put the Cicle into the Shape array
            shapes[1] = circle; 
            
            Console.WriteLine();
            foreach (Shape s in shapes)
            {
                Console.WriteLine(s.Draw());

                // test to see is s a cicle
                if (s is Circle)
                {
                    Console.WriteLine("Cicle radius is {0}", ((Circle)s).Radius);
                }
            }

            Console.ReadLine();
        }
    }
}
