﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithStrings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("***** Fun With Strings *****\n");

            BasicStringFunctionality();
            StringConcatenation();
            EscapeChars();
            VerbatimStrings();
            StringsAreImmutable();
            FunWithStringBuilder();
            StringEquality();

            Console.ReadLine();
        }

        static void BasicStringFunctionality()
        {
            Console.WriteLine("=> Basic String Functionality");

            string firstName = "Victor";
            Console.WriteLine("Value of firstname is {0}", firstName);
            Console.WriteLine("My first name is {0} characters", firstName.Length);
            Console.WriteLine("Hey look, uppercase: {0}", firstName.ToUpper());
            Console.WriteLine("Hey look, lowercase: {0}", firstName.ToLower());
            Console.WriteLine("Does firstName contain a v? {0}", firstName.ToLower().Contains("v"));
            Console.WriteLine("replaced part of the string: {0}", firstName.Replace("ctor", "ncent"));

            Console.WriteLine();
        }

        static void StringConcatenation()
        {
            Console.WriteLine("=> String Concatenation");

            string s1 = "Hello ";
            string s2 = "World!";
            string s3 = String.Concat(s1, s2);

            Console.WriteLine(s3);
            Console.WriteLine();
        }

        static void EscapeChars()
        {
            Console.WriteLine("=> Escape Characters\a");

            string strWithTabs = "Model\tColor\tSpeed";
            Console.WriteLine(strWithTabs);

            Console.WriteLine("Everyone loves \"Hello World\"");
            Console.WriteLine("C:\\_repos\\");

            Console.WriteLine();
        }

        static void VerbatimStrings()
        {
            Console.WriteLine("=> Verbatim Strings");

            Console.WriteLine(@"C:\_repos\");

            string myLongString = @"This is a very
                very 
                    very

                        long string";
            Console.WriteLine(myLongString);
            Console.WriteLine(@"Robin said: ""Carpe Diem!""");

            Console.WriteLine();
        }

        static void StringsAreImmutable()
        {
            Console.WriteLine("=> Strings are Immutable (unchanged by action)");

            string firstName = "Victor";

            string upperName = firstName.ToUpper();

            Console.WriteLine("{0} in upper case is {1}", firstName, upperName);

            Console.WriteLine();
        }

        static void FunWithStringBuilder()
        {
            Console.WriteLine("=> StringBuilder");

            StringBuilder sb = new StringBuilder("***** Fantastic Games *****", 256);

            sb.Append("\n");
            sb.AppendLine("Half Life");
            sb.AppendLine("Morrowind");

            Console.WriteLine(sb.ToString());

            sb.Replace("Half Life", "System Shock");

            Console.WriteLine(sb.ToString());
            Console.WriteLine("The length is {0}", sb.Length);

            Console.WriteLine();
        }

        //NEW METHOD...
        //THIS METHOD IS FOR YOU TO REVIEW AND COME TO ME WITH QUESTIONS IF YOU HAVE THEM
        static void StringEquality()
        {
            // write our header to console
            Console.WriteLine("=> String equality:");

            // declare a couple string variables
            string s1 = "Hello!";
            string s2 = "Yo!";

            // write those strings to console. 
            Console.WriteLine("s1 = {0}", s1);
            Console.WriteLine("s2 = {0}", s2);
            Console.WriteLine();

            // Test these strings for equality.
            Console.WriteLine("s1 == s2: {0}", s1 == s2);
            Console.WriteLine("s1 == Hello!: {0}", s1 == "Hello!");
            Console.WriteLine("s1 == HELLO!: {0}", s1 == "HELLO!");
            Console.WriteLine("s1 == hello!: {0}", s1 == "hello!");
            Console.WriteLine("s1.Equals(s2): {0}", s1.Equals(s2));
            Console.WriteLine("Yo.Equals(s2): {0}", "Yo!".Equals(s2));
            Console.ReadLine();
            Console.Clear();
        }

    }
}
