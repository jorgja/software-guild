﻿// path to our contacts web API controller
var uri = '/api/contacts/';

// when the document is loaded do this:
$(document).ready(function() {
    loadContacts();
});

function loadContacts() {
    // make an AJAX call that will return JSON
    $.getJSON(uri)
        // when that call is done do this:
        .done(function (data) {
            // remove all rows from table
            $('#contacts tbody tr').remove();

            // iterate all objects returned in web API call
            $.each(data, function(index, contact) {
                // create the HTML row for the data for a contact
                // append that HTML to the contacts table inside the tbody tag
                $(createRow(contact)).appendTo($('#contacts tbody'));
            });
        });
};

function createRow(contact) {
    // return HTML
    return '<tr><td>' + contact.ContactID + '</td><td>' + contact.Name +
        '</td><td>' + contact.PhoneNumber + '</td></tr>';
};