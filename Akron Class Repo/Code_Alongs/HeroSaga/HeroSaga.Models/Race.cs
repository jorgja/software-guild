﻿namespace HeroSaga.Models
{
    public enum Race
    {
        Human,
        Elf,
        Dawlf,
        Orc,
        Wizard

    }
}