using System;

namespace HeroSaga.Models
{
    public class Battle
    {
        public int Id { get; set; }
        public int CharacterId { get; set; }
        public string MonsterName { get; set; }
        public DateTime Date { get; set; }

        public virtual Character Character { get; set; }

    }
}