﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HeroSaga.Models;

namespace HeroSaga.Data
{
    public class CharacterRepository : ICharacterRepository
    {
        HeroSagaContext context = new HeroSagaContext();
        public List<Character> GetAll()
        {
            return context.Characters.ToList();
        }

        public Character GetById(int id)
        {
            return context.Characters.Find(id);
        }

        public Character Create(Character entity)
        {
            context.Characters.Add(entity);
            context.SaveChanges();
            return entity;
        }

        public void Update(int id, Character entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Characters.Remove(context.Characters.Find(id));
        }
    }
}
