using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using HeroSaga.Models;

namespace HeroSaga.Data
{
    public class BattlesRepository : IBattlesRepository
    {
        HeroSagaContext context = new HeroSagaContext();
        public List<Battle> GetAll()
        {
            return context.Battles.ToList();
        }

        public Battle GetById(int id)
        {
            return context.Battles.Find(id);
        }

        public Battle Create(Battle entity)
        {
            context.Battles.Add(entity);
            context.SaveChanges();
            return entity;
        }

        public void Update(int id, Battle entity)
        {
            context.Entry(entity).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Battles.Remove(context.Battles.Find(id));
        }
    }
}