using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using HeroSaga.Models;

namespace HeroSaga.Data
{
    public class ADOCharacterRepository : ICharacterRepository
    {
        private string _conStr;
        private const string SelectCharacters = "Select * From Characters ";
        public ADOCharacterRepository(string conStr)
        {
            _conStr = conStr;
        }

        public List<Character> GetAll()
        {
            List<Character> characters = new List<Character>();
            using (SqlConnection conn = new SqlConnection(_conStr))
            {
              
                SqlCommand cmd = new SqlCommand("Select * From Characters",conn);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Character character = new Character();
                    character.Id = int.Parse(dr["Id"].ToString());
                    character.Age = int.Parse(dr["Age"].ToString());
                    character.Race = (Race) int.Parse(dr["Race"].ToString());
                    characters.Add(character);
                }
            }
            return characters;
        }

        
        public Character GetById(int id)
        {
            List<Character> characters = new List<Character>();
            using (SqlConnection conn = new SqlConnection(_conStr))
            {
                SqlCommand cmd = new SqlCommand("Select * From Characters Where Id = @Id", conn);
                cmd.Parameters.AddWithValue("@Id", id);
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Character character = new Character();
                    character.Id = int.Parse(dr["Id"].ToString());
                    character.Age = int.Parse(dr["Age"].ToString());
                    character.Name = dr["Name"].ToString();
                    character.Race = (Race)int.Parse(dr["Race"].ToString());
                    characters.Add(character);
                }
            }
            return characters.FirstOrDefault();
        }

        public Character Create(Character entity)
        {
            throw new System.NotImplementedException();
        }

        public void Update(int id, Character entity)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}