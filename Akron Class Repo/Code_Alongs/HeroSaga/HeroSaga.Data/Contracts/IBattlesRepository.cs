using System.Collections.Generic;
using HeroSaga.Models;

namespace HeroSaga.Data
{
    public interface IBattlesRepository
    {
        List<Battle> GetAll();
        Battle GetById(int id);
        Battle Create(Battle entity);
        void Update(int id, Battle entity);
        void Delete(int id);
    }
}