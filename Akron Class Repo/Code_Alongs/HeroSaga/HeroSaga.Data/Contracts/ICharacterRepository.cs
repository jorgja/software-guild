using System.Collections.Generic;
using HeroSaga.Models;

namespace HeroSaga.Data
{
    public interface ICharacterRepository
    {
        List<Character> GetAll();
        Character GetById(int id);
        Character Create(Character entity);
        void Update(int id, Character entity);
        void Delete(int id);
    }
}