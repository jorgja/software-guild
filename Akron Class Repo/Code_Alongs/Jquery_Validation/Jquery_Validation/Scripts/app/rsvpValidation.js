﻿$(document).ready(function() {
    $('#rsvpForm2').validate({
        rules: {
            Name: {
                required: true
            },
            Email: {
                required: true,
                email: true
            },
            Phone: {
                required: true
            },
            FavoriteGame: {
                required: true
            },
            WillAttend: {
                required: true
            }
        },
        messages: {
            Name: "Enter your name",
            Email: {
                required: "Enter your email address",
                email: "That's not a format for email..."
            },
            Phone: "Enter your phone number",
            FavoriteGame: "Tell us your favorite game",
            WillAttend: "We need to know if you are coming or not!"
        }
    });
});