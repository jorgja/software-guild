﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework_DBFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            GetCustomersInUSA();
            AddRegion();

            Console.ReadLine();
        }

        private static void GetCustomersInUSA()
        {
            using (var context = new NorthwindEntities())
            {
                // PREPARES SQL STATEMENT
                var usaCustomers = context.Customers.Where(c => c.Country == "USA");

                // LOADS THE DATA HERE 
                foreach (var customer in usaCustomers)
                {
                    Console.WriteLine("{0} {1} - {2}", customer.CustomerID, customer.CompanyName,
                        customer.ContactName);
                }
            }
        }

        private static void AddRegion()
        {
            using (var context = new NorthwindEntities())
            {
                Region newRegion = new Region();
                newRegion.RegionDescription = "Entity Framework";

                context.Regions.Add(newRegion);
                context.SaveChanges();

                Console.WriteLine("{0} {1}", newRegion.RegionID, newRegion.RegionDescription);
            }
        }
    }
}
