﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SamuraiDI.Models;

namespace SamuraiDI
{
    class Program
    {
        static void Main(string[] args)
        {
            // constructor injection implementation
            Samurai warrior = new Samurai(new Sword());
            warrior.Attack("the evildoers");

            Samurai warrior2 = new Samurai(new Shuriken());
            warrior2.Attack("the bad guy");

            // property injection implementation
            warrior2.SecondaryWeapon = new Sword();
            warrior2.SecondaryAttack("the evildoers");
            warrior2.SecondaryWeapon.Hit("the evildoers");

            // method injection implementation
            warrior2.PickupItem(new Ration());
            warrior2.Items[0].Use();

            Console.ReadLine();
        }
    }
}
