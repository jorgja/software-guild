﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamuraiDI.Models
{
    public class Samurai
    {
        public List<IItem> Items { get; set; } 
        private readonly IWeapon _weapon;

        // property injection
        public IWeapon SecondaryWeapon { get; set; }

        // constructor injection
        public Samurai(IWeapon weapon)
        {
            _weapon = weapon;
            Items = new List<IItem>();
        }

        public void Attack(string target)
        {
            _weapon.Hit(target);
        }

        public void SecondaryAttack(string target)
        {
            SecondaryWeapon.Hit(target);
        }

        // method injection
        public void PickupItem(IItem item)
        {
            Items.Add(item);
        }
    }
}
