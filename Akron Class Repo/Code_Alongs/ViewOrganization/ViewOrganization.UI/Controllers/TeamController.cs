﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewOrganization.UI.Models;

namespace ViewOrganization.UI.Controllers
{
    public class TeamController : Controller
    {
        // GET: Team
        public ActionResult Index()
        {
            var players = new List<Player>
            {
                new Player {Name = "Lebron James", Number = 23, Position = "F"},
                new Player {Name = "Kyrie Irving", Number = 2, Position = "G"}
            };

            return View(players);
        }
    }
}