﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FunWithCollections.Collections;

namespace FunWithCollections
{
    class Program
    {
        static void Main(string[] args)
        {
            // all static methods
            NonGenerics.ShowArrayList();
            NonGenerics.ShowHashTable();
            NonGenerics.ShowStack();
            NonGenerics.ShowQueue();

            // all instance method
            Generics collections = new Generics();
            collections.ShowStack();
            collections.ShowQueue();
            collections.SimpleList();
            collections.PersonDictionary();

            Console.ReadLine();
        }
    }
}
