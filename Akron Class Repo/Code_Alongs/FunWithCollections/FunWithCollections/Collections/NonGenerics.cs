﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithCollections.Collections
{
    public class NonGenerics
    {
        public static void ShowArrayList()
        {
            Console.WriteLine("ShowArrayList");

            ArrayList arrayList = new ArrayList();

            arrayList.Add("Hello");
            arrayList.Add(123);
            arrayList.Add(new int[] {1, 2, 5});

            Person me = new Person();
            me.FirstName = "Victor";
            me.LastName = "Pudelski";

            arrayList.Add(me);

            foreach (object o in arrayList)
            {
                Console.WriteLine(o);
            }
        }

        public static void ShowHashTable()
        {
            Console.WriteLine("ShowHashTable");

            Hashtable map = new Hashtable();

            map.Add(1, "Hello");
            map.Add("World", 2);
            map.Add(true, 123.556);
            
            Person bart = new Person();
            bart.FirstName = "Bart";
            bart.LastName = "Simpson";

            map.Add("Simpsons", bart);

            // causes runtime error since key is already used. 
            //map.Add(1, "Hello World!");

            foreach (var key in map.Keys)
            {
                Console.WriteLine("{0} - {1}", key, map[key]);
            }

            Console.WriteLine("{0} - {1}", "World", map["World"]);
        }

        public static void ShowStack()
        {
            Console.WriteLine("ShowStack");

            Stack myStack = new Stack();
            myStack.Push("Hello");
            myStack.Push(123);
            
            Person kyrie = new Person();
            kyrie.FirstName = "Kyrie";
            kyrie.LastName = "Irving";

            myStack.Push(kyrie);

            int count = myStack.Count;
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine(myStack.Pop());
            }

        }

        public static void ShowQueue()
        {
            Console.WriteLine("ShowQueue");

            Queue myStack = new Queue();
            myStack.Enqueue("Hello");
            myStack.Enqueue(123);

            Person kyrie = new Person();
            kyrie.FirstName = "Kyrie";
            kyrie.LastName = "Irving";

            myStack.Enqueue(kyrie);

            int count = myStack.Count;
            for (int i = 0; i < count; i++)
            {
                Console.WriteLine(myStack.Dequeue());
            }

        }

    }
}
