﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunWithCollections.Collections
{
    public class Generics
    {
        public void ShowStack()
        {
            Console.WriteLine("\nGeneric ShowStack");

            Stack<Person> people = new Stack<Person>();

            people.Push(new Person {FirstName = "Homer", LastName = "Simpson"});
            people.Push(new Person {FirstName = "Marge", LastName = "Simpson"});
            people.Push(new Person {FirstName = "Bart", LastName = "Simpson"});

            int count = people.Count;
            Person simpson;
            for (int i = 0; i < count; i++)
            {
                simpson = people.Pop();

                Console.WriteLine("{0}, {1}", simpson.LastName, simpson.FirstName);
            }
        }

        public void ShowQueue()
        {
            Console.WriteLine("\nGeneric ShowQueue");

            Queue<Person> people = new Queue<Person>();

            people.Enqueue(new Person { FirstName = "Homer", LastName = "Simpson" });
            people.Enqueue(new Person { FirstName = "Marge", LastName = "Simpson" });
            people.Enqueue(new Person { FirstName = "Bart", LastName = "Simpson" });

            //*****NEW CODE
            Person topPerson = people.Peek();
            Console.WriteLine("First Element is {0}, {1}", topPerson.LastName, topPerson.FirstName);

            int count = people.Count;
            Person simpson;
            for (int i = 0; i < count; i++)
            {
                simpson = people.Dequeue();

                Console.WriteLine("{0}, {1}", simpson.LastName, simpson.FirstName);
            }
        }

        public void SimpleList()
        {
            Console.WriteLine("\nSimpleList");

            List<int> numbers = new List<int>();

            // List - 1
            numbers.Add(1);

            // List - 1, 5, 4, 3, 2
            numbers.AddRange(new int[] {5, 4, 3, 2});

            // List - 1, 5, 100, 4, 3, 2
            numbers.Insert(2, 100);

            foreach (int num in numbers)
            {
                Console.WriteLine(num);
            }

            // List - 1, 5, 100, 3, 2
            numbers.Remove(4);

            // List - 1, 5, 2
            numbers.RemoveRange(2, 2);

            // List - 5, 2
            numbers.RemoveAt(0);

            foreach (int num in numbers)
            {
                Console.WriteLine(num);
            }

        }

        public void PersonDictionary()
        {
            Console.WriteLine("\nPersonDictionary");

            Dictionary<string, Person> people = new Dictionary<string, Person>();

            Person kyrie = new Person {FirstName = "Kyrie", LastName = "Irving"};
            Person bart = new Person {FirstName = "Bart", LastName = "Simpson"};
            Person jason = new Person {FirstName = "Jason", LastName = "Kipnis"};

            people.Add(kyrie.LastName, kyrie);
            people.Add(bart.LastName, bart);
            people.Add(jason.LastName, jason);

            foreach (var person in people)
            {
                Console.WriteLine("{0}, {1}", person.Value.LastName, person.Value.FirstName);
            }

            Console.WriteLine("{0}, {1}", people["Kipnis"].LastName, people["Kipnis"].FirstName);
        }
    }
}
