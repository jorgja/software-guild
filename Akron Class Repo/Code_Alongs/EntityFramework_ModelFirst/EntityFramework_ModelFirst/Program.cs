﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace EntityFramework_ModelFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            LoadSampleData();
            DisplayPeople();

            Console.ReadLine();
        }

        private static void LoadSampleData()
        {
            using (var context = new ContactsContext())
            {
                if (context.People.Count() == 0)
                {
                    var person = new Person {FirstName = "Victor", LastName = "Pudelski", PhoneNumber = "555-5555"};
                    context.People.Add(person);

                    context.SaveChanges();
                }
            }
        }

        private static void DisplayPeople()
        {
            using (var context = new ContactsContext())
            {
                foreach (var person in context.People)
                {
                    Console.WriteLine("{0}, {1} - {2}", person.LastName, person.FirstName, person.PhoneNumber);
                }
            }
        }

    }
}
