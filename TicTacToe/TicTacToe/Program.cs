﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameLogic;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            Logic logicObject = new Logic();
            var game = new Program();

            Console.Write("Enter your name player 1: ");
            string p1 = Console.ReadLine();
            Console.Write("Enter your name player 2: ");
            string p2 = Console.ReadLine();
            string[,] boardState = { { "1", "2", "3" }, { "4", "5", "6" }, { "7", "8", "9" } };
            game.drawBoard(boardState);

            bool openSpace = false;
            int number = 0;
            int turn = 0;
            string marker = "x";
            bool winner = false;

            do
            {
                do
                {
                    if (turn % 2 == 0)  //Tracking turn and respective marker
                        marker = "x";
                    else marker = "o";
                    turn++;
                    openSpace = false;
                    do
                    {
                        if (marker == "x")
                            Console.Write("Place a x, {0}: ", p1);  //Collecting user input
                        else Console.WriteLine("Place an o, {0}: ", p2);
                        string input = Console.ReadLine();
                        number = 0;
                        bool result = int.TryParse(input, out number);  //Makes sure number is on the board
                        if (number >= 1 && number <= 9)
                            foreach (string s in boardState)  //Checks if the space is occupied
                            {
                                if(boardState[(number - 1) / 3, (number - 1) % 3] == "x" || boardState[(number - 1) / 3, (number - 1) % 3] == "o")
                                {
                                    Console.WriteLine("That space is taken.");
                                    break;
                                }
                                else if (input == s && (s != "x" || s != "o"))
                                {
                                    boardState[(number - 1) / 3, (number - 1) % 3] = marker;
                                    openSpace = true;
                                    break;
                                }
                            }
                        else Console.WriteLine("That's not a valid space.");
                    }
                    while (openSpace != true);
                    if(logicObject.checkForWin(boardState) == true)
                    {
                        winner = true;
                        break;
                    }
                    if (turn == 9 && winner == false)  //Checking for tie
                    {
                        game.drawBoard(boardState);
                        Console.WriteLine("It's a tie!  You both lose!");
                        Console.Read();
                        return;
                    }
                }
                while (openSpace != true);
                game.drawBoard(boardState);
            }
            while (winner != true);
            if (marker == "x")
                Console.WriteLine("{0} wins!", p1);
            else Console.WriteLine("{0} wins!", p2);
            Console.Read();
        }

        public void drawBoard(string[,] boardState)
        {
            Console.WriteLine("     |     |     ");
            Console.WriteLine("  {0}  |  {1}  |  {2}  ", boardState[0, 0], boardState[0, 1], boardState[0, 2]);
            Console.WriteLine("_____|_____|_____");
            Console.WriteLine("     |     |     ");
            Console.WriteLine("  {0}  |  {1}  |  {2}  ", boardState[1, 0], boardState[1, 1], boardState[1, 2]);
            Console.WriteLine("_____|_____|_____");
            Console.WriteLine("     |     |     ");
            Console.WriteLine("  {0}  |  {1}  |  {2}  ", boardState[2, 0], boardState[2, 1], boardState[2, 2]);
            Console.WriteLine("     |     |     ");
        }
    }
}
