﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameLogic
{
    public class Logic
    {
        public bool horizontalWin(string[,] boardState)
        {
            bool result = false;
            int number = 0;
            for (int i = 0; i < 3; i++)
            {
                result = int.TryParse(boardState[i, 0], out number);
                if (result == false)
                    if ((boardState[i, 0] == boardState[i, 1]) && (boardState[i, 1] == boardState[i, 2]))
                        return true;
            }
            return false;
        }

        public bool verticalWin(string[,] boardState)
        {
            bool result = false;
            int number = 0;
            for (int i = 0; i < 3; i++)
            {
                result = int.TryParse(boardState[0, i], out number);
                if (result == false)
                    if ((boardState[0, i] == boardState[1, i]) && (boardState[1, i] == boardState[2, i]))
                        return true;
            }
            return false;
        }

        public bool diaganolWin(string[,] boardState)
        {
            bool result = false;
            int number = 0;
            result = int.TryParse(boardState[1, 1], out number);
            if (result == false)
                if ((boardState[0, 0] == boardState[1, 1]) && (boardState[1, 1] == boardState[2, 2]))
                    return true;
            if (result == false)
                if ((boardState[2, 0] == boardState[1, 1]) && (boardState[1, 1] == boardState[0, 2]))
                    return true;
            return false;
        }
        public bool checkForWin(string[,] boardState)
        {
            if (horizontalWin(boardState) == true)
                return true;
            if (verticalWin(boardState) == true)
                return true;
            if (diaganolWin(boardState) == true)
                return true;
            return false;
        }
    }
}
