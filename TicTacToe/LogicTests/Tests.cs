﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GameLogic;

namespace LogicTests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void horizontalWin()
        {
            // Arrange
            Logic logicObject = new Logic();
            string[,] boardState = { { "1", "2", "3" }, { "x", "x", "x" }, { "7", "8", "9" } };
            bool expected = true;

            // Act
            bool actual = logicObject.horizontalWin(boardState);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void verticalWin()
        {
            // Arrange
            Logic logicObject = new Logic();
            string[,] boardState = { { "1", "o", "3" }, { "4", "o", "6" }, { "7", "o", "9" } };
            bool expected = true;

            // Act
            bool actual = logicObject.verticalWin(boardState);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void diagonalWin()
        {
            // Arrange
            Logic logicObject = new Logic();
            string[,] boardState = { { "1", "2", "x" }, { "4", "x", "6" }, { "x", "8", "9" } };
            bool expected = true;

            // Act
            bool actual = logicObject.diaganolWin(boardState);

            // Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void checkForWin()
        {
            // Arrange
            Logic logicObject = new Logic();
            string[,] boardState = { { "x", "x", "x" }, { "4", "5", "6" }, { "7", "8", "9" } };
            bool expected = true;

            // Act
            bool actual = logicObject.checkForWin(boardState);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
