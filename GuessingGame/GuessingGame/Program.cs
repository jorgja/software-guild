﻿using System;

namespace GuessingGame
{
    class Program
    {
        static void Main()
        {
            int theAnswer;
            int playerGuess;
            int guessCount = 0;
            string playerInput;
            bool isNumberGuessed = false;

            Console.WriteLine("What's your name?");
            string playerName = Console.ReadLine();

            Random r = new Random(); // This is System's built in random number generator
            theAnswer = r.Next(1, 21); // Get a random from 1 to 20

            do
            {
                // get player input
                Console.Write("{0}, enter your guess, enter Q to quit: ", playerName);
                playerInput = Console.ReadLine();

                if (playerInput == "Q")
                {
                    break;
                }

                // attempt to convert to number
                if (int.TryParse(playerInput, out playerGuess))
                {
                    if (playerGuess < 1 || playerGuess > 20)
                    {
                        Console.WriteLine("Enter a guess between 1 and 20 {0}", playerName);
                        continue;
                    }

                    // see if they won
                    if (playerGuess == theAnswer)
                    {
                        if (guessCount == 0)
                        {
                            Console.WriteLine("You guessed it on your first try, hacker!");
                            isNumberGuessed = true;
                        }
                        else
                        {
                            Console.WriteLine("You got it, {0}!", playerName);
                            isNumberGuessed = true;
                        }

                    }
                    else
                    {
                        // they didn't win, so were they too high or too low?
                        if (playerGuess > theAnswer)
                            Console.WriteLine("Too high!");
                        else
                            Console.WriteLine("Too low!");
                    }
                }
                else
                {
                    // At this point we know it's not a number.....

                    // learn to play n00b
                    Console.WriteLine("That wasn't a valid number {0}! Get a real job!", playerName);
                }
                guessCount++;
                if (guessCount == 1 && isNumberGuessed == false)
                    Console.WriteLine("You have taken 1 guess.");
                else
                    if (isNumberGuessed == false)
                    Console.WriteLine("You have taken {0} guesses.", guessCount);
            } while (!isNumberGuessed);

            Console.WriteLine("Press any key to quit.");
            Console.ReadLine();
        }
    }
}
