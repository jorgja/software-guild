﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("What number would you like to factor? ");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("The factors of {0} are:", num);
            int[] factors = new int[num];  //stores factor values
            int factorCount = 0;

            for (int i = 1; i <= num; i++)
            {
                if (num % i == 0)
                {
                    if(i != num)
                        Console.WriteLine(i);
                    factors[factorCount] = i;
                    factorCount++;
                    if (factorCount == 2 && i == num)
                        Console.WriteLine("{0} is a prime number!", num);
                }
            }

            int total = 0;
            int count = 0;
            foreach (int j in factors)
            {
                total = total + j;
                count++;
                if (total - num == num  && count == num)
                    Console.WriteLine("{0} is a perfect number!", num);
            }
            Console.Read();
        }
    }
}
