ALTER TABLE Ratings
ALTER COLUMN Rating varchar(10) NOT NULL

INSERT INTO Ratings (Rating)
VALUES ('G'), ('PG'), ('PG-13'), ('R'), ('NC-17')

INSERT INTO Studios (StudioName) VALUES('Universal Pictures')

INSERT INTO FilmParticipants(FirstName, LastName) VALUES('Adam', 'Sandler'), ('Dennis', 'Dugan')

INSERT INTO FilmRoles (RoleType) VALUES('Actor'), ('Director')

INSERT INTO FilmParticipantRoles(DVDID, FilmRoleID, FilmParticipantID) VALUES(1,1,1), (1,2,2)

INSERT INTO DVDs (Title, ReleaseDate, MPPARating, Studio, OwnerNotes, OwnerRating)
VALUES ('Happy Gilmore', '1996-02-16', 3, 1, 'You eat pieces of shit for breakfast?', 4)

INSERT INTO FilmParticipants(FirstName, LastName) VALUES('Al', 'Pacino'), ('Brian', 'De Palma')

INSERT INTO FilmParticipantRoles(DVDID, FilmRoleID, FilmParticipantID) VALUES(2,1,3), (2,2,4)

INSERT INTO DVDs (Title, ReleaseDate, MPPARating, Studio, OwnerNotes, OwnerRating)
VALUES ('Scarface', '1983-12-09', 4, 1, 'Say hello to my little friend!', 4)

INSERT INTO Studios (StudioName) VALUES('Castle Rock Entertainment')

INSERT INTO FilmParticipants(FirstName, LastName) VALUES('Tim', 'Robbins'), ('Frank', 'Darabont')

INSERT INTO FilmParticipantRoles(DVDID, FilmRoleID, FilmParticipantID) VALUES(3,1,5), (3,2,6)

INSERT INTO DVDs (Title, ReleaseDate, MPPARating, Studio, OwnerNotes, OwnerRating)
VALUES ('The Shawshank Redemption', '1994-10-04', 4, 2, 'Get busy living, or get busy dying.', 4)

INSERT INTO Studios (StudioName) VALUES('Miramax')

INSERT INTO FilmParticipants(FirstName, LastName) VALUES('John', 'Travolta'), ('Quentin', 'Tarintino')

INSERT INTO FilmParticipantRoles(DVDID, FilmRoleID, FilmParticipantID) VALUES(4,1,7), (4,2,8)

INSERT INTO DVDs (Title, ReleaseDate, MPPARating, Studio, OwnerNotes, OwnerRating)
VALUES ('Pulp Fiction', '1994-10-14', 4, 3, 'English motherfucker! Do you speak it?', 5)

INSERT INTO Studios (StudioName) VALUES('Fox 2000 Pictures')

INSERT INTO FilmParticipants(FirstName, LastName) VALUES('Brad', 'Pitt'), ('David', 'Fincher')

INSERT INTO FilmParticipantRoles(DVDID, FilmRoleID, FilmParticipantID) VALUES(5,1,9), (5,2,10)

INSERT INTO DVDs (Title, ReleaseDate, MPPARating, Studio, OwnerNotes, OwnerRating)
VALUES ('Fight Club', '1999-10-15', 4, 4, 'You do not talk about Fight Club.', 4)

INSERT INTO Borrowers (FirstName, LastName)
VALUES('Jake', 'Jorgenson'), ('Victor', 'Pudelski'), ('John', 'Doe')