USE [DVD Library]
GO

/****** Object:  StoredProcedure [dbo].[AddNewFilmParticipant]    Script Date: 4/10/2016 6:50:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddNewFilmParticipant] 
	-- Add the parameters for the stored procedure here
	@FirstName varchar(20),
	@LastName varchar(25),
	@DVDID int,
	@FilmRoleID int,
	@FilmParticipantID int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO FilmParticipants (FirstName, LastName) VALUES (@FirstName, @LastName);
		
	SET @FilmParticipantID = SCOPE_IDENTITY();

	EXEC AddExistingFilmParticipant @FilmParticipantID, @DVDID, @FilmRoleID;
    
END

GO

