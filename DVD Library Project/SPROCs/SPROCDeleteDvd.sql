USE [DVD Library]
GO
/****** Object:  StoredProcedure [dbo].[DeleteDvd]    Script Date: 4/14/2016 12:14:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[DeleteDvd]
	-- Add the parameters for the stored procedure here
	@DvdID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	Delete from [dbo].[DVDs]
	Where DVDID = @DvdID
	Delete from [dbo].[BorrowHistory]
	where DVDID = @DvdID
	Delete from [dbo].[FilmParticipantRoles]
	where DVDID = @DvdID

	Set @DvdID = SCOPE_IDENTITY();
END
