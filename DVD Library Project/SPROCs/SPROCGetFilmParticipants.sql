USE [DVD Library]
GO

/****** Object:  StoredProcedure [dbo].[GetParticipantsForDVDID]    Script Date: 4/10/2016 6:49:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetParticipantsForDVDID]
	-- Add the parameters for the stored procedure here
	@DVDID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT fp.LastName, fp.FirstName, fp.FilmParticipantID, fr.RoleType, fr.FilmRoleID FROM DVDs d
		INNER JOIN FilmParticipantRoles fpr ON fpr.DVDID = d.DVDID
		INNER JOIN FilmParticipants fp ON fp.FilmParticipantID = fpr.FilmParticipantID
		INNER JOIN FilmRoles fr ON fr.FilmRoleID = fpr.FilmRoleID
		WHERE d.DVDID = @DVDID
END

GO

