USE [DVD Library]
GO

/****** Object:  StoredProcedure [dbo].[AddExistingFilmParticipant]    Script Date: 4/10/2016 6:50:11 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddExistingFilmParticipant] 
	-- Add the parameters for the stored procedure here
	@FilmParticipantID int,
	@DVDID int,
	@FilmRoleID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF (SELECT COUNT (*) FROM FilmParticipantRoles fpr WHERE fpr.DVDID = @DVDID AND fpr.FilmParticipantID = @FilmParticipantID AND fpr.FilmRoleID = @FilmRoleID) = 0
		INSERT INTO FilmParticipantRoles (DVDID, FilmRoleID, FilmParticipantID) VALUES (@DVDID, @FilmRoleID, @FilmParticipantID);   
END

GO

