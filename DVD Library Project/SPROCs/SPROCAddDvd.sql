-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE AddDvd 
	-- Add the parameters for the stored procedure here
	 @Title varchar(50),
	 @ReleaseDate date,
	 @MpaaRating int,
	 @Studio varchar(50),
	 @OwnerRating int,
	 @OwnerNotes varchar(200),
	 @DvdID int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @StudioID int
	Set @StudioID = (Select StudioID From Studios 
	Where @Studio = StudioName)
	Insert Into DVDs (Title, ReleaseDate, MPPARating, Studio, OwnerRating, OwnerNotes) 
	Values (@Title, @ReleaseDate, @MpaaRating, @StudioID, @OwnerRating, @OwnerNotes)

	Set @DvdID = SCOPE_IDENTITY();
END
GO
