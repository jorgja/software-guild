USE [DVDTest]
GO

/****** Object:  StoredProcedure [dbo].[DANDCDVDT]    Script Date: 4/14/2016 4:20:45 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DANDCDVDT]

AS
BEGIN

IF object_id('DVDs') is not null ALTER TABLE [dbo].[DVDs] DROP CONSTRAINT [FK_DVDs_Studios]

IF object_id('DVDs') is not null ALTER TABLE [dbo].[DVDs] DROP CONSTRAINT [FK_DVDs_Ratings]

/****** Object:  Table [dbo].[Studios]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('Studios') is not null DROP TABLE [dbo].[Studios]

/****** Object:  Table [dbo].[Ratings]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('Ratings') is not null DROP TABLE [dbo].[Ratings]

/****** Object:  Table [dbo].[FilmRoles]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('FilmRoles') is not null DROP TABLE [dbo].[FilmRoles]

/****** Object:  Table [dbo].[FilmParticipants]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('FilmParticipants') is not null DROP TABLE [dbo].[FilmParticipants]

/****** Object:  Table [dbo].[FilmParticipantRoles]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('FilmParticipantRoles') is not null DROP TABLE [dbo].[FilmParticipantRoles]

/****** Object:  Table [dbo].[DVDs]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('DVDs') is not null DROP TABLE [dbo].[DVDs]

/****** Object:  Table [dbo].[BorrowHistory]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('BorrowHistory') is not null DROP TABLE [dbo].[BorrowHistory]

/****** Object:  Table [dbo].[Borrowers]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('Borrowers') is not null DROP TABLE [dbo].[Borrowers]

/****** Object:  Table [dbo].[Borrowers]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[Borrowers](
	[BorrowerID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [varchar](25) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BorrowerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[BorrowHistory]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[BorrowHistory](
	[LoanID] [int] IDENTITY(1,1) NOT NULL,
	[BorrowerID] [int] NOT NULL,
	[DVDID] [int] NOT NULL,
	[TakenDate] [date] NOT NULL,
	[ReturnDate] [date] NULL,
 CONSTRAINT [PK_BorrowHistory] PRIMARY KEY CLUSTERED 
(
	[LoanID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



/****** Object:  Table [dbo].[DVDs]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[DVDs](
	[DVDID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[ReleaseDate] [date] NOT NULL,
	[MPPARating] [int] NOT NULL,
	[Studio] [int] NOT NULL,
	[OwnerRating] [int] NULL,
	[OwnerNotes] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[DVDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FilmParticipantRoles]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[FilmParticipantRoles](
	[DVDID] [int] NULL,
	[FilmRoleID] [int] NULL,
	[FilmParticipantID] [int] NULL
) ON [PRIMARY]


/****** Object:  Table [dbo].[FilmParticipants]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FilmParticipants](
	[FilmParticipantID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [varchar](25) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FilmParticipantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FilmRoles]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FilmRoles](
	[FilmRoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleType] [varchar](30) NOT NULL
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[Ratings]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[Ratings](
	[RatingsID] [int] IDENTITY(1,1) NOT NULL,
	[Rating] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RatingsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[Studios]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[Studios](
	[StudioID] [int] IDENTITY(1,1) NOT NULL,
	[StudioName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[StudioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

SET IDENTITY_INSERT [dbo].[Borrowers] ON 


INSERT [dbo].[Borrowers] ([BorrowerID], [LastName], [FirstName]) VALUES (1, N'Jorgenson', N'Jake')

INSERT [dbo].[Borrowers] ([BorrowerID], [LastName], [FirstName]) VALUES (2, N'Pudelski', N'Victor')

INSERT [dbo].[Borrowers] ([BorrowerID], [LastName], [FirstName]) VALUES (3, N'Doe', N'John')

SET IDENTITY_INSERT [dbo].[Borrowers] OFF

SET IDENTITY_INSERT [dbo].[DVDs] ON 


INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (1, N'Happy Gilmore', CAST(N'1996-02-16' AS Date), 3, 1, 4, N'You eat pieces of shit for breakfast?')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (2, N'Scarface', CAST(N'1983-12-09' AS Date), 4, 1, 4, N'Say hello to my little friend!')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (3, N'The Shawshank Redemption', CAST(N'1994-10-04' AS Date), 4, 2, 4, N'Get busy living, or get busy dying.')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (4, N'Pulp Fiction', CAST(N'1994-10-14' AS Date), 4, 3, 5, N'English motherfucker! Do you speak it?')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (5, N'Fight Club', CAST(N'1999-10-15' AS Date), 4, 4, 4, N'You do not talk about Fight Club.')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (7, N'The Great Gatsby', CAST(N'2016-04-07' AS Date), 3, 2, 1, N'ff')

SET IDENTITY_INSERT [dbo].[DVDs] OFF

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 1, 1)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 2)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (2, 1, 3)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (2, 2, 4)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (3, 1, 5)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (3, 2, 6)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (4, 1, 7)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (4, 2, 8)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (5, 1, 9)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (5, 2, 10)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 1)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 11)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 12)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 13)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 14)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (2, 1, 1)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (7, 1, 2)

SET IDENTITY_INSERT [dbo].[FilmParticipants] ON 


INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (1, N'Sandler', N'Adam')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (2, N'Dugan', N'Dennis')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (3, N'Pacino', N'Al')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (4, N'De Palma', N'Brian')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (5, N'Robbins', N'Tim')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (6, N'Darabont', N'Frank')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (7, N'Travolta', N'John')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (8, N'Tarintino', N'Quentin')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (9, N'Pitt', N'Brad')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (10, N'Fincher', N'David')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (11, N'Williams', N'John')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (12, N'Williams', N'John')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (13, N'Williams', N'John')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (14, N'Mueller', N'TJ')

SET IDENTITY_INSERT [dbo].[FilmParticipants] OFF

SET IDENTITY_INSERT [dbo].[FilmRoles] ON 


INSERT [dbo].[FilmRoles] ([FilmRoleID], [RoleType]) VALUES (1, N'Actor')

INSERT [dbo].[FilmRoles] ([FilmRoleID], [RoleType]) VALUES (2, N'Director')

SET IDENTITY_INSERT [dbo].[FilmRoles] OFF

SET IDENTITY_INSERT [dbo].[Ratings] ON 


INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (1, N'G')

INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (2, N'PG')

INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (3, N'PG-13')

INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (4, N'R')

INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (5, N'NC-17')

SET IDENTITY_INSERT [dbo].[Ratings] OFF

SET IDENTITY_INSERT [dbo].[BorrowHistory] ON 

INSERT [dbo].[BorrowHistory] ([LoanID], [BorrowerID], [DVDID], [TakenDate], [ReturnDate]) VALUES (1, 1, 1, CAST(N'2016-04-11' AS Date), CAST(N'2016-04-13' AS Date))

INSERT [dbo].[BorrowHistory] ([LoanID], [BorrowerID], [DVDID], [TakenDate], [ReturnDate]) VALUES (3, 1, 1, CAST(N'2016-04-14' AS Date), CAST(N'2016-04-14' AS Date))

INSERT [dbo].[BorrowHistory] ([LoanID], [BorrowerID], [DVDID], [TakenDate], [ReturnDate]) VALUES (7, 6, 1, CAST(N'2016-04-14' AS Date), CAST(N'2016-04-14' AS Date))

INSERT [dbo].[BorrowHistory] ([LoanID], [BorrowerID], [DVDID], [TakenDate], [ReturnDate]) VALUES (8, 7, 1, CAST(N'2016-04-14' AS Date), CAST(N'2016-04-14' AS Date))

INSERT [dbo].[BorrowHistory] ([LoanID], [BorrowerID], [DVDID], [TakenDate], [ReturnDate]) VALUES (9, 5, 1, CAST(N'2016-04-14' AS Date), CAST(N'2016-04-14' AS Date))

INSERT [dbo].[BorrowHistory] ([LoanID], [BorrowerID], [DVDID], [TakenDate], [ReturnDate]) VALUES (10, 1, 2, CAST(N'2016-04-14' AS Date), CAST(N'2016-04-14' AS Date))

SET IDENTITY_INSERT [dbo].[BorrowHistory] OFF

SET IDENTITY_INSERT [dbo].[Studios] ON 


INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (1, N'Universal Pictures')

INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (2, N'Castle Rock Entertainment')

INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (3, N'Miramax')

INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (4, N'Fox 2000 Pictures')

SET IDENTITY_INSERT [dbo].[Studios] OFF

ALTER TABLE [dbo].[DVDs]  WITH CHECK ADD  CONSTRAINT [FK_DVDs_Ratings] FOREIGN KEY([MPPARating])
REFERENCES [dbo].[Ratings] ([RatingsID])

ALTER TABLE [dbo].[DVDs] CHECK CONSTRAINT [FK_DVDs_Ratings]

ALTER TABLE [dbo].[DVDs]  WITH CHECK ADD  CONSTRAINT [FK_DVDs_Studios] FOREIGN KEY([Studio])
REFERENCES [dbo].[Studios] ([StudioID])

ALTER TABLE [dbo].[DVDs] CHECK CONSTRAINT [FK_DVDs_Studios]


END



GO


