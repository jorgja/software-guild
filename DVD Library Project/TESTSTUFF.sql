USE [master]
GO
/****** Object:  Database [DVDTest]    Script Date: 4/14/2016 2:58:45 PM ******/
CREATE DATABASE [DVDTest]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DVDTest', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\DVDTest.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DVDTest_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\DVDTest_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DVDTest] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DVDTest].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DVDTest] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DVDTest] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DVDTest] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DVDTest] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DVDTest] SET ARITHABORT OFF 
GO
ALTER DATABASE [DVDTest] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DVDTest] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DVDTest] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DVDTest] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DVDTest] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DVDTest] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DVDTest] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DVDTest] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DVDTest] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DVDTest] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DVDTest] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DVDTest] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DVDTest] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DVDTest] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DVDTest] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DVDTest] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DVDTest] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DVDTest] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DVDTest] SET  MULTI_USER 
GO
ALTER DATABASE [DVDTest] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DVDTest] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DVDTest] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DVDTest] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DVDTest] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DVDTest]
GO
/****** Object:  Table [dbo].[Borrowers]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Borrowers](
	[BorrowerID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [varchar](25) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BorrowerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BorrowHistory]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BorrowHistory](
	[BorrowerID] [int] NOT NULL,
	[DVDID] [int] NOT NULL,
	[TakenDate] [date] NOT NULL,
	[ReturnDate] [date] NULL,
 CONSTRAINT [pk_LoanID] PRIMARY KEY CLUSTERED 
(
	[BorrowerID] ASC,
	[DVDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DVDs]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DVDs](
	[DVDID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[ReleaseDate] [date] NOT NULL,
	[MPPARating] [int] NOT NULL,
	[Studio] [int] NOT NULL,
	[OwnerRating] [int] NULL,
	[OwnerNotes] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[DVDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FilmParticipantRoles]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FilmParticipantRoles](
	[DVDID] [int] NULL,
	[FilmRoleID] [int] NULL,
	[FilmParticipantID] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FilmParticipants]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FilmParticipants](
	[FilmParticipantID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [varchar](25) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FilmParticipantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FilmRoles]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FilmRoles](
	[FilmRoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleType] [varchar](30) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ratings]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ratings](
	[RatingsID] [int] IDENTITY(1,1) NOT NULL,
	[Rating] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RatingsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Studios]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Studios](
	[StudioID] [int] IDENTITY(1,1) NOT NULL,
	[StudioName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[StudioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Borrowers] ON 

GO
INSERT [dbo].[Borrowers] ([BorrowerID], [LastName], [FirstName]) VALUES (1, N'Jorgenson', N'Jake')
GO
INSERT [dbo].[Borrowers] ([BorrowerID], [LastName], [FirstName]) VALUES (2, N'Pudelski', N'Victor')
GO
INSERT [dbo].[Borrowers] ([BorrowerID], [LastName], [FirstName]) VALUES (3, N'Doe', N'John')
GO
SET IDENTITY_INSERT [dbo].[Borrowers] OFF
GO
SET IDENTITY_INSERT [dbo].[DVDs] ON 

GO
INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (1, N'Happy Gilmore', CAST(N'1996-02-16' AS Date), 3, 1, 4, N'You eat pieces of shit for breakfast?')
GO
INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (2, N'Scarface', CAST(N'1983-12-09' AS Date), 4, 1, 4, N'Say hello to my little friend!')
GO
INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (3, N'The Shawshank Redemption', CAST(N'1994-10-04' AS Date), 4, 2, 4, N'Get busy living, or get busy dying.')
GO
INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (4, N'Pulp Fiction', CAST(N'1994-10-14' AS Date), 4, 3, 5, N'English motherfucker! Do you speak it?')
GO
INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (5, N'Fight Club', CAST(N'1999-10-15' AS Date), 4, 4, 4, N'You do not talk about Fight Club.')
GO
INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (7, N'The Great Gatsby', CAST(N'2016-04-07' AS Date), 3, 2, 1, N'ff')
GO
SET IDENTITY_INSERT [dbo].[DVDs] OFF
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 1, 1)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 2)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (2, 1, 3)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (2, 2, 4)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (3, 1, 5)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (3, 2, 6)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (4, 1, 7)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (4, 2, 8)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (5, 1, 9)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (5, 2, 10)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 1)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 11)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 12)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 13)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 14)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (2, 1, 1)
GO
INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (7, 1, 2)
GO
SET IDENTITY_INSERT [dbo].[FilmParticipants] ON 

GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (1, N'Sandler', N'Adam')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (2, N'Dugan', N'Dennis')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (3, N'Pacino', N'Al')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (4, N'De Palma', N'Brian')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (5, N'Robbins', N'Tim')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (6, N'Darabont', N'Frank')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (7, N'Travolta', N'John')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (8, N'Tarintino', N'Quentin')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (9, N'Pitt', N'Brad')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (10, N'Fincher', N'David')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (11, N'Williams', N'John')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (12, N'Williams', N'John')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (13, N'Williams', N'John')
GO
INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (14, N'Mueller', N'TJ')
GO
SET IDENTITY_INSERT [dbo].[FilmParticipants] OFF
GO
SET IDENTITY_INSERT [dbo].[FilmRoles] ON 

GO
INSERT [dbo].[FilmRoles] ([FilmRoleID], [RoleType]) VALUES (1, N'Actor')
GO
INSERT [dbo].[FilmRoles] ([FilmRoleID], [RoleType]) VALUES (2, N'Director')
GO
SET IDENTITY_INSERT [dbo].[FilmRoles] OFF
GO
SET IDENTITY_INSERT [dbo].[Ratings] ON 

GO
INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (1, N'G')
GO
INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (2, N'PG')
GO
INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (3, N'PG-13')
GO
INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (4, N'R')
GO
INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (5, N'NC-17')
GO
SET IDENTITY_INSERT [dbo].[Ratings] OFF
GO
SET IDENTITY_INSERT [dbo].[Studios] ON 

GO
INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (1, N'Universal Pictures')
GO
INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (2, N'Castle Rock Entertainment')
GO
INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (3, N'Miramax')
GO
INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (4, N'Fox 2000 Pictures')
GO
SET IDENTITY_INSERT [dbo].[Studios] OFF
GO
ALTER TABLE [dbo].[DVDs]  WITH CHECK ADD  CONSTRAINT [FK_DVDs_Ratings] FOREIGN KEY([MPPARating])
REFERENCES [dbo].[Ratings] ([RatingsID])
GO
ALTER TABLE [dbo].[DVDs] CHECK CONSTRAINT [FK_DVDs_Ratings]
GO
ALTER TABLE [dbo].[DVDs]  WITH CHECK ADD  CONSTRAINT [FK_DVDs_Studios] FOREIGN KEY([Studio])
REFERENCES [dbo].[Studios] ([StudioID])
GO
ALTER TABLE [dbo].[DVDs] CHECK CONSTRAINT [FK_DVDs_Studios]
GO
/****** Object:  StoredProcedure [dbo].[AddDvd]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddDvd] 
	-- Add the parameters for the stored procedure here
	 @Title varchar(50),
	 @ReleaseDate date,
	 @MpaaRating int,
	 @Studio varchar(50),
	 @OwnerRating int,
	 @OwnerNotes varchar(200),
	 @DvdID int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Declare @StudioID int
	Set @StudioID = (Select StudioID From Studios 
	Where @Studio = StudioName)
	Insert Into DVDs (Title, ReleaseDate, MPPARating, Studio, OwnerRating, OwnerNotes) 
	Values (@Title, @ReleaseDate, @MpaaRating, @StudioID, @OwnerRating, @OwnerNotes)

	Set @DvdID = SCOPE_IDENTITY();
END


GO
/****** Object:  StoredProcedure [dbo].[AddExistingFilmParticipant]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddExistingFilmParticipant] 
	-- Add the parameters for the stored procedure here
	@FilmParticipantID int,
	@DVDID int,
	@FilmRoleID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF (SELECT COUNT (*) FROM FilmParticipantRoles fpr WHERE fpr.DVDID = @DVDID AND fpr.FilmParticipantID = @FilmParticipantID AND fpr.FilmRoleID = @FilmRoleID) = 0
		INSERT INTO FilmParticipantRoles (DVDID, FilmRoleID, FilmParticipantID) VALUES (@DVDID, @FilmRoleID, @FilmParticipantID);   
END


GO
/****** Object:  StoredProcedure [dbo].[AddNewFilmParticipant]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddNewFilmParticipant] 
	-- Add the parameters for the stored procedure here
	@FirstName varchar(20),
	@LastName varchar(25),
	@DVDID int,
	@FilmRoleID int,
	@FilmParticipantID int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO FilmParticipants (FirstName, LastName) VALUES (@FirstName, @LastName);
		
	SET @FilmParticipantID = SCOPE_IDENTITY();

	EXEC AddExistingFilmParticipant @FilmParticipantID, @DVDID, @FilmRoleID;
    
END


GO
/****** Object:  StoredProcedure [dbo].[DANDCDVDT]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DANDCDVDT]

AS
BEGIN

IF object_id('DVDs') is not null ALTER TABLE [dbo].[DVDs] DROP CONSTRAINT [FK_DVDs_Studios]

IF object_id('DVDs') is not null ALTER TABLE [dbo].[DVDs] DROP CONSTRAINT [FK_DVDs_Ratings]

/****** Object:  Table [dbo].[Studios]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('Studios') is not null DROP TABLE [dbo].[Studios]

/****** Object:  Table [dbo].[Ratings]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('Ratings') is not null DROP TABLE [dbo].[Ratings]

/****** Object:  Table [dbo].[FilmRoles]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('FilmRoles') is not null DROP TABLE [dbo].[FilmRoles]

/****** Object:  Table [dbo].[FilmParticipants]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('FilmParticipants') is not null DROP TABLE [dbo].[FilmParticipants]

/****** Object:  Table [dbo].[FilmParticipantRoles]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('FilmParticipantRoles') is not null DROP TABLE [dbo].[FilmParticipantRoles]

/****** Object:  Table [dbo].[DVDs]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('DVDs') is not null DROP TABLE [dbo].[DVDs]

/****** Object:  Table [dbo].[BorrowHistory]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('BorrowHistory') is not null DROP TABLE [dbo].[BorrowHistory]

/****** Object:  Table [dbo].[Borrowers]    Script Date: 4/14/2016 2:11:18 PM ******/
IF object_id('Borrowers') is not null DROP TABLE [dbo].[Borrowers]

/****** Object:  Table [dbo].[Borrowers]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[Borrowers](
	[BorrowerID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [varchar](25) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BorrowerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[BorrowHistory]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[BorrowHistory](
	[BorrowerID] [int] NOT NULL,
	[DVDID] [int] NOT NULL,
	[TakenDate] [date] NOT NULL,
	[ReturnDate] [date] NULL,
 CONSTRAINT [pk_LoanID] PRIMARY KEY CLUSTERED 
(
	[BorrowerID] ASC,
	[DVDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


/****** Object:  Table [dbo].[DVDs]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[DVDs](
	[DVDID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[ReleaseDate] [date] NOT NULL,
	[MPPARating] [int] NOT NULL,
	[Studio] [int] NOT NULL,
	[OwnerRating] [int] NULL,
	[OwnerNotes] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[DVDID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FilmParticipantRoles]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[FilmParticipantRoles](
	[DVDID] [int] NULL,
	[FilmRoleID] [int] NULL,
	[FilmParticipantID] [int] NULL
) ON [PRIMARY]


/****** Object:  Table [dbo].[FilmParticipants]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FilmParticipants](
	[FilmParticipantID] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [varchar](25) NOT NULL,
	[FirstName] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[FilmParticipantID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[FilmRoles]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[FilmRoles](
	[FilmRoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleType] [varchar](30) NOT NULL
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[Ratings]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[Ratings](
	[RatingsID] [int] IDENTITY(1,1) NOT NULL,
	[Rating] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[RatingsID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

/****** Object:  Table [dbo].[Studios]    Script Date: 4/14/2016 2:11:18 PM ******/
SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

SET ANSI_PADDING ON

CREATE TABLE [dbo].[Studios](
	[StudioID] [int] IDENTITY(1,1) NOT NULL,
	[StudioName] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[StudioID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

SET IDENTITY_INSERT [dbo].[Borrowers] ON 


INSERT [dbo].[Borrowers] ([BorrowerID], [LastName], [FirstName]) VALUES (1, N'Jorgenson', N'Jake')

INSERT [dbo].[Borrowers] ([BorrowerID], [LastName], [FirstName]) VALUES (2, N'Pudelski', N'Victor')

INSERT [dbo].[Borrowers] ([BorrowerID], [LastName], [FirstName]) VALUES (3, N'Doe', N'John')

SET IDENTITY_INSERT [dbo].[Borrowers] OFF

SET IDENTITY_INSERT [dbo].[DVDs] ON 


INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (1, N'Happy Gilmore', CAST(N'1996-02-16' AS Date), 3, 1, 4, N'You eat pieces of shit for breakfast?')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (2, N'Scarface', CAST(N'1983-12-09' AS Date), 4, 1, 4, N'Say hello to my little friend!')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (3, N'The Shawshank Redemption', CAST(N'1994-10-04' AS Date), 4, 2, 4, N'Get busy living, or get busy dying.')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (4, N'Pulp Fiction', CAST(N'1994-10-14' AS Date), 4, 3, 5, N'English motherfucker! Do you speak it?')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (5, N'Fight Club', CAST(N'1999-10-15' AS Date), 4, 4, 4, N'You do not talk about Fight Club.')

INSERT [dbo].[DVDs] ([DVDID], [Title], [ReleaseDate], [MPPARating], [Studio], [OwnerRating], [OwnerNotes]) VALUES (7, N'The Great Gatsby', CAST(N'2016-04-07' AS Date), 3, 2, 1, N'ff')

SET IDENTITY_INSERT [dbo].[DVDs] OFF

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 1, 1)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 2)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (2, 1, 3)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (2, 2, 4)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (3, 1, 5)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (3, 2, 6)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (4, 1, 7)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (4, 2, 8)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (5, 1, 9)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (5, 2, 10)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 1)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 11)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 12)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 13)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (1, 2, 14)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (2, 1, 1)

INSERT [dbo].[FilmParticipantRoles] ([DVDID], [FilmRoleID], [FilmParticipantID]) VALUES (7, 1, 2)

SET IDENTITY_INSERT [dbo].[FilmParticipants] ON 


INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (1, N'Sandler', N'Adam')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (2, N'Dugan', N'Dennis')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (3, N'Pacino', N'Al')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (4, N'De Palma', N'Brian')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (5, N'Robbins', N'Tim')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (6, N'Darabont', N'Frank')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (7, N'Travolta', N'John')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (8, N'Tarintino', N'Quentin')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (9, N'Pitt', N'Brad')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (10, N'Fincher', N'David')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (11, N'Williams', N'John')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (12, N'Williams', N'John')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (13, N'Williams', N'John')

INSERT [dbo].[FilmParticipants] ([FilmParticipantID], [LastName], [FirstName]) VALUES (14, N'Mueller', N'TJ')

SET IDENTITY_INSERT [dbo].[FilmParticipants] OFF

SET IDENTITY_INSERT [dbo].[FilmRoles] ON 


INSERT [dbo].[FilmRoles] ([FilmRoleID], [RoleType]) VALUES (1, N'Actor')

INSERT [dbo].[FilmRoles] ([FilmRoleID], [RoleType]) VALUES (2, N'Director')

SET IDENTITY_INSERT [dbo].[FilmRoles] OFF

SET IDENTITY_INSERT [dbo].[Ratings] ON 


INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (1, N'G')

INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (2, N'PG')

INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (3, N'PG-13')

INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (4, N'R')

INSERT [dbo].[Ratings] ([RatingsID], [Rating]) VALUES (5, N'NC-17')

SET IDENTITY_INSERT [dbo].[Ratings] OFF

SET IDENTITY_INSERT [dbo].[Studios] ON 


INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (1, N'Universal Pictures')

INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (2, N'Castle Rock Entertainment')

INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (3, N'Miramax')

INSERT [dbo].[Studios] ([StudioID], [StudioName]) VALUES (4, N'Fox 2000 Pictures')

SET IDENTITY_INSERT [dbo].[Studios] OFF

ALTER TABLE [dbo].[DVDs]  WITH CHECK ADD  CONSTRAINT [FK_DVDs_Ratings] FOREIGN KEY([MPPARating])
REFERENCES [dbo].[Ratings] ([RatingsID])

ALTER TABLE [dbo].[DVDs] CHECK CONSTRAINT [FK_DVDs_Ratings]

ALTER TABLE [dbo].[DVDs]  WITH CHECK ADD  CONSTRAINT [FK_DVDs_Studios] FOREIGN KEY([Studio])
REFERENCES [dbo].[Studios] ([StudioID])

ALTER TABLE [dbo].[DVDs] CHECK CONSTRAINT [FK_DVDs_Studios]


END


GO
/****** Object:  StoredProcedure [dbo].[GetParticipantsForDVDID]    Script Date: 4/14/2016 2:58:45 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetParticipantsForDVDID]
	-- Add the parameters for the stored procedure here
	@DVDID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT fp.LastName, fp.FirstName, fp.FilmParticipantID, fr.RoleType, fr.FilmRoleID FROM DVDs d
		INNER JOIN FilmParticipantRoles fpr ON fpr.DVDID = d.DVDID
		INNER JOIN FilmParticipants fp ON fp.FilmParticipantID = fpr.FilmParticipantID
		INNER JOIN FilmRoles fr ON fr.FilmRoleID = fpr.FilmRoleID
		WHERE d.DVDID = @DVDID
END


GO
USE [master]
GO
ALTER DATABASE [DVDTest] SET  READ_WRITE 
GO
