﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using DVDLibrary.DATA.Config;
using DVDLibrary.Models;

namespace DVDLibrary.DATA
{
    public class DvdRepository
    {
        public List<Dvd> GetAllDvds()
        {
          List<Dvd> dvds = new List<Dvd>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT d.DVDID, d.Title, d.ReleaseDate, d.OwnerNotes, d.OwnerRating, r.Rating, s.StudioName, r.RatingsID " +
                                  "FROM DVDS d " +
                                  "inner join Ratings r on r.RatingsID = d.MPPARating " +
                                  "inner join Studios s on s.StudioID = d.Studio";
                cmd.Connection = cn;
                cn.Open();
                
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dvds.Add(PopulateFromDataReader(dr));
                    }
                }
            }
            return dvds;
        }
        
        private Dvd PopulateFromDataReader(SqlDataReader dr)
        {
            Dvd dvd = new Dvd();

            dvd.DvdId = (int) dr["DVDID"];
            dvd.Title = dr["Title"].ToString();
            dvd.ReleaseDate = (DateTime) dr["ReleaseDate"];
            dvd.MppaRating = (Rating) dr["RatingsID"];
            dvd.OwnerRating = (int) dr["OwnerRating"];
            dvd.Studio = dr["StudioName"].ToString();
            dvd.OwnerNotes = dr["OwnerNotes"].ToString();

            return dvd;
        }

        public Dvd GetDvd(int dvdId)
        {
            Dvd dvd = new Dvd();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT d.DVDID, d.Title, d.ReleaseDate, d.OwnerNotes, d.OwnerRating, r.Rating, s.StudioName, r.RatingsID " +
                                  "FROM DVDS d " +
                                  "INNER JOIN Ratings r on r.RatingsID = d.MPPARating " +
                                  "INNER JOIN Studios s on s.StudioID = d.Studio " +
                                  "WHERE d.DVDID = " + dvdId;
                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        dvd = PopulateFromDataReader(dr);
                    }
                }
            }

            dvd.BorrowedHistory = GetBorrowHistory(dvd.DvdId);
            dvd.FilmParticipants = GetFilmParticipantsForDVD(dvd.DvdId);
            //assign borrower ???

            return dvd;
        }

        private List<BorrowOccurence> GetBorrowHistory(int dvdId)
        {
            List<BorrowOccurence> borrowList = new List<BorrowOccurence>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM BorrowHistory bh " +
                                  "WHERE bh.DVDID = " + dvdId;
                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        borrowList.Add(BorrowOccurenceFromDataReader(dr));
                    }
                }
            }

            return borrowList;
        }

        private BorrowOccurence BorrowOccurenceFromDataReader(SqlDataReader dr)
        {
            BorrowOccurence borrowOccurence = new BorrowOccurence();

            if (dr["ReturnDate"] != DBNull.Value)
            {
                borrowOccurence.ReturnDate = (DateTime)dr["ReturnDate"];
            }

            borrowOccurence.TakenDate = (DateTime) dr["TakenDate"];
            borrowOccurence.Dvd = (int) dr["DVDID"];
            borrowOccurence.Borrower = BorrowerFromDataReader((int) dr["BorrowerID"]);
            borrowOccurence.LoanId = (int) dr["LoanID"];
            //borrowOccurence.BorrowerRating = ;
            //borrowOccurence.BorrowerNotes =;

            return borrowOccurence;
        }

        private Borrower BorrowerFromDataReader(int borrowerId)
        {
            Borrower borrower = new Borrower();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Borrowers b " +
                                  "WHERE b.BorrowerID = " + borrowerId;
                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        borrower.BorrowerKey = (int) dr["BorrowerID"];
                        borrower.FirstName = dr["FirstName"].ToString();
                        borrower.LastName = dr["LastName"].ToString();
                    }

                }
            }

            return borrower;
        }

        public void AddDvd(Dvd dvd)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "AddDvd";
                cmd.Parameters.AddWithValue("@Title", dvd.Title);

                cmd.Parameters.AddWithValue("@ReleaseDate", dvd.ReleaseDate);

                

                cmd.Parameters.AddWithValue("@MpaaRating", dvd.MppaRating);
                cmd.Parameters.AddWithValue("@OwnerRating", dvd.OwnerRating);
                cmd.Parameters.AddWithValue("@OwnerNotes", dvd.OwnerNotes);
                cmd.Parameters.AddWithValue("@Studio", dvd.Studio);


                SqlParameter outputIDParam = new SqlParameter("@DvdID", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };
                cmd.Parameters.Add(outputIDParam);
                

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
                int idOrDefaultValue = outputIDParam.Value as int? ?? default(int);
                if (idOrDefaultValue > 0)
                {
                    foreach (FilmParticipant fp in dvd.FilmParticipants)
                    {
                        AddFilmParticipantForDVD(fp, idOrDefaultValue);
                    }
                }
                else
                {
                    //throw error
                }
            }
        }

        public void DeleteVDvd(int DVDID)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DeleteDvd";
                cmd.Parameters.AddWithValue("@DvdID", DVDID);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public List<FilmParticipant> GetFilmParticipantsForDVD(int DVDID)
        {
            List<FilmParticipant> filmParticipants = new List<FilmParticipant>();
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "GetParticipantsForDVDID";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DVDID", DVDID);
                cmd.Connection = cn;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        filmParticipants.Add(ConvertDataToFilmParticipant(dr));
                    }
                }
            }
            return filmParticipants;
        }

        public List<FilmParticipant> GetAllFilmParticipants()
        {
            List<FilmParticipant> filmParticipants = new List<FilmParticipant>();
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM FilmParticipants";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        filmParticipants.Add(ConvertDataToFilmParticipant(dr));
                    }
                }
            }
            return filmParticipants;
        }

        private FilmParticipant ConvertDataToFilmParticipant(SqlDataReader dr)
        {
            FilmParticipant fp =  new FilmParticipant();

            fp.FirstName = dr["FirstName"].ToString();
            fp.LastName = dr["LastName"].ToString();
            fp.FilmParticipantID = (int)dr["FilmParticipantID"];
            if (dr.HasColumn("RoleType"))
            {
                fp.RoleTitle = dr["RoleType"].ToString();
            }
            if (dr.HasColumn("FilmRoleID"))
            {
                fp.RoleKey = (int)dr["FilmRoleID"];
            }
            return fp;
        }

        public Dictionary<int, string> GetAllFilmRoles()
        {
            Dictionary<int, string> filmRoles = new Dictionary<int, string>();
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM FilmRoles";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        filmRoles.Add(((int)dr["FilmRoleID"]), dr["RoleType"].ToString());
                    }
                }
            }
            return filmRoles;
        }

        public Dictionary<int, string> GetAllStudios()
        {
            Dictionary<int, string> studios = new Dictionary<int, string>();
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Studios";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cn;

                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        studios.Add(((int)dr["StudioID"]), dr["StudioName"].ToString());
                    }
                }
            }
            return studios;
        }

        public void AddFilmParticipantForDVD(FilmParticipant fp, int DVDID)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();

                if (fp.FilmParticipantID > 0)
                {
                    cmd.CommandText = "AddExistingFilmParticipant";
                    cmd.Parameters.AddWithValue("@DVDID", DVDID);
                    cmd.Parameters.AddWithValue("@FilmRoleID", fp.RoleKey);
                    cmd.Parameters.AddWithValue("@FilmParticipantID", fp.FilmParticipantID);
                }
                else
                {
                    cmd.CommandText = "AddNewFilmParticipant";
                    cmd.Parameters.AddWithValue("@FilmRoleID", fp.RoleKey);
                    cmd.Parameters.AddWithValue("@FirstName", fp.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", fp.LastName);
                    cmd.Parameters.AddWithValue("@DVDID", DVDID);

                    SqlParameter outputParameter = cmd.Parameters.Add("@FilmParticipantID", SqlDbType.Int);
                    outputParameter.Direction = ParameterDirection.Output;


                }

                cmd.CommandType = CommandType.StoredProcedure;
                
                cmd.Connection = cn;

                cn.Open();

                cmd.ExecuteNonQuery();
            }
        }

        public void ReturnDvd(int dvdId)
        {
            List<BorrowOccurence> list = GetBorrowHistory(dvdId);

            var results = list.Where(m => m.Dvd == dvdId && m.ReturnDate == null);

            int loanId = results.First().LoanId;

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "UPDATE BorrowHistory " +
                                  "SET ReturnDate = '" + DateTime.Now.ToShortDateString() + "' " +
                                  "WHERE LoanID = " + loanId;
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
