﻿using DVDLibrary.DATA.Config;
using DVDLibrary.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.DATA
{
    public class BorrowerRepository
    {
        public List<Borrower> GetAllBorrowers()
        {
            List<Borrower> borrowers = new List<Borrower>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Borrowers";
                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        Borrower borrower = new Borrower()
                        {
                            BorrowerKey = int.Parse(dr["BorrowerID"].ToString()),
                            FirstName = dr["FirstName"].ToString(),
                            LastName = dr["LastName"].ToString()
                        };
                        borrowers.Add(borrower);
                    }
                }
            }
            return borrowers;
        }

        public Borrower AddBorrower(Borrower newBorrower)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "INSERT INTO Borrowers (FirstName, LastName) VALUES('" + newBorrower.FirstName + "', '" + newBorrower.LastName + "')";
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
                //using (SqlDataReader dr = cmd.ExecuteReader())
                //{
                //    dr.Read();
                //}
            }
            var list = GetAllBorrowers();
            var borrower = list.OrderByDescending(b => b.BorrowerKey).FirstOrDefault();
            return borrower;
        }

        public void AddBorrowOccurence(BorrowOccurence newBorrowOccurence)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                string date = DateTime.Now.ToShortDateString();
                cmd.CommandText = "INSERT INTO BorrowHistory (BorrowerID, DVDID, TakenDate) " +
                    "VALUES(" + newBorrowOccurence.Borrower.BorrowerKey + ", " + newBorrowOccurence.Dvd + ", '" + date +"')";
                cmd.Connection = cn;
                cn.Open();

                cmd.ExecuteNonQuery();

            }
        }

    }
}
