﻿function resetForm($form) {
    $form.find('input:text, input:password, input:file, textarea').val('');

}

$(document).ready($('#testButton').click(function () {
    var $fName = $('#firstName').val().toString();
    var $lName = $('#lastName').val().toString();
    var $fRole = $('#Role option:selected').text().toString();
    var $fRoleID = $('#Role option:selected').val();
    var $FPID = $('#FilmParticipant option:selected').val();
    if ($('#existingFP_False').is(':checked')) {
        $FPID = 0;
    }
    var fp = {
        FirstName: $fName,
        LastName: $lName,
        RoleTitle: $fRole,
        RoleKey: $fRoleID,
        FilmParticipantID: $FPID
    }

    var arr = localStorage.getItem('FPArray');
    if (arr === null) {
        arr = [fp];
    } else {
        arr = JSON.parse(arr);
        arr.push(fp);
    }
    localStorage.setItem('FPArray', JSON.stringify(arr));


    $(function () {
        $('#myModalHorizontal').modal('hide');
    });
    if ($fName.length > 0) {
        $('#testList').append($('<li>').append($fName + ' ' + $lName + ' as ' + $fRole));
    } else {
        //$('#testList').append($('<li>').append($("#FilmParticipant").children("option").is("selected").text() + ' as ' + $fRole));
        var potato = $('#FilmParticipant option:selected')[0].innerHTML;
        $('#testList').append($('<li>').append(potato + ' as ' + $fRole));
    }
    
    resetForm($('#modalVerticalForm'));
    return false;
}));

$(function () {
    $('#newFPForm').on('submit', function (e) {
        e.preventDefault();
        var arr = localStorage.getItem('FPArray');
        if (arr === null) {
        } else {
            $('#hiddenJSON').val(arr);
        }
        var selTypeText = $("#studioD option:selected").text();
        $("#studioH").val(selTypeText);
        this.submit();
        localStorage.clear();
    });
});

$('#existingFP_True').click(function () {
    $('#FilmParticipant').removeAttr("disabled");
    $('#firstName').attr("disabled", "disabled");
    $('#lastName').attr("disabled", "disabled");
});

$('#existingFP_False').click(function () {
    $('#FilmParticipant').attr("disabled", "disabled");
    $('#firstName').removeAttr("disabled");
    $('#lastName').removeAttr("disabled");
});

