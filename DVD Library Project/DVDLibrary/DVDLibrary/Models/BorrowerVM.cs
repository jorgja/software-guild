﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DVDLibrary.Models
{
    public class BorrowerVM
    {
        public Borrower borrower { get; set; }
        public BorrowOccurence borrowOccurence { get; set; }
        public List<Borrower> borrowerList { get; set; }
        public List<SelectListItem> selectList { get; set; }

    }
}