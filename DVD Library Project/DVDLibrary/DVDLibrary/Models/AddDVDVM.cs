﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DVDLibrary.BLL;

namespace DVDLibrary.Models
{
    public class AddDVDVM
    {
        public Dvd DVD { get; set; }
        public List<SelectListItem> FilmParticipants { get; set; }
        public List<SelectListItem> FilmRoles { get; set; }
        public List<SelectListItem> MPAARatings { get; set; }
        public List<SelectListItem> Studios { get; set; }
        public List<SelectListItem> Ratings { get; set; }

        public AddDVDVM()
        {
            DVD = new Dvd();
            DVD.FilmParticipants = new List<FilmParticipant>();
            FilmParticipants = new List<SelectListItem>();
            FilmRoles = new List<SelectListItem>();
            MPAARatings = new List<SelectListItem>();
            Studios = new List<SelectListItem>();
            Ratings = new List<SelectListItem>();

            Operations ops = new Operations();

            foreach (FilmParticipant participant in ops.GetAllFilmParticipants())
            {
                string fullName = participant.FirstName + " " + participant.LastName;
                FilmParticipants.Add(new SelectListItem {Text = fullName, Value = participant.FilmParticipantID.ToString()});
            }

            foreach (KeyValuePair<int, string> pair in ops.GetAllFilmRoles())
            {
                FilmRoles.Add(new SelectListItem { Text = pair.Value, Value = pair.Key.ToString()});
            }

            foreach (var value in Enum.GetValues(typeof(Rating)))
            {
                MPAARatings.Add(new SelectListItem { Text = value.ToString(), Value = ((int)value).ToString()});
            }

            foreach (KeyValuePair<int, string> pair in ops.GetAllStudios())
            {
                Studios.Add(new SelectListItem { Text = pair.Value, Value = pair.Key.ToString()});
            }
            for (int i = 0; i < 6; i++)
            {
                Ratings.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString()});
            }
        }
    }
}