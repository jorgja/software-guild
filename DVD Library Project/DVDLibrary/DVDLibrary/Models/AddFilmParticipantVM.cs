﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DVDLibrary.BLL;

namespace DVDLibrary.Models
{
    public class AddFilmParticipantVM
    {
        public FilmParticipant FilmParticipant { get; set; }
        public List<SelectListItem> FilmParticipants { get; set; }
        public List<SelectListItem> FilmRoles { get; set; }

        public AddFilmParticipantVM()
        {
            FilmParticipant = new FilmParticipant();
            FilmParticipants = new List<SelectListItem>();
            FilmRoles = new List<SelectListItem>();

            Operations ops = new Operations();

            foreach (FilmParticipant participant in ops.GetAllFilmParticipants())
            {
                string fullName = participant.FirstName + " " + participant.LastName;
                FilmParticipants.Add(new SelectListItem { Text = fullName, Value = participant.FilmParticipantID.ToString() });
            }

            foreach (KeyValuePair<int, string> pair in ops.GetAllFilmRoles())
            {
                FilmRoles.Add(new SelectListItem { Text = pair.Value, Value = pair.Key.ToString() });
            }

        }
    }
}