﻿using DVDLibrary.BLL;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DVDLibrary.Models;

namespace DVDLibrary.Controllers
{

    public class HomeController : Controller
    {
        private readonly Operations _ops = new Operations();

        // GET: Home
        public ActionResult Views()
        {
            Response<List<Dvd>> reps = _ops.GetAllDvds();
            
            return View(reps.ResponseObject);
        }


        //
        //Might not need this view if we have a modal
        //public ActionResult Remove(int dvdId)
        //{
        //    return View();

        
        public ActionResult Remove(int id)
        {
            _ops.DeleteDvd(id);
            return RedirectToAction("Views");
        }


        public ActionResult AddDvd()
        {
            AddDVDVM dvd = new AddDVDVM();
            return View(dvd);
        }
        [HttpPost]
        public ActionResult Search(string searchTerm)
        {
            Response<List<Dvd>> reps = _ops.GetAllDvds();
            List<Dvd> filteredList = reps.ResponseObject.Where(d => d.Title.ToUpper().Contains(searchTerm.ToUpper())).ToList();
            return View("Views", filteredList);
        }

        [HttpPost]
        public ActionResult AddDvd(Dvd dvd)
        {
            string json = Request.Form["hiddenJSON"];
            var serializer = new JavaScriptSerializer();
            if (json.Length > 2)
            {
                List<FilmParticipant> arr = serializer.Deserialize<List<FilmParticipant>>(json);
                dvd.FilmParticipants = arr;
            }
            dvd.Studio = Request.Form["studioH"].ToString();
            _ops.AddDvd(dvd);
            // ops add dvd
            return RedirectToAction("Views", "Home");
        }

        public ActionResult DvdDetails(int id)
        {

            Response<Dvd> response = _ops.GetDvdDetails(id);

            return View(response.ResponseObject);
        }

        [HttpPost]
        public ActionResult DvdDetails()
        {
            return View();
        }

        public ActionResult ReturnDvd(int id)
        {
            _ops.ReturnDvd(id);
            return RedirectToAction("DvdDetails", new {id = id});
        }

        public ActionResult Borrow(int id)
        {
            var borrowerList = _ops.GetAllBorrowers();

            var borrowerVM = new BorrowerVM()
            {
                borrowOccurence = new BorrowOccurence(),
                borrowerList = borrowerList,
                selectList = new List<SelectListItem>()
            };
            borrowerVM.borrowOccurence.Dvd = id;

            foreach(var b in borrowerList)
            {
                borrowerVM.selectList.Add(new SelectListItem { Text = b.LastName + ", " + b.FirstName, Value = b.BorrowerKey.ToString() });
            }
            return View(borrowerVM);
        }

        [HttpPost]
        public ActionResult Borrow(BorrowOccurence borrowOccurence)
        {
            if (Request.Form.AllKeys.Contains("returning"))
            {
                _ops.AddBorrowOccurence(borrowOccurence);
                return RedirectToAction ("DvdDetails", new { id = borrowOccurence.Dvd });
            }

            if (Request.Form.AllKeys.Contains("new"))
            {
                borrowOccurence.Borrower = _ops.AddBorrower(borrowOccurence.Borrower);
                _ops.AddBorrowOccurence(borrowOccurence);
                return RedirectToAction("DvdDetails", new { id = borrowOccurence.Dvd });
            }

            return View();
        }
    }
}