﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DVDLibrary.BLL;
using DVDLibrary.Models;
using NUnit.Framework;

namespace DVDLibrary.TEST
{
    [TestFixture]
    class DvdLibraryTests
    {
        public string _cns = @"Data Source=.\SQL2014; Initial Catalog=DVDTest;User ID=sa;Password=sqlserver;";

        [SetUp]
        public void Setup()
        {
            using (SqlConnection cn = new SqlConnection(_cns))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "DANDCDVDT";
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        [Test]
        public void ShouldReturnAllBorrowers()
        {
            var bll = new Operations();
            var expected = 3;
            var borrowers = bll.GetAllBorrowers();

            var actual = borrowers.Count;

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ShouldAddNewBorrower()
        {
            var bll = new Operations();
            var expectedBorrower = new Borrower
            {
                FirstName = "Testy",
                LastName = "Testerson"
            };
            bll.AddBorrower(expectedBorrower);
            var list = bll.GetAllBorrowers();

            var actualBorrower = list.Last();

            Assert.AreEqual(actualBorrower.LastName, expectedBorrower.LastName);
            Assert.AreEqual(actualBorrower.FirstName, expectedBorrower.FirstName);

        }

        [Test]
        public void ShouldAddNewBorrowOccurence()
        {
            var bll = new Operations();

            var borrower = new Borrower
            {
                FirstName = "Testy",
                LastName = "Testerson"
            };
            bll.AddBorrower(borrower);
            var borrowers = bll.GetAllBorrowers();
            var newBorrower = borrowers.Last();

            var expectedOccurence = new BorrowOccurence
            {
                TakenDate = DateTime.Today,
                Dvd = 1,
                Borrower = newBorrower,
                BorrowerRating = 3,
                BorrowerNotes = "Yep that is a movie",
            };

            bll.AddBorrowOccurence(expectedOccurence);
            var list = bll.GetDvdDetails(1).ResponseObject.BorrowedHistory;
            var actualOccurence = list.Last();

            Assert.AreEqual(expectedOccurence.TakenDate, actualOccurence.TakenDate);
            Assert.AreEqual(expectedOccurence.Dvd, actualOccurence.Dvd);
            Assert.AreEqual(actualOccurence.LoanId, 11);
            
        }

        [Test]
        public void ShouldReturnAllFilmParticipantsForDvd()
        {
            var bll = new Operations();
            var list = bll.GetFilmParticipantsForDVD(1);
            Assert.AreEqual(list.Count, 7);

        }

        [Test]
        public void ShouldReturnAllFilmParticipants()
        {
            var bll = new Operations();
            var list = bll.GetAllFilmParticipants();

            Assert.AreEqual(list.Count, 14);
        }

        [Test]
        public void ShouldReturnAllFilmRoles()
        {
            var bll = new Operations();

            var list = bll.GetAllFilmRoles();

            Assert.AreEqual(list.Count, 2);
            Assert.AreEqual(list[1], "Actor");
            Assert.AreEqual(list[2], "Director");

        }

        [Test]
        public void ShouldReturnAllStudios()
        {
            var bll = new Operations();

            var list = bll.GetAllStudios();

            Assert.AreEqual(list.Count, 4);
            Assert.AreEqual(list[1], "Universal Pictures");
            Assert.AreEqual(list[2], "Castle Rock Entertainment");
            Assert.AreEqual(list[3], "Miramax");
            Assert.AreEqual(list[4], "Fox 2000 Pictures");

        }

        [Test]
        public void ShouldAddNewFilmParticipantForDvd()
        {
            var bll = new Operations();

            FilmParticipant fp = new FilmParticipant
            {
                FirstName = "Testy",
                LastName = "McTesterson",
                RoleKey = 1,
                RoleTitle = "Actor"
            };

            bll.AddFilmParticipantForDVD(fp, 1);

            var list = bll.GetFilmParticipantsForDVD(1);

            Assert.AreEqual(list.Count, 8);
            Assert.AreEqual(list.Last().LastName, fp.LastName);
            Assert.AreEqual(list.Last().FirstName, fp.FirstName);
            Assert.AreEqual(list.Last().RoleKey, fp.RoleKey);
            Assert.AreEqual(list.Last().RoleTitle, fp.RoleTitle);
            
        }

        [Test]
        public void ShouldReturnAllDvds()
        {
            var bll = new Operations();

            var actualList = bll.GetAllDvds().ResponseObject;

            Assert.AreEqual(actualList.Count, 6);
            Assert.AreEqual(actualList[4].MppaRating, (Rating)4);
            Assert.AreEqual(actualList[5].Studio, "Castle Rock Entertainment");
        }

        [Test]
        public void ShouldReturnDetailsOfSpecificDvd()
        {
            var bll = new Operations();

            var details = bll.GetDvdDetails(1).ResponseObject;

            Assert.AreEqual(details.DvdId, 1);
            Assert.AreEqual(details.MppaRating, (Rating)3);
            Assert.AreEqual(details.FilmParticipants[0].LastName, "Sandler");
            Assert.AreEqual(details.BorrowedHistory[0].Borrower.LastName, "Jorgenson");

        }

        [Test]
        public void ShouldAddDvd()
        {
            var bll = new Operations();

            var expectedDvd = new Dvd
            {
                ReleaseDate = DateTime.Now,
                MppaRating = (Rating)2,
                OwnerNotes = "I like it",
                OwnerRating = 4,
                Studio = "Miramax",
                Title = "Chasing Amy"
            };

            bll.AddDvd(expectedDvd);
            var actualDvd = bll.GetAllDvds().ResponseObject.Last();
            Assert.AreEqual(expectedDvd.MppaRating, actualDvd.MppaRating);
            Assert.AreEqual(expectedDvd.Studio, actualDvd.Studio);
            Assert.AreEqual(expectedDvd.OwnerNotes, actualDvd.OwnerNotes);
            Assert.AreEqual(expectedDvd.Title, actualDvd.Title);
            Assert.AreEqual(expectedDvd.OwnerRating, actualDvd.OwnerRating);
            
        }

        [Test]
        public void ShouldDeleteDvd()
        {
            var bll = new Operations();

            int expectedResult = bll.GetAllDvds().ResponseObject.Count - 1;
            bll.DeleteDvd(4);

            int actualResult = bll.GetAllDvds().ResponseObject.Count;

            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void ShouldUpdateDvdAsReturned()
        {
            var bll = new Operations();
            var borrower = new Borrower
            {
                FirstName = "Testy",
                LastName = "Testerson"
            };
            bll.AddBorrower(borrower);
            var borrowers = bll.GetAllBorrowers();
            var newBorrower = borrowers.Last();

            var expectedOccurence = new BorrowOccurence
            {
                TakenDate = DateTime.Today,
                Dvd = 1,
                Borrower = newBorrower,
                BorrowerRating = 3,
                BorrowerNotes = "Yep that is a movie",
            };

            bll.AddBorrowOccurence(expectedOccurence);
            bll.ReturnDvd(1);
            Dvd dvd = bll.GetDvdDetails(1).ResponseObject;
            Assert.AreEqual(dvd.BorrowedHistory.Last().ReturnDate, DateTime.Today);
        }
    }
}
