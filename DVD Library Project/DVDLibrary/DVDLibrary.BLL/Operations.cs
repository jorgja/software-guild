﻿using System;
using System.Collections.Generic;
using DVDLibrary.Models;
using DVDLibrary.DATA;

namespace DVDLibrary.BLL
{
    public class Operations
    {
        private readonly DvdRepository _dvdRepo;
        private readonly BorrowerRepository _borrowerRepo;
        
        public Operations()
        {
            _dvdRepo = new DvdRepository();
            _borrowerRepo = new BorrowerRepository();
        }

        public List<Borrower> GetAllBorrowers()
        {

            return _borrowerRepo.GetAllBorrowers();
        }

        // GetAllBorrowers response for Jake
        //public Response<List<Borrower>> GetAllBorrowers()
        //{
        //    Response<List<Borrower>> response = new Response<List<Borrower>>();

        //    try
        //    {
        //        response.ResponseObject = _borrowerRepo.GetAllBorrowers();
        //    }

        //    catch (Exception ex)
        //    {
        //        response.Exception = ex;
        //    }

        //    if (response.Exception == null && response.ResponseObject != null)
        //    {
        //        response.Success = true;
        //    }

        //    return response;
        //}
        public Borrower AddBorrower(Borrower newBorrower)
        {
            return _borrowerRepo.AddBorrower(newBorrower);
        }

        public void AddBorrowOccurence(BorrowOccurence newBorrowOccurence)
        {
            _borrowerRepo.AddBorrowOccurence(newBorrowOccurence);
        }

        // AddBorrower response for Jake
        //public Response<Borrower> AddBorrower(Borrower newBorrower)
        //{
        //    Response<Borrower> response = new Response<Borrower>()
        //    {
        //        ResponseObject = newBorrower
        //    };

        //    try
        //    {
        //        _borrowerRepo.AddBorrower(newBorrower);
        //    }

        //    catch (Exception ex)
        //    {
        //        response.Exception = ex;
        //    }

        //    if (response.Exception == null && response.ResponseObject != null)
        //    {
        //        response.Success = true;
        //    }

        //    return response;
        //}

        public List<FilmParticipant> GetFilmParticipantsForDVD(int DVDID)
        {
            return _dvdRepo.GetFilmParticipantsForDVD(DVDID);
        }

        // GetFilmParticipantsForDVD response for TJ
        //public Response<List<FilmParticipant>> GetFilmParticipantsForDVD(int DVDID)
        //{
        //    Response<List<FilmParticipant>> response = new Response<List<FilmParticipant>>();

        //    try
        //    {
        //        response.ResponseObject = _dvdRepo.GetFilmParticipantsForDVD(DVDID);
        //    }

        //    catch (Exception ex)
        //    {
        //        response.Exception = ex;
        //    }

        //    if (response.Exception == null && response.ResponseObject != null)
        //    {
        //        response.Success = true;
        //    }

        //    return response;
        //}

        public List<FilmParticipant> GetAllFilmParticipants()
        {

            return _dvdRepo.GetAllFilmParticipants();
        }

        // GetAllFilmParticipants response for TJ
        //public Response<List<FilmParticipant>> GetAllFilmParticipants()
        //{
        //    Response<List<FilmParticipant>> response = new Response<List<FilmParticipant>>();

        //    try
        //    {
        //        response.ResponseObject = _dvdRepo.GetAllFilmParticipants();
        //    }

        //    catch (Exception ex)
        //    {
        //        response.Exception = ex;
        //    }

        //    if (response.Exception == null && response.ResponseObject != null)
        //    {
        //        response.Success = true;
        //    }

        //    return response;
        //}

        public Dictionary<int, string> GetAllFilmRoles()
        {
            return _dvdRepo.GetAllFilmRoles();
        }

        //GetAllFilmRoles response for TJ
        //public Response<Dictionary<int, string>> GetAllFilmRoles()
        //{
        //    Response<Dictionary<int, string>> response = new Response<Dictionary<int, string>>();

        //    try
        //    {
        //        response.ResponseObject = _dvdRepo.GetAllFilmRoles();
        //    }

        //    catch (Exception ex)
        //    {
        //        response.Exception = ex;
        //    }

        //    if (response.Exception == null && response.ResponseObject != null)
        //    {
        //        response.Success = true;
        //    }

        //    return response;
        //}

        public Dictionary<int, string> GetAllStudios()
        {
            DvdRepository repo = new DvdRepository();
            return repo.GetAllStudios();
        }

        // GetAllStudios response for TJ
        //public Response<Dictionary<int, string>> GetAllStudios()
        //{
        //    Response<Dictionary<int, string>> response = new Response<Dictionary<int, string>>();

        //    try
        //    {
        //        response.ResponseObject = _dvdRepo.GetAllStudios();
        //    }

        //    catch (Exception ex)
        //    {
        //        response.Exception = ex;
        //    }

        //    if (response.Exception == null && response.ResponseObject != null)
        //    {
        //        response.Success = true;
        //    }

        //    return response;
        //}

        public void AddFilmParticipantForDVD(FilmParticipant fp, int DVDID)
        {
            _dvdRepo.AddFilmParticipantForDVD(fp, DVDID);
        }

        // AddFilmParticipantForDVD response for TJ
        //public Response<FilmParticipant> AddFilmParticipantForDVD(FilmParticipant fp, int DVDID)
        //{
        //    Response<FilmParticipant> response = new Response<FilmParticipant>()
        //    {
        //        ResponseObject = fp
        //    };

        //    try
        //    {
        //        _dvdRepo.AddFilmParticipantForDVD(fp, DVDID);
        //    }

        //    catch (Exception ex)
        //    {
        //        response.Exception = ex;
        //    }

        //    if (response.Exception == null && response.ResponseObject != null)
        //    {
        //        response.Success = true;
        //    }

        //    return response;
        //}


        //public List<Dvd> GetAllDvds()
        //{
        //    DvdRepository repo = new DvdRepository();
        //    var dvds = repo.GetAllDvds();
        //    return dvds;
        //}

        // GetAllDvds response for Alfred
        public Response<List<Dvd>> GetAllDvds()
        {
            Response<List<Dvd>> response = new Response<List<Dvd>>();

            try
            {
                response.ResponseObject = _dvdRepo.GetAllDvds();
            }

            catch (Exception ex)
            {
                response.Exception = ex;
            }

            if (response.Exception == null && response.ResponseObject != null)
            {
                response.Success = true;
            }

            return response;
        }

        public Response<Dvd> GetDvdDetails(int dvdId)
        {
            Response<Dvd> response = new Response<Dvd>();

            try
            {
                response.ResponseObject = _dvdRepo.GetDvd(dvdId);
            }

            catch (Exception ex)
            {
                response.Exception = ex;
            }

            if (response.Exception == null && response.ResponseObject != null)
            {
                response.Success = true;
            }

            return response;
        }

        public Response<Dvd> AddDvd(Dvd newDvd)
        {
            Response<Dvd> response = new Response<Dvd>()
            {
                ResponseObject = newDvd
            };

            try
            {
                _dvdRepo.AddDvd(newDvd);
            }

            catch (Exception ex)
            {
                response.Exception = ex;
            }

            if (response.Exception == null && response.ResponseObject != null)
            {
                response.Success = true;
            }

            return response;
        }

        public void DeleteDvd(int dvdId)
        {
            _dvdRepo.DeleteVDvd(dvdId);
        }

        public void ReturnDvd(int dvdId)
        {
            _dvdRepo.ReturnDvd(dvdId);
        }
    }
}
