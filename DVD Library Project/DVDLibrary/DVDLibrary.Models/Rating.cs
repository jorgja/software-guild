﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public enum Rating
    {
        G = 1, 
        PG,
        PG13,
        R,
        NC17
    }
}
