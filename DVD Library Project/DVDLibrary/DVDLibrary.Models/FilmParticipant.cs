﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class FilmParticipant
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int RoleKey { get; set; }
        public string RoleTitle { get; set; }
        public int FilmParticipantID { get; set; }
    }
}
