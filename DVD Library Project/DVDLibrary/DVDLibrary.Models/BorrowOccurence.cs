﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class BorrowOccurence
    {
        public DateTime TakenDate { get; set; }
        public DateTime? ReturnDate { get; set; }
        public int Dvd { get; set; }
        public Borrower Borrower { get; set; }
        public int BorrowerRating { get; set; }
        public string BorrowerNotes { get; set; }
        public int LoanId { get; set; }
    }
}
