﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class Response <T>
    {
        public T ResponseObject { get; set; }
        public bool Success { get; set; }
        public Exception Exception { get; set; }
    }
}
