﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVDLibrary.Models
{
    public class Dvd
    {
        public int DvdId { get; set; }

        [DisplayName("Movie Title")]
        public string Title { get; set; }

        [DisplayName("Release Date")]
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }

        [DisplayName("MPAA Rating")]
        public Rating MppaRating { get; set; }

        [DisplayName("Studio")]
        public string Studio { get; set; }

        [DisplayName("Owner's Rating")]
        public int OwnerRating { get; set; }

        [DisplayName("Owners Notes")]
        [DataType(DataType.MultilineText)]
        public string OwnerNotes { get; set; }

        public List<BorrowOccurence> BorrowedHistory { get; set; }

        public List<FilmParticipant> FilmParticipants { get; set; }
    }
}
