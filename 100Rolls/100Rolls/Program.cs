﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _100Rolls
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            int[] rolls = new int[6];
            int roll;
            for (int i = 0; i < 100; i ++)
            {
                roll = r.Next(1, 7);
                rolls[roll-1]++;
            }
            Console.WriteLine("You rolled:");
            Console.WriteLine("{0} 1's", rolls[0]);
            Console.WriteLine("{0} 2's", rolls[1]);
            Console.WriteLine("{0} 3's", rolls[2]);
            Console.WriteLine("{0} 4's", rolls[3]);
            Console.WriteLine("{0} 5's", rolls[4]);
            Console.WriteLine("{0} 6's", rolls[5]);
            Console.Read();
        }
    }
}
