Insert Into Person (FirstName, LastName, Email, Phone)
Values ('Alfred', 'Crawford II', 'al.crawfordii@gmail.com', '330-283-1055'),
('Timothy', 'Meuller', 'tj@tjiscool.com','330-123-4567'),
('Jake', 'Jorgensen', 'jake@jakethesnake.com', '330-987-6543'),
('William', 'Pulson', 'bill@bill.com', '330-101-1001'),
('Rick', 'James', 'ricky@imrickjames.com', '555-101-3301');

Insert Into Make (Manufacturer) 
Values ('Chevy'),('Buick'),('Ford'),('Jeep'),('Toyota'),('Lexus'),
('Honda'),('Acura'),('Chrystler'),('Hyundai'),('Subaru'),('Porsche'),
('Lamborghini'),('Ferrari'),('Daewoo'),('Nissan'),('Infiniti'),('Mercedes-Benz'),
('Saab'),('Audi'),('BMW'),('Aston Martin'),('Land Rover'),('Dodge'),('Fiat');

Insert Into Vehicle (MakeID, Model, Mileage, Year, IsAvailable)
Values(6, 'LFA', 450, 2016, 0),
(20, 'RS7', 1200, 2014,1),
(3, 'Focus', 22354, 2010,0),
(16, 'GT-R', 12345, 2010,0),
(11, 'Impreza', 43210, 2004,1);

Insert Into RequestStatus (Status)
Values ('New'),('Awaiting Reply'),('Future Prospect'),('Lost Opportunity');

Insert Into Listing (AdTitle, Description, Price, Picture, VehicleID, IsAvailable)
Values ('Luxury & Play','Ultra Rare Lexus LFA. A True head turner', 340000, 'https://upload.wikimedia.org/wikipedia/commons/8/82/Lexus_LFA_001.JPG', 1, 0),
('Fuel Freindly Vehicle', 'This Ford Focus averages 42MPG...WOW!', 9200, 'http://www.ford.com/ngbs-services/resources/ford/focus/2016/highlights/Desktop_657x424_0004_focus_ST.jpg', 3, 0),
('Beauty and The Beast', 'The ULTIMATE tuner car for tuners that live life 1/4 mile at a time.', 93000, 'https://upload.wikimedia.org/wikipedia/commons/b/ba/Nissan_GT-R_01.JPG', 4, 1);

Insert Into Request (ListingID, PersonID, BestTime, ContatMethodID, TimeFrame, LastContact, RequestStatusID)
Values (3, 2, 'After 4:00', 1, '3 Months', '2016', 1);

Insert Into ContactMethod (Method)
Values ('Phone'),('E-Mail');

