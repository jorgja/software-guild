USE [CarTell]
GO
/****** Object:  StoredProcedure [dbo].[AddRequest]    Script Date: 4/18/2016 4:18:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddRequest]
	-- Add the parameters for the stored procedure here
	@ListingID int,
	@FirstName varchar(20),
	@LastName varchar(30),
	@Phone varchar(15),
	@Email varchar(30),
	@Timeframe varchar(50),
	@BestTime varchar(30),
	@ContactMethod int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Person(FirstName, LastName, Phone, Email) VALUES(@FirstName, @LastName, @Phone, @Email)
	
	DECLARE @PersonID int
	SET @PersonID = SCOPE_IDENTITY()

	INSERT INTO Request(PersonID, ListingID, TimeFrame, ContatMethodID, BestTime, RequestStatusID)
	VALUES(@PersonID, @ListingID, @Timeframe, @ContactMethod, @BestTime, 1)
END
