-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE AddListing 
	-- Add the parameters for the stored procedure here
	@AdTitle varchar(50),
	@Description varchar(500),
	@Price int,
	@Picture varchar(100),
	@Available int,
	@Mileage int,
	@Year int,
	@Manufacturer varchar(30),
	@Model varchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	IF((SELECT COUNT(*) FROM Make WHERE Manufacturer = @Manufacturer) = 0)
	BEGIN
	INSERT INTO Make (Manufacturer) VALUES(@Manufacturer)
	END

	DECLARE @MakeID int
	SET @MakeID = (SELECT Manufacturer FROM Make WHERE Manufacturer = @Manufacturer)

	INSERT INTO Vehicle (MakeID, Model, Mileage, [Year]) VALUES(@MakeID, @Model, @Mileage, @Year)

	DECLARE	@VehicleID int
	SET @VehicleID = (SELECT MAX(VehicleID) FROM Vehicle GROUP BY VehicleID)

	INSERT INTO Listing (AdTitle, Available, [Description], Picture, Price, VehicleID)
	VALUES (@AdTitle, @Available, @Description, @Picture, @Price, @VehicleID)
END
GO
