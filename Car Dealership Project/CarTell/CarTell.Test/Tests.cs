﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarTell.data;
using CarTell.models;
using NUnit.Framework;

namespace CarTell.Test
{
    [TestFixture]
    public class Tests
    {
        private ListingRepository _listing;
        private CarRepository _car;

        public string _cns = @"Data Source=.\SQL2014; Initial Catalog=CarTest;User ID=sa;Password=sqlserver;";

        [SetUp]
        public void Setup()
        {


            using (SqlConnection cn = new SqlConnection(_cns))
            {
                SqlCommand cmd = new SqlCommand();

                cmd.Connection = cn;
                cn.Open();

            }

            _listing = new ListingRepository();
            _car = new CarRepository();
        }


        [Test]
        public void Get_Listing_By_ID_Test()
        {
            var vehicleId = 4;
            var listing =_listing.GetVehicleListingById(vehicleId);

            var expectedListing = new Listing
            {
                ListingID = 3,
                AdTitle = "Beauty and The Beast",
                Description = "The ULTIMATE tuner car for tuners that live life 1/4 mile at a time.",
                Price = 93000,
                Picture = "https://upload.wikimedia.org/wikipedia/commons/b/ba/Nissan_GT-R_01.JPG",
                Make = "Nissan",
                Model = "GT-R"
                

            };

            
            Assert.AreEqual(expectedListing.ListingID, listing.ListingID);
            Assert.AreEqual(expectedListing.Price, listing.Price);
            Assert.AreEqual(expectedListing.Description, listing.Description);
            Assert.AreEqual(expectedListing.Make, listing.Make);
            Assert.AreEqual(expectedListing.Model, listing.Model);
        }
    }
}
