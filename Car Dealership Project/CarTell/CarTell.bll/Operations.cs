﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using CarTell.data;
using CarTell.models;

namespace CarTell.bll
{
    public class Operations
    {

        private readonly ListingRepository _repo = new ListingRepository();

        public Response<List<Listing>> GetAllVehicleListings()
        {
            Response <List<Listing>> response = new Response<List<Listing>>();

            try
            {
                response.ResponseObject = _repo.GetAllVehicleListings();
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }
            if (response.ResponseObject != null && response.Exception == null)
            {
                response.Success = true;
            }

            return response;
        }

        public Response<Listing> GetVehicleListingById(int id)
        {
            Response<Listing> response = new Response<Listing>();

            try
            {
                response.ResponseObject = _repo.GetVehicleListingById(id);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }
            if (response.ResponseObject != null && response.Exception == null)
            {
                response.Success = true;
            }

            return response;
        }

        public Response<List<Request>> GetAllRequestsByVehicleId(int id)
        {
            Response<List<Request>> response = new Response<List<Request>>();

            try
            {
                response.ResponseObject = _repo.GetAllRequestsByVehicle(id);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }
            if (response.ResponseObject != null && response.Exception == null)
            {
                response.Success = true;
            }

            return response;
        }

        public Response<int[]> EditRequestStatus(int id, int status)
        {
            Response<int[]> response = new Response<int[]>
            {
                ResponseObject = new int[2]
            };

            response.ResponseObject[0] = id;
            response.ResponseObject[1] = status;


            try
            {
                EditRequestStatus(id, status);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }
            if (response.ResponseObject != null && response.Exception == null)
            {
                response.Success = true;
            }

            return response;
        }

        public Response<Listing> AddVehicleListing(Listing newListing)
        {
            Response<Listing> response = new Response<Listing>();

            response.ResponseObject = newListing;

            try
            {
                _repo.AddVehicleListing(newListing);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }
            if (response.ResponseObject != null && response.Exception == null)
            {
                response.Success = true;
            }

            return response;
        }

        public Response<Request> AddRequestForVehicle(Request newRequest)
        {
            Response<Request> response = new Response<Request>();

            response.ResponseObject = newRequest;

            try
            {
                _repo.AddRequestForVehicle(newRequest);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }
            if (response.ResponseObject != null && response.Exception == null)
            {
                response.Success = true;
            }

            return response;
        }

        public Response<int> RemoveVehicleListing(int id)
        {
            Response<int> response = new Response<int>
            {
                ResponseObject = id
            };

            try
            {
                _repo.RemoveVehicleListing(id);
            }
            catch (Exception ex)
            {
                response.Exception = ex;
            }
            if (response.ResponseObject != 0 && response.Exception == null)
            {
                response.Success = true;
            }
            
            return response;
        }

        //public Response<List<SelectListItem>> GetListOfMakes()
        //{
        //    Response<List<SelectListItem>> response = new Response<List<SelectListItem>>();

        //    try
        //    {
        //        //data layer
        //    }
        //    catch (Exception ex)
        //    {
        //        response.Exception = ex;
        //    }
        //    if (response.ResponseObject != null && response.Exception == null)
        //    {
        //        response.Success = true;
        //    }

        //    return response;
        //}
    }
}
