﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTell.models
{
    public class Response <R>
    {
        public Exception Exception { get; set; }
        public bool Success { get; set; }
        public R ResponseObject { get; set; }
    }
}
