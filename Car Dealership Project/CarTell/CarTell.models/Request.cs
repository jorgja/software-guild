﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarTell.models
{
    public class Request
    {
        public Person Requestor { get; set; }
        public int ListingID { get; set; }
        public int RequestID { get; set; }

        [DisplayName("What is the best time to contact you?")]
        public string BestContactTime { get; set; }

        [DisplayName("Preferred Contact Method")]
        public string ContactMethod { get; set; }

        [DisplayName("When are you looking to purchase a vehicle?")]
        public string Timeframe { get; set; }

        [DisplayName("When did you last contact this client?")]
        public DateTime? LastContact { get; set; }

        [DisplayName("Current Request Status")]
        public string Status { get; set; }

        public RequestStatus RequestStatus { get; set; }
    }
}
