﻿namespace CarTell.models
{
    public enum RequestStatus
    {
        New = 1,
        AwaitingReply,
        FutureProspect,
        LostOpportunity
    }
}