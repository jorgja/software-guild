﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarTell.bll;
using CarTell.models;
using CarTell.Models;

namespace CarTell.Controllers
{
    public class HomeController : Controller
    {
        private Operations _Ops = new Operations();

        public ActionResult Index()
        {
            ListingsIndexVM lmvm = new ListingsIndexVM();
            lmvm.AvailibleListings = new List<Listing>();
            if (User.IsInRole("Admin"))
            {
                lmvm.AvailibleListings = _Ops.GetAllVehicleListings().ResponseObject.ToList();
            }
            else
            {
                lmvm.AvailibleListings = _Ops.GetAllVehicleListings().ResponseObject.Where(l => l.Available).ToList();
            }

            return View(lmvm);
        }
        public ActionResult IndexWithAlert(string err)
        {
            ListingsIndexVM lmvm = new ListingsIndexVM();
            lmvm.AvailibleListings = new List<Listing>();
            if (User.IsInRole("Admin"))
            {
                lmvm.AvailibleListings = _Ops.GetAllVehicleListings().ResponseObject.ToList();
            }
            else
            {
                lmvm.AvailibleListings = _Ops.GetAllVehicleListings().ResponseObject.Where(l => l.Available).ToList();
            }
            lmvm.ErrorMessage = err;
            return View("Index",lmvm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult VehicleRequest(int id)
        {
            Request newRequest = new Request();
            newRequest.ListingID = id;
            return View("VehicleRequest", newRequest);
        }

        [HttpPost]
        public ActionResult VehicleRequest(Request newRequest)
        {
            Response<Request> requestResonse = _Ops.AddRequestForVehicle(newRequest);
            return RedirectToAction("Index");
        }


        [Authorize(Roles = "Admin")]
        public ActionResult ReviewRequests(int id)
        {
            Response<List<Request>> requests = _Ops.GetAllRequestsByVehicleId(id);
            
            RequestIndexVM v = new RequestIndexVM();
            v.Requests = requests.ResponseObject;
            if (v.Requests != null && v.Requests.Count > 0)
            {
                return View(v);
            }
            return RedirectToAction("IndexWithAlert", new {err = "No Requests for this listing"});
        }

        [Authorize(Roles = "Admin")]
        public ActionResult UpdateRequestStatus(Request request)
        {
            _Ops.EditRequestStatus(request.RequestID, (int)request.RequestStatus);
            return RedirectToAction("ReviewRequests", request.ListingID);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddVehicle()
        {
            Listing newListing = new Listing();
            newListing.Year = 2000;
            return View(newListing);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult AddVehicle(Listing newListing)
        {
            Response<Listing> listingResponse = _Ops.AddVehicleListing(newListing);

            return RedirectToAction("Index");
        }

        //[HttpPost]
        public ActionResult Delete(int id)
        {
            _Ops.RemoveVehicleListing(id);
            return RedirectToAction("Index");
        }
    }
}