﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CarTell.bll;
using CarTell.models;

namespace CarTell.Controllers
{
    public class RequestAPIController : ApiController
    {
        public HttpResponseMessage UpdatedRequest(Request req)
        {
            Operations ops = new Operations();
            //todo: whats being passed req id and status id
            ops.EditRequestStatus(0, 0);
            return Request.CreateResponse(HttpStatusCode.Accepted, req);
        }
    }
}
