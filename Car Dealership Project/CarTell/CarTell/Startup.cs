﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CarTell.Startup))]
namespace CarTell
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
