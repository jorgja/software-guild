﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarTell.models;

namespace CarTell.Models
{
    public class ListingsIndexVM
    {
        public List<Listing>AvailibleListings { get; set; }
        public string ErrorMessage { get; set; }
    }
}