﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CarTell.models;

namespace CarTell.Models
{
    public class RequestIndexVM
    {
        public List<Request> Requests { get; set; }
    }
}