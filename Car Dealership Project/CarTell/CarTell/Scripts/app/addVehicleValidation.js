﻿$(document).ready(function () {
    $('#addVehicleForm').validate({
        rules: {
            AdTitle: {
                required: true
            },
            Description: {
                required: true
            },
            Price: {
                minlength: 2,
                required: true
            },
            Picture: {
                required: true
            },
            Mileage: {
                required: true,
                minlength: 2
            },
            Model: {
                required: true
            },
            Make: {
                required: true
            },
            Year: {
                min: 1900,
                max:2016,
                required: true
            }
        },
        messages: {
            
        }
    });
});