﻿$(document).ready(function() {
    $('#vehicleRequestForm').validate({
        rules: {
            "Requestor.FirstName": {
                required: true
            },
            "Requestor.Email": {
                required: true,
                email: true
            },
            "Requestor.Phone": {
                required: true
            },
            BestContactTime: {
                required: true
            },
            ContactMethod: {
                required: true
            }
        },
        messages: {
            "Requestor.FirstName": "Please enter a first name",
            "Requestor.Email": {
                required: "Please enter your email address",
                email: "That is not a valid email format"
            },
            "Requestor.Phone": "Please enter your phone number",
            BestContactTime: "Please let us know when it is best to contact you",
            ContactMethod: "Please choose a preferred contact method"
        }
    });
});