﻿using CarTell.models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CarTell.data
{
    public class ListingRepository
    {

        public List<Listing> GetAllVehicleListings()
        {
            List<Listing> listings = new List<Listing>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT l.ListingID, l.AdTitle, l.[Description], l.Price, l.Picture, v.Mileage, v.[Year], " +
                                  "m.Manufacturer, v.Model, l.IsAvailable  FROM Listing l INNER JOIN Vehicle v ON l.VehicleID " +
                                  "= v.VehicleID INNER JOIN Make m ON l.VehicleID = m.MakeID";
                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        //if (dr["IsAvailable"].ToString().ToLower() !="false")
                        //{
                            Listing listing = new Listing
                            {
                                ListingID = int.Parse(dr["ListingID"].ToString()),
                                AdTitle = dr["AdTitle"].ToString(),
                                Available = Convert.ToBoolean(dr["IsAvailable"].ToString()),
                                Description = dr["Description"].ToString(),
                                Make = dr["Manufacturer"].ToString(),
                                Mileage = int.Parse(dr["Mileage"].ToString()),
                                Model = dr["Model"].ToString(),
                                Picture = dr["Picture"].ToString(),
                                Price = double.Parse(dr["Price"].ToString()),
                                Year = int.Parse(dr["Year"].ToString())
                            };
                            listings.Add(listing);
                        //}
                    }
                }
            }
            return listings;
        }

        public Listing GetVehicleListingById(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Listing l INNER JOIN Vehicle v ON l.VehicleID = v.VehicleID " +
                                  "INNER JOIN Make m ON v.MakeID = m.MakeID WHERE l.VehicleID = " + id;
                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        if ((bool)dr["isAvailable"])
                        {
                            Listing listing = new Listing()
                            {
                                ListingID = int.Parse(dr["ListingID"].ToString()),
                                AdTitle = dr["AdTitle"].ToString(),
                                Available = (bool)dr["isAvailable"],
                                Description = dr["Description"].ToString(),
                                Make = dr["Manufacturer"].ToString(),
                                Mileage = int.Parse(dr["Mileage"].ToString()),
                                Model = dr["Model"].ToString(),
                                Picture = dr["Picture"].ToString(),
                                Price = int.Parse(dr["Price"].ToString()),
                                Year = int.Parse(dr["Year"].ToString())
                            };
                            return listing;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                throw new Exception();
            }
        }

        public void AddVehicleListing(Listing newListing)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "AddListing";

                int available = 0;
                if (newListing.Available)
                {
                    available = 1;
                }

                cmd.Parameters.AddWithValue("@AdTitle", newListing.AdTitle);
                cmd.Parameters.AddWithValue("@Description", newListing.Description);
                cmd.Parameters.AddWithValue("@Price", newListing.Price);
                cmd.Parameters.AddWithValue("@Picture", newListing.Picture);
                cmd.Parameters.AddWithValue("@Available", available);
                cmd.Parameters.AddWithValue("@Mileage", newListing.Mileage);
                cmd.Parameters.AddWithValue("@Year", int.Parse(newListing.Year.ToString()));
                cmd.Parameters.AddWithValue("@Manufacturer", newListing.Make);
                cmd.Parameters.AddWithValue("@Model", newListing.Model);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        public void RemoveVehicleListing(int id)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "UPDATE Listing SET IsAvailable = 0 WHERE ListingID = " + id;
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        public List<Request> GetAllRequestsByVehicle(int id)
        {
            List<Request> requests = new List<Request>();

            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "SELECT * FROM Request r " +
                                  "INNER JOIN ContactMethod c ON r.ContatMethodID = c.ContactMethodID " +
                                  "INNER JOIN Person p ON r.PersonID = p.PersonID " +
                                  "INNER JOIN RequestStatus s ON r.RequestStatusID = s.RequestStatusID " +
                                  "WHERE ListingID = " + id;
                cmd.Connection = cn;
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        DateTime? tryContact = null;
                        if (dr["LastContact"] != DBNull.Value)
                        {
                            tryContact = DateTime.Parse(dr["LastContact"].ToString());
                        }
                        Request request = new Request()
                        {
                            BestContactTime = dr["BestTime"].ToString(),
                            RequestID = int.Parse(dr["RequestID"].ToString()),
                            ContactMethod = dr["Method"].ToString(),
                            LastContact = tryContact,
                            Timeframe = dr["TimeFrame"].ToString(),
                            Status = dr["Status"].ToString(),
                            Requestor = new Person()
                            {
                                FirstName = dr["FirstName"].ToString(),
                                LastName = dr["LastName"].ToString(),
                                Email = dr["Email"].ToString(),
                                Phone = dr["Phone"].ToString()
                            }
                        };
                        requests.Add(request);
                    }
                }
            }
            return requests;
        }

        public void AddRequestForVehicle(Request newRequest)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "AddRequest";

                int contactMethod = 1;  //Not seeing values in DB, making up that 1 is phone and 2 is email
                if (newRequest.ContactMethod.ToUpper() == "EMAIL")
                {
                    contactMethod = 2;
                }

                cmd.Parameters.AddWithValue("@ListingID", newRequest.ListingID);
                cmd.Parameters.AddWithValue("@FirstName", newRequest.Requestor.FirstName);
                cmd.Parameters.AddWithValue("@LastName", newRequest.Requestor.LastName);
                cmd.Parameters.AddWithValue("@Phone", newRequest.Requestor.Phone);
                cmd.Parameters.AddWithValue("@Email", newRequest.Requestor.Email);
                cmd.Parameters.AddWithValue("@Timeframe", newRequest.Timeframe);
                cmd.Parameters.AddWithValue("@BestTime", newRequest.BestContactTime);
                cmd.Parameters.AddWithValue("@ContactMethod", contactMethod);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        public void EditRequestStatus(int id, int status)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "UPDATE Request " +
                                  "SET RequestStatusID = " + status +
                                  " WHERE ListingID = " + id;
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }

        //public List<SelectListItem> GetListOfMakes()
        //{
        //    List<SelectListItem> makes = new List<SelectListItem>();

        //    using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
        //    {
        //        SqlCommand cmd = new SqlCommand();
        //        cmd.CommandText = "SELECT * FROM Make";
        //        cmd.Connection = cn;
        //        cn.Open();

        //        using (SqlDataReader dr = cmd.ExecuteReader())
        //        {
        //            while (dr.Read())
        //            {
        //                SelectListItem item = new SelectListItem()
        //                {
        //                    Text = dr["Manufacturer"].ToString(),
        //                    Value = dr["MakeID"].ToString()
        //                };
        //                makes.Add(item);
        //            }

        //        }
        //    }
        //    return makes;
        //}
        //get a list of makes & ids
        //select list items 
    }
}
