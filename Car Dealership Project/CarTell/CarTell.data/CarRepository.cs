﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarTell.models;

namespace CarTell.data
{
    public class CarRepository
    {
        public static void AddVehicle(Vehicle car)
        {
            using (SqlConnection cn = new SqlConnection(Settings.ConnectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "AddVehicle";
                cmd.Parameters.AddWithValue("@Make", car.Make);
                cmd.Parameters.AddWithValue("@Model", car.Model);
                cmd.Parameters.AddWithValue("@Mileage", car.Mileage);
                cmd.Parameters.AddWithValue("@Year", car.Year);
                cmd.Parameters.AddWithValue("@IsAvailable", car.IsAvailable);

                SqlParameter outputIDparam = new SqlParameter("@VehicleID", SqlDbType.Int)
                {
                    Direction = ParameterDirection.Output
                };

                cmd.Parameters.Add(outputIDparam);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();


            }
            
        }

        public static void RemoveVehicle(int id)
        {
            using (SqlConnection cn = new SqlConnection())
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "RemoveVehicle";
                cmd.Parameters.AddWithValue("@VehicleID", id);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = cn;
                cn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
