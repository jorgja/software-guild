﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models;

namespace HJJTPortal.UI.Models
{
    public class WorkHistoryVM
    {
        public List<WorkHistory> WorkHistories { get; set; }
        public int id { get; set; }
    }
}
