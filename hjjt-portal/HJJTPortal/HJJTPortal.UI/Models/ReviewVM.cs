﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HJJTPortal.UI.Models
{
    public class ReviewVM
    {
        public Candidate Candidate { get; set; }
        public List<WorkHistory> WorkHistory { get; set; }
        public DesiredJobInfo DesiredJobInfo { get; set; }
        public List<EducationHistory> EducationHistory { get; set; }

        public ReviewVM()
        {
            Candidate = new Candidate();
            WorkHistory = new List<WorkHistory>();
            DesiredJobInfo = new DesiredJobInfo();
            EducationHistory = new List<EducationHistory>();
        }
    }
}