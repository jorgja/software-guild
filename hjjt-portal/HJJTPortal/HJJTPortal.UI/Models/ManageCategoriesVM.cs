﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HJJTPortal.UI.Models
{
    public class ManageCategoriesVM
    {
        public Category editCategory { get; set; }
        public List<Category> Categories { get; set; }

        public ManageCategoriesVM(List<Category> categories)
        {
            Categories = categories;
        }
    }
}