﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models;

namespace HJJTPortal.UI.Models
{
    public class EducationHistoryVM
    {
        public List<EducationHistory> EducationHistories { get; set; }
        public int id { get; set; }
    }
}