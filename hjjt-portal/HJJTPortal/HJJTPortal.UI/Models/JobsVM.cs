﻿using Portal.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HJJTPortal.UI.Models
{
    public class JobsVM
    {
        public List<SelectListItem> JobList { get; set; }
        public DesiredJobInfo DesiredJobInfo { get; set; }
        public Candidate Candidate { get; set; }

        public JobsVM()
        {
            JobList = new List<SelectListItem>
            {
                new SelectListItem { Text = "Developer", Value = "Developer"},
                new SelectListItem { Text = "Project Manager", Value = "Project Manager"},
                new SelectListItem { Text = "Account Manager", Value = "Account Manager"},
                new SelectListItem { Text = "QA Lead", Value="QA Lead" }
            };
        }
    }
}