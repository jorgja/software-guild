﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HJJTPortal.UI.Models
{
    public class EditPolicyVM
    {
        public Policy editPolicy { get; set; }
        public List<Policy> Policies { get; set; }

        //public List<Category> Categories { get; set; }
        public List<SelectListItem> CategoriesDropDown { get; set; }

        public EditPolicyVM(List<Category> categories)
        {
            CategoriesDropDown = new List<SelectListItem>();
            foreach (var cat in categories)
            {
                var ListItem = new SelectListItem { Text = cat.CategoryName, Value = cat.CategoryId.ToString() };
                CategoriesDropDown.Add(ListItem);
            }
        }
    }
}