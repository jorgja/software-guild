using Portal.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace HJJTPortal.UI.Models
{
    public class CandidateCreateVM
    {
        [Required(ErrorMessage = "Please enter your first name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your nast name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter a valid email")]
        [DataType(DataType.EmailAddress)]
        public string EMail { get; set; }

        public Address Address { get; set; }

        [Required(ErrorMessage = "Please enter a phone number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please enter a date of birth")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        public string Position { get; set; }

        public string DesiredSalary { get; set; }

        public List<SelectListItem> AvailablePositions { get; set; }

        public CandidateCreateVM()
        {
            AvailablePositions = new List<SelectListItem>
            {
                new SelectListItem { Text = "Developer", Value = "Developer"},
                new SelectListItem { Text = "Project Manager", Value = "Project Manager"},
                new SelectListItem { Text = "Account Manager", Value = "Account Manager"},
                new SelectListItem { Text = "QA Lead", Value="QA Lead" }
            };
        }
    }
}