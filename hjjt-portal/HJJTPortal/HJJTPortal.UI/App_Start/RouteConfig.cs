﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HJJTPortal.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Apply", "Apply",
              new { controller = "Home", action = "Application" });

            routes.MapRoute("Policies", "Policies",
              new { controller = "Home", action = "ViewPolicies" });

            routes.MapRoute("Default", "{controller}/{action}/{id}",
               new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        }
    }
}