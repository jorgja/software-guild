﻿using HJJTPortal.UI.Models;
using Portal.BLL;
using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HJJTPortal.UI.Controllers
{
    public class PolicyController : Controller
    {
        public ActionResult ViewPolicies()
        {
            var _policyrepo = RepoFactory.CreatePolicyRepository();
            var _categoryrepo = RepoFactory.CreateCategoryRepository();
            var policies = _policyrepo.GetAll();
            var categories = _categoryrepo.GetAll();
            foreach (var p in policies)
            {
                foreach (var c in categories.Where(c => p.CategoryId == c.CategoryId))
                {
                    p.CategoryName = c.CategoryName;
                }
            }
            return View(policies);
        }

        public ActionResult ManagePolicies()
        {
            var _policyrepo = RepoFactory.CreatePolicyRepository();
            var _categoryrepo = RepoFactory.CreateCategoryRepository();
            var policies = _policyrepo.GetAll();
            var categories = _categoryrepo.GetAll();
            foreach (var p in policies)
            {
                foreach (var c in categories.Where(c => p.CategoryId == c.CategoryId))
                {
                    p.CategoryName = c.CategoryName;
                }
            }
            return View(policies);
        }

        [HttpGet]
        public ActionResult AddPolicies()
        {
            var _policyrepo = RepoFactory.CreatePolicyRepository();
            var _categoryrepo = RepoFactory.CreateCategoryRepository();
            var vm = new AddPoliciesVM(_categoryrepo.GetAll(), _policyrepo.GetAll());

            return View(vm);
        }

        [HttpPost]
        public ActionResult AddPolicies(Policy newPolicy)
        {
            if (ModelState.IsValid)
            {
                var _repo = RepoFactory.CreatePolicyRepository();
                _repo.Create(newPolicy);
                return RedirectToAction("ViewPolicies");
            }
            var _policyrepo = RepoFactory.CreatePolicyRepository();
            var _categoryrepo = RepoFactory.CreateCategoryRepository();
            var vm = new AddPoliciesVM(_categoryrepo.GetAll(), _policyrepo.GetAll());
            vm.newPolicy = newPolicy;
            return View(vm);
        }

        public ActionResult DeletePolicy(int id)
        {
            var _repo = RepoFactory.CreatePolicyRepository();
            _repo.Delete(id);
            return RedirectToAction("ManagePolicies");
        }

        public ActionResult EditPolicy(int id)
        {
            var _policyrepo = RepoFactory.CreatePolicyRepository();
            var _categoryrepo = RepoFactory.CreateCategoryRepository();
            var vm = new EditPolicyVM(_categoryrepo.GetAll());
            vm.editPolicy = _policyrepo.GetById(id);
            return View(vm);
        }

        [HttpPost]
        public ActionResult EditPolicy(Policy editPolicy)
        {
            var _repo = RepoFactory.CreatePolicyRepository();
            _repo.Update(editPolicy.PolicyId, editPolicy);
            return RedirectToAction("ManagePolicies");
        }

        public ActionResult ManageCategories()
        {
            var _categoryrepo = RepoFactory.CreateCategoryRepository();
            var categories = _categoryrepo.GetAll();
            return View(categories);
        }

        public ActionResult DeleteCategory(int id)
        {
            var _repo = RepoFactory.CreateCategoryRepository();
            _repo.Delete(id);
            return RedirectToAction("ManageCategories");
        }

        public ActionResult AddCategories()
        {
            var _categoryrepo = RepoFactory.CreateCategoryRepository();
            var vm = new ManageCategoriesVM(_categoryrepo.GetAll());
            return View(vm);
        }

        [HttpPost]
        public ActionResult AddCategories(Category editCategory)
        {
            var _repo = RepoFactory.CreateCategoryRepository();
            _repo.Create(editCategory);
            return RedirectToAction("ManageCategories");
        }

        public ActionResult EditCategory(int id)
        {
            var _categoryrepo = RepoFactory.CreateCategoryRepository();
            var vm = new ManageCategoriesVM(_categoryrepo.GetAll());
            vm.editCategory = _categoryrepo.GetById(id);
            return View(vm);
        }

        [HttpPost]
        public ActionResult EditCategory(Category editCategory)
        {
            var _repo = RepoFactory.CreateCategoryRepository();
            _repo.Update(editCategory.CategoryId, editCategory);
            return RedirectToAction("ManageCategories");
        }
    }
}