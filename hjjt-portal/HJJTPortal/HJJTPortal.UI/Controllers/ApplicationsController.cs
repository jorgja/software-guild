﻿using HJJTPortal.UI.Models;
using Portal.BLL;
using Portal.Models;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HJJTPortal.UI.Controllers
{
    public class CandidatesController : Controller
    {
        private Operations ops = new Operations();

        public ActionResult Index()
        {
            var candidates = ops.GetAllCandidates();
            return View(candidates);
        }

        public ActionResult Create()
        {
            var vm = new CandidateCreateVM();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Create(Candidate model)
        {
            if (ModelState.IsValid)
            {
                var response = ops.CreateCandidate(model);
                if (response.Success)
                {
                    return RedirectToAction("AddWorkHistory", new { candidateid = model.CandidateId });
                }
                ModelState.AddModelError("", response.Message);
            }
            var vm = new CandidateCreateVM();
            return View(vm);
        }

        public ActionResult AddWorkHistory(int candidateid)
        {
            var model = new WorkHistory();
            model.CandidateId = candidateid;
            return View(model);
        }

        [HttpPost]
        public ActionResult AddWorkHistory(WorkHistory model)
        {
            if (Request.Form.AllKeys.Contains("Next"))
            {
                return RedirectToAction("AddEducationHistory", new { id = model.CandidateId });
            }
            if (ModelState.IsValid)
            {
                var response = ops.CreateWorkHistory(model);
                if (response.Success)
                {
                    ModelState.Clear();
                    int id = model.CandidateId;
                    model = new WorkHistory();
                    model.CandidateId = id;
                    return View(model);
                }
                ModelState.AddModelError("", response.Message);
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult AddEducationHistory(EducationHistory model)
        {
            if (Request.Form.AllKeys.Contains("Next"))
            {
                return RedirectToAction("Index");
            }
            if (ModelState.IsValid)
            {
                var response = ops.CreateEducationHistory(model);
                if (response.Success)
                {
                    int id = model.CandidateId;
                    model = new EducationHistory();
                    model.CandidateId = id;
                    return View(model);
                }
                ModelState.AddModelError("", response.Message);
            }
            return View(model);
        }

        public ActionResult AddEducationHistory(int candidateid)
        {
            var model = new EducationHistory();
            model.CandidateId = candidateid;
            return View(model);
        }

        public ActionResult Review(int id)
        {
            var Model = new ReviewVM()
            {
                Candidate = ops.GetCandidateByID(id),
                DesiredJobInfo = ops.GetDesiredJobInfoByID(id),
                WorkHistory = ops.GetHistoryByID(id),
                EducationHistory = ops.GetEducationHistoryByID(id)
            };
            return View(Model);
        }
    }
}