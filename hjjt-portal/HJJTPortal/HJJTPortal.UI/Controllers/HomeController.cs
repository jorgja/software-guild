﻿using HJJTPortal.UI.Models;
using Portal.BLL;
using Portal.Data;
using Portal.Data.Interfaces;
using Portal.Data.Policies;
using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HJJTPortal.UI.Controllers
{
    public class HomeController : Controller
    {
        private Operations ops = new Operations();

        public HomeController()
        {
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Careers()
        {
            return View();
        }

        //public ActionResult Application()
        //{
        //    return View(new Candidate());
        //}

        //[HttpPost]
        //public ActionResult Application(Candidate candidate)
        //{
        //    if (candidate.PhoneNumber.Length != 12)
        //    {
        //        ModelState.AddModelError("PhoneNumber", "Please enter a valid phone number");
        //    }

        //    if (DateTime.Now.Year - candidate.BirthDate.Year < 16)
        //    {
        //        ModelState.AddModelError("Birthdate", "You must be at least 16");
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        ops.CreateCandidate(candidate);
        //        return RedirectToAction("EduHx", new { id = candidate.CandidateId });
        //    }
        //    return View();
        //}

        //public ActionResult EduHx(int id)
        //{
        //    var model = new EducationHistoryVM()
        //    {
        //        EducationHistories = new List<EducationHistory>()
        //        {
        //            new EducationHistory()
        //            {
        //                CandidateId = id
        //            }
        //        }
        //    };
        //    model.id = id;

        //    if (ops.GetHistoryByID(id).Count > 0)
        //    {
        //        model.EducationHistories = ops.GetEducationHistoryByID(id);
        //    }
        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult EduHx(List<EducationHistory> educationHx)
        //{
        //    if (Request.Form.AllKeys.Contains("Next"))
        //    {
        //        if (ModelState.IsValid && ops.GetHistoryByID(educationHx[0].CandidateId) != null)
        //        {
        //            return RedirectToAction("Review", new { id = educationHx[0].CandidateId });
        //        }

        //        if (ModelState.IsValid)
        //        {
        //            // ops.CreateEducationHistory(educationHx);
        //            return RedirectToAction("WorkHx", new { id = educationHx[0].CandidateId });
        //        }
        //    }
        //    else if (Request.Form.AllKeys.Contains("Add"))
        //    {
        //        educationHx.Add(new EducationHistory());

        //        var model = new EducationHistoryVM()
        //        {
        //            EducationHistories = educationHx,
        //        };

        //        return View(model);
        //    }
        //    return View();
        //}

        //public ActionResult WorkHx(int id)
        //{
        //    var model = new WorkHistoryVM();
        //    model.id = id;
        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult WorkHx(List<WorkHistory> workHistories)
        //{
        //    foreach (var workHistory in workHistories)
        //    {
        //        if (workHistory.CurrentlyWorking == true && workHistory.EndDate != null)
        //        {
        //            ModelState.AddModelError("EndDate", "Current job can't have an end date");
        //        }

        //        if (workHistory.CurrentlyWorking == false && workHistory.EndDate == null)
        //        {
        //            ModelState.AddModelError("EndDate", "Previous job must have an end date");
        //        }
        //    }
        //    if (ModelState.IsValid)
        //    {
        //        //ops.CreateWorkHistory(workHistories);
        //        return RedirectToAction("DesiredJobInfo", new { id = workHistories[0].CandidateId });
        //    }

        //    return View();
        //}

        //public ActionResult DesiredJobInfo(int id)
        //{
        //    var model = new JobsVM();
        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult DesiredJobInfo(DesiredJobInfo djInfo)
        //{
        //    ops.CreateDesiredJobInfo(djInfo);
        //    return RedirectToAction("Review", new { id = djInfo.CandidateId });
        //}

        //public ActionResult Review(int id)
        //{
        //    var Model = new ReviewVM()
        //    {
        //        Candidate = ops.GetCandidateByID(id),
        //        DesiredJobInfo = ops.GetDesiredJobInfoByID(id),
        //        WorkHistory = ops.GetHistoryByID(id),
        //        EducationHistory = ops.GetEducationHistoryByID(id)
        //    };
        //    return View(Model);
        //}

        //public ActionResult Candidates()
        //{
        //    var _repo = RepoFactory.CreateCandidateRepository();
        //    return View(_repo.GetAll());
        //}

        public ActionResult Login()
        {
            return RedirectToAction("Login");
        }
    }
}