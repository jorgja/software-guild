﻿using System.ComponentModel.DataAnnotations;

namespace Portal.Models
{
    public class Login
    {
        [Required(ErrorMessage = "You must enter your LoginID")]
        public string LoginID { get; set; }

        [Required(ErrorMessage = "You must enter a valid password")]
        public string Password { get; set; }
    }
}