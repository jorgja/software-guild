﻿namespace Portal.Models
{
    public enum CandidateStatus
    {
        InProgress,
        Active,

        InActive

        //soft delete = InActive
    }
}