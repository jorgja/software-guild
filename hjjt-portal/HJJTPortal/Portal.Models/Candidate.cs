﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Models
{
    public class Candidate
    {
        public Candidate()
        {
            Address = new Address();
            //WorkHistory = new List<WorkHistory>();
            //EducationHistory = new List<EducationHistory>();
            //DesiredJobInfo = new DesiredJobInfo();
        }

        public int CandidateId { get; set; }

        [Required(ErrorMessage = "Please enter your first name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage ="Please enter your nast name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter a valid email")]
        [DataType(DataType.EmailAddress)]
        public string EMail { get; set; }

        public Address Address { get; set; }

        [Required(ErrorMessage = "Please enter a phone number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Please enter a date of birth")]
        [DataType(DataType.Date)]
        public DateTime BirthDate { get; set; }

        //public List<WorkHistory> WorkHistory { get; set; }
        //public List<EducationHistory> EducationHistory { get; set; }
        //public DesiredJobInfo DesiredJobInfo { get; set; }

        public CandidateStatus CandidateStatus { get; set; }

        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    List<ValidationResult> errors = new List<ValidationResult>();

        //    if (string.IsNullOrEmpty(FirstName))
        //    {
        //        errors.Add(new ValidationResult("Please enter a first name", new[] { "FirstName" }));
        //    }
        //    if (string.IsNullOrEmpty(LastName))
        //    {
        //        errors.Add(new ValidationResult("Please enter a last name", new[] { "LastName" }));
        //    }
        //    if (string.IsNullOrEmpty(EMail) || !EMail.Contains("@"))
        //    {
        //        errors.Add(new ValidationResult("Please enter a valid email address", new[] { "Email" }));
        //    }
        //    if (string.IsNullOrEmpty(PhoneNumber))
        //    {
        //        errors.Add(new ValidationResult("Please enter a phone number", new[] { PhoneNumber }));
        //    }
        //    if (DateTime.Now - BirthDate <= TimeSpan.FromDays(16*360))
        //    {
        //        errors.Add(new ValidationResult("You must be at least 16 to apply."));
        //    }
        //    return errors;
        //}
    }
}