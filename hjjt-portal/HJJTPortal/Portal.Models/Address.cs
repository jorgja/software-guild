﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Portal.Models
{
    public class Address : IValidatableObject
    {
        public string Street { get; set; }

        public string City { get; set; }
        public string State { get; set; }

        [DataType(DataType.PostalCode)]
        public string Zipcode { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (string.IsNullOrEmpty(Street))
            {
                errors.Add(new ValidationResult("Please enter street address", new[] { "Street" }));
            }
            if (string.IsNullOrEmpty(City))
            {
                errors.Add(new ValidationResult("Please enter city for address", new[] { "City" }));
            }
            if (string.IsNullOrEmpty(State))
            {
                errors.Add(new ValidationResult("Please enter state of address", new[] { "State" }));
            }
            if (string.IsNullOrEmpty(Zipcode))
            {
                errors.Add(new ValidationResult("Please enter zipcode for address", new[] { "ZipCode" }));
            }
            return errors;
        }
    }
}