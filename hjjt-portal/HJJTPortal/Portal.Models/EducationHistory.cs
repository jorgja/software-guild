﻿using System.ComponentModel.DataAnnotations;

namespace Portal.Models
{
    public class EducationHistory
    {
        public int CandidateId { get; set; }

        [Required(ErrorMessage = "Please enter institution name")]
        public string InstitutionName { get; set; }

        [Required(ErrorMessage = "Please enter institution city")]
        public string InstitutionCity { get; set; }

        [Required(ErrorMessage = "Please enter institution state")]
        public string InstitutionState { get; set; }

        public string FieldOfStudy { get; set; }
        public bool IsGraduated { get; set; }

        [Required(ErrorMessage = "Please enter what degree was earned")]
        public string DegreeEarned { get; set; }
    }
}