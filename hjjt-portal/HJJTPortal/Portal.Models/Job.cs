﻿namespace Portal.Models
{
    public class Job
    {
        public int JobId { get; set; }
        public string Position { get; set; }
        public string Description { get; set; }
        public bool IsPositionFilled { get; set; }
    }
}