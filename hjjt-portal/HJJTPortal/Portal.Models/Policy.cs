﻿using System.ComponentModel.DataAnnotations;

namespace Portal.Models
{
    public class Policy
    {
        public int PolicyId { get; set; }

        [Required]
        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        [Required(ErrorMessage = "Please enter a policy name")]
        public string PolicyName { get; set; }

        [Required(ErrorMessage = "Please enter a description")]
        public string Description { get; set; }
    }
}