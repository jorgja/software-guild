﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Portal.Models
{
    public class WorkHistory
    {
        public int CandidateId { get; set; }

        [Required(ErrorMessage = "Please enter previous employer's name")]
        public string EmployerName { get; set; }

        [Required(ErrorMessage = "Please enter city of previous employer")]
        public string City { get; set; }

        [Required(ErrorMessage = "Please enter state of previous employer")]
        public string State { get; set; }

        [Required(ErrorMessage = "Please enter start date")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        public string Duties { get; set; }

        [Required(ErrorMessage = "Please enter supervisor's name")]
        public string SupervisorName { get; set; }

        [Required(ErrorMessage = "Please enter employer's phone number")]
        public string SupervisorPhone { get; set; }

        public bool CurrentlyWorking { get; set; }
    }
}