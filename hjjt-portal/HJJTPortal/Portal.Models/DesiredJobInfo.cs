﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Portal.Models
{
    public class DesiredJobInfo : IValidatableObject
    {
        public int CandidateId { get; set; }

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        public int YearlySalary { get; set; }

        //VM needs created for List of Jobs
        public string DesiredJob { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (StartDate == null)
            {
                errors.Add(new ValidationResult("Please enter date you're able to start working", new[] { "StartDate" }));
            }
            if (YearlySalary == 0)
            {
                errors.Add(new ValidationResult("Please enter salary requirement", new[] { "YearlySalary" }));
            }
            return errors;
        }
    }
}