﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Portal.Data
{
    public class FileRepository : IPortalRepository
    {
        private List<Candidate> listOfCandidates = new List<Candidate>();

        public void AddCandidate(Candidate c)
        {
            //string path = Server.MapPath(ConfigurationManager.AppSettings["FileLocation"]);
            string path = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["FileLocation"]);
            if (File.Exists(path))
            {
                c.CandidateId = GetLastCandidateId();

                var writer = new StreamWriter(path);

                using (writer)
                {
                    //writer.Write("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", c.CandidateId, c.FirstName, c.LastName, c.EMail, c.PhoneNumber,
                    //    c.BirthDate, c.Address.City, c.Address.State, c.Address.Street, c.Address.Zipcode, c.Active);
                    foreach (var job in c.WorkHistory)
                    {
                        writer.Write(",&&&,{0},{1},{2},{3},{4},{5},{6},{7},{8}", job.EmployerName, job.State, job.City, job.StartDate, job.EndDate,
                            job.SupervisorName, job.SupervisorPhone, job.Duties, job.CurrentlyWorking);
                    }
                    foreach (var e in c.EducationHistory)
                    {
                        writer.Write(",###,{0},{1},{2},{3},{4},{5}", e.InstitutionName, e.InstitutionCity, e.InstitutionState, e.DegreeEarned, e.FieldOfStudy, e.IsGraduated);
                    }
                }
            }
        }

        public void AddEducationHistory(List<EducationHistory> educationHistory, int id)
        {
            throw new NotImplementedException();
        }

        public void AddWorkHistory(List<WorkHistory> workHistory, int id)
        {
            throw new NotImplementedException();
        }

        public void Delete(Candidate canidate)
        {
            throw new NotImplementedException();
        }

        public void Edit(Candidate candidate)
        {
            throw new NotImplementedException();
        }

        public List<Candidate> GetAll()
        {
            List<Candidate> canadidates = new List<Candidate>();

            //string path = System.Configuration.ConfigurationSettings.AppSettings["FileLocation"];

            //var reader = File.ReadAllLines(path);

            //for (int i = 0; i < reader.Length; i++)
            //{
            //    var
            //}
            return canadidates;
        }

        public Candidate GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public int GetLastCandidateId()
        {
            string path = ConfigurationManager.AppSettings["FileLocation"];

            var reader = File.ReadAllLines(path);

            return reader.Length + 1;
        }
    }
}