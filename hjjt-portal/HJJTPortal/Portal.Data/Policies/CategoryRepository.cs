﻿using Portal.Data.Interfaces;
using Portal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Portal.Data.Policies
{
    public class CategoryRepository : ICategoryRepository
    {
        private List<Category> Categories = new List<Category>();
        public List<Policy> Policies = new List<Policy>();
        private string FilePath;

        public CategoryRepository()
        {
            FilePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Categories"]);
        }

        public List<Category> GetAll()
        {
            List<Category> Categories = new List<Category>();
            var reader = File.ReadAllLines(FilePath);

            for (int i = 0; i < reader.Length; i++)
            {
                var columns = reader[i].Split('#');
                var c = new Category();

                c.CategoryId = int.Parse(columns[0]);
                c.CategoryName = columns[1];

                Categories.Add(c);
            }
            return Categories;
        }

        public Category GetById(int id)
        {
            var Categories = GetAll();
            var results = from c in Categories
                          select c;
            return results.FirstOrDefault(result => result.CategoryId == id);
        }

        public Category Create(Category model)
        {
            Categories = GetAll();
            if (Categories.Any())
            {
                int nextId = Categories.Max(c => c.CategoryId);
                nextId++;
                model.CategoryId = nextId;
                Categories.Add(model);
                Save();
                return model;
            }
            model.CategoryId = 1;
            Categories.Add(model);

            Save();

            return model;
        }

        public void Update(int id, Category model)
        {
            Categories = GetAll();
            Category c = Categories.SingleOrDefault(co => co.CategoryId == id);
            c.CategoryId = id;
            c.CategoryName = model.CategoryName;
            Save();
        }

        public void Delete(int id)
        {
            Categories = GetAll();
            var categorytoRemove = Categories.FirstOrDefault(c => c.CategoryId == id);
            Categories.Remove(categorytoRemove);
            var lostPolicies = Policies.FindAll(p => p.CategoryId == id);
            foreach (var p in lostPolicies)
            {
                p.CategoryId = 9999999;
            }

            Save();
        }

        private void Save()
        {
            var writer = new StreamWriter(FilePath, false);
            using (writer)
            {
                foreach (var c in Categories)
                {
                    writer.WriteLine("{0}#{1}", c.CategoryId, c.CategoryName);
                }
            }
        }
    }
}