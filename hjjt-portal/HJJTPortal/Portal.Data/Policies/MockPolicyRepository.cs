﻿using Portal.Data.Interfaces;
using Portal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Portal.Data.Policies
{
    public class MockPolicyRepository : IPolicyRepository
    {
        private List<Policy> Policies = new List<Policy>();
        private string FilePath;

        public MockPolicyRepository()
        {
            FilePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Policies"]);
        }

        public List<Policy> GetAll()
        {
            List<Policy> Policies = new List<Policy>();
            var reader = File.ReadAllLines(FilePath);

            for (int i = 0; i < reader.Length; i++)
            {
                var columns = reader[i].Split('#');
                var p = new Policy();

                p.PolicyId = int.Parse(columns[0]);
                p.CategoryId = int.Parse(columns[1]);
                p.PolicyName = columns[2];
                p.Description = columns[3];

                Policies.Add(p);
            }

            return Policies;
        }

        public Policy GetById(int id)
        {
            var Policies = GetAll();
            var results = from p in Policies
                          select p;
            return results.FirstOrDefault(result => result.PolicyId == id);
        }

        public Policy Create(Policy model)
        {
            Policies = GetAll();
            if (Policies.Any())
            {
                int nextId = Policies.Max(p => p.PolicyId);
                nextId++;
                model.PolicyId = nextId;
                Policies.Add(model);
                return model;
            }

            model.PolicyId = 1;
            Policies.Add(model);

            return model;
        }

        public void Update(int id, Policy model)
        {
            Policies = GetAll();
            Policy p = Policies.SingleOrDefault(po => po.PolicyId == id);
            p.PolicyId = id;
            p.PolicyName = model.PolicyName;
            p.CategoryId = model.CategoryId;
            p.CategoryName = model.CategoryName;
            p.Description = model.Description;
        }

        public void Delete(int id)
        {
            Policies = GetAll();
            var policytoRemove = Policies.FirstOrDefault(p => p.PolicyId == id);
            Policies.Remove(policytoRemove);
        }
    }
}