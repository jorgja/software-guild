﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.Models;

namespace Portal.Data.MockRepos
{
    public class WorkHistoryMockRepository : IWorkHistoryRepository
    {
        private List<WorkHistory> workHistoryList = new List<WorkHistory>();

        public WorkHistory Create(WorkHistory model)
        {
            workHistoryList.Add(model);
            return model;
        }

        public void Delete(int id)
        {
            workHistoryList.RemoveAll(c => c.CandidateId == id);  //Should we remove all WH records tied to candidate?
        }                                                         //Need unique key if not

        public List<WorkHistory> GetAll()
        {
            return workHistoryList;
        }

        public List<WorkHistory> GetByCandidateById(int id)
        {
            return workHistoryList.Where(c => c.CandidateId == id).ToList();
        }

        public WorkHistory GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, WorkHistory model)
        {
            throw new NotImplementedException();        //Again probably need a unique key, over 1 per candidate and it
        }                                               //wouldn't know which to update
    }
}
