﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.Models;

namespace Portal.Data.MockRepos
{
    public class DesiredJobMockRepository : IDesiredJobInfoRepository
    {
        private List<DesiredJobInfo> jobInfoList = new List<DesiredJobInfo>();

        public DesiredJobInfo Create(DesiredJobInfo model)
        {
            throw new NotImplementedException();
        }

        public Candidate Create(Candidate model)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<DesiredJobInfo> GetAll()
        {
            return jobInfoList;
        }

        public Candidate GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, DesiredJobInfo model)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, Candidate model)
        {
            throw new NotImplementedException();
        }

        DesiredJobInfo IDesiredJobInfoRepository.GetById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
