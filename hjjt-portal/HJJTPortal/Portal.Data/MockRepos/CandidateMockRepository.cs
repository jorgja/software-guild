﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.Data.Interfaces;

namespace Portal.Data
{
    public class CandidateMockRepository : ICandidateRepository
    {
        private List<Candidate> candidateList = new List<Candidate>();

        public List<Candidate> GetAll()
        {
            if (candidateList.Count() == 0)
            {
                return new List<Candidate>()
            {
                new Candidate()
                {
                    CandidateId = 1,
                    FirstName = "Hannah",
                    LastName = "Seligson",
                    EMail = "hseligson@youwish.com",
                    PhoneNumber = "555-7654",
                    BirthDate = new DateTime(1987, 10, 23),

                    Address = new Address()
                    {
                        State = "OH",
                        City = "Akron",
                        Zipcode = "55555",
                        Street = "123 Fake St"
                    },

                    //WorkHistory = new List<WorkHistory>()
                    //{
                    //   new WorkHistory()
                    //   {
                    //       EmployerName = "SWG",
                    //       City = "Akron",
                    //       State = "OH",
                    //       StartDate = new DateTime(2,1,16),
                    //       EndDate = null,
                    //       Duties = "Wrote cool code",
                    //       SupervisorName = "Victor",
                    //       SupervisorPhone = "555-5555",
                    //       CurrentlyWorking = true
                    //   }
                    //}
                },

                new Candidate()
                {
                    CandidateId = 2,
                    FirstName = "Randall",
                    LastName = "Clapper",
                    EMail = "rsclapper@youwish.com",
                    PhoneNumber = "999-7654",
                    BirthDate = new DateTime(1985, 09, 16),

                    Address = new Address()
                    {
                        State = "OH",
                        City = "Akron",
                        Zipcode = "55555",
                        Street = "123 Fake St"
                    }

                    //WorkHistory = new List<WorkHistory>()
                    //{
                    //   new WorkHistory()
                    //   {
                    //       EmployerName = "SWG",
                    //       City = "Akron",
                    //       State = "OH",
                    //       StartDate = new DateTime(2,1,16),
                    //       EndDate = null,
                    //       Duties = "Wrote cool code",
                    //       SupervisorName = "Victor",
                    //       SupervisorPhone = "555-5555",
                    //       CurrentlyWorking = true
                    //   }
                    //}
                }
            };
            }
            else return candidateList;
        }

        public Candidate AddCandidate(Candidate newCandidate)
        {
            if (candidateList.Count() > 0)
            {
                newCandidate.CandidateId = candidateList.Count() + 1;
            }
            else newCandidate.CandidateId = 1;

            newCandidate.CandidateStatus = CandidateStatus.InProgress;

            candidateList.Add(newCandidate);
            return newCandidate;
        }

        public void Delete(int id)
        {
            var candidate = candidateList.FirstOrDefault(c => c.CandidateId == id);
            candidate.CandidateStatus = CandidateStatus.InActive;
        }

        public void Update(int id, Candidate candidate)
        {
            candidateList[candidate.CandidateId - 1] = candidate;
        }

        public Candidate GetById(int id)
        {
            return candidateList.FirstOrDefault(c => c.CandidateId == id);
        }
    }
}