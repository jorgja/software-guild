﻿using Portal.Data.Interfaces;
using Portal.Models;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace Portal.Data.MockRepos
{
    public class MockPolicyRepository : IPolicyRepository
    {
        private List<Policy> Policies { get; set; }
        private Dictionary<int, List<Policy>> polDictionary = new Dictionary<int, List<Policy>>();
        public string FilePath { get; set; }

        public MockPolicyRepository()
        {
            FilePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Policies"]);
            Policies = LoadPolicies();
        }

        public List<Policy> LoadPolicies()
        {
            List<Policy> Policies = new List<Policy>();
            var reader = File.ReadAllLines(FilePath);

            for (int i = 0; i < reader.Length; i++)
            {
                var columns = reader[i].Split('#');
                var p = new Policy();

                p.PolicyId = int.Parse(columns[0]);
                p.Category = (Categories)int.Parse(columns[1]);
                p.PolicyName = columns[2];
                p.Description = columns[3];

                Policies.Add(p);
            }

            return Policies;
        }

        public List<Policy> GetAll()
        {
            return Policies;
        }

        public Policy GetById(int id)
        {
            Policies = GetAll();
            var results = from p in Policies
                          select p;
            return results.FirstOrDefault(result => result.PolicyId == id);
        }

        public Policy Create(Policy model)
        {
            if (polDictionary.ContainsKey(model.PolicyId))
            {
                int nextId = polDictionary[model.PolicyId].Max(p => p.PolicyId);
                nextId++;
                model.PolicyId = nextId;
                polDictionary[model.PolicyId].Add(model);
            }
            else
            {
                model.PolicyId = 1;
                polDictionary.Add(model.PolicyId, new List<Policy>() { model });
            }
            this.Save();

            return model;
        }

        public void Update(int id, Policy model)
        {
            Policies[id] = model;

            Save();
        }

        public void Delete(int id)
        {
            polDictionary[id].Remove(polDictionary[id].FirstOrDefault(p => p.PolicyId == id));
            Save();
        }

        private void Save()
        {
            var writer = new StreamWriter(FilePath, false);
            using (writer)
            {
                foreach (var p in Policies)
                {
                    writer.Write("{0}#{1}#{2}#{3}", p.PolicyId, p.Category, p.PolicyName, p.Description);
                }
            }
        }
    }
}