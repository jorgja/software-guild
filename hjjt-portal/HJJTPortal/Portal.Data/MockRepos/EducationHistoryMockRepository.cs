﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Portal.Models;

namespace Portal.Data.MockRepos
{
    public class EducationHistoryMockRepository : IEducationHistoryRepository
    {
        private List<EducationHistory> educationHistory = new List<EducationHistory>();

        public EducationHistory Create(EducationHistory model)
        {
            educationHistory.Add(model);
            return model;
        }

        public void Delete(int id)
        {
            educationHistory.RemoveAll(c => c.CandidateId == id);
        }

        public List<EducationHistory> GetAll()
        {
            return educationHistory;
        }

        public List<EducationHistory> GetByCandidateById(int id)
        {
            return educationHistory.Where(c => c.CandidateId == id).ToList();
        }

        public EducationHistory GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, EducationHistory model)
        {
            throw new NotImplementedException();
        }
    }
}
