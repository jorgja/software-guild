﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Portal.Data.Interfaces;

namespace Portal.Data
{
    public class RepositoryFactory
    {
        public static ICandidateRepository CreateRepository()
        {

            return new CandidateMockRepository();
        }
    }
}
