﻿using Portal.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Data.Interfaces
{
    public interface IPolicyRepository
    {
        List<Policy> GetAll();

        Policy GetById(int id);

        Policy Create(Policy model);

        void Update(int id, Policy model);

        void Delete(int id);
    }
}