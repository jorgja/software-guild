using System.Collections.Generic;
using Portal.Models;

namespace Portal.Data
{
    public interface IEducationHistoryRepository
    {
        List<EducationHistory> GetAll();

        List<EducationHistory> GetByCandidateById(int id);

        EducationHistory GetById(int id);

        EducationHistory Create(EducationHistory model);

        void Update(int id, EducationHistory model);

        void Delete(int id);
    }
}