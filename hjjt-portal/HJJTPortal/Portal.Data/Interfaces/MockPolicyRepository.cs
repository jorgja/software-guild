﻿using Portal.Models;
using System;
using System.Collections.Generic;

namespace Portal.Data.Interfaces
{
    public class MockPolicyRepository : IPolicyRepository
    {
        public List<Policy> GetAll()
        {
            throw new NotImplementedException();
        }

        public Policy GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Policy Create(Policy model)
        {
            throw new NotImplementedException();
        }

        public void Update(int id, Policy model)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }
    }
}