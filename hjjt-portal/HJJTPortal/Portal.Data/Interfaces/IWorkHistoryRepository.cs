﻿using System.Collections.Generic;
using Portal.Models;

namespace Portal.Data
{
    public interface IWorkHistoryRepository
    {
        List<WorkHistory> GetAll();

        List<WorkHistory> GetByCandidateById(int id);

        WorkHistory GetById(int id);

        WorkHistory Create(WorkHistory model);

        void Update(int id, WorkHistory model);

        void Delete(int id);
    }
}