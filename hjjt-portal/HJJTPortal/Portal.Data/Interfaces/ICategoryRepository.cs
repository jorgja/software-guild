﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Portal.Data.Interfaces
{
    public interface ICategoryRepository
    {
        List<Category> GetAll();

        Category GetById(int id);

        Category Create(Category model);

        void Update(int id, Category model);

        void Delete(int id);
    }
}