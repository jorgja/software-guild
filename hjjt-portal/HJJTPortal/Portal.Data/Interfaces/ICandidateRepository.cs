﻿using System.Collections.Generic;
using Portal.Models;

namespace Portal.Data.Interfaces
{
    public interface ICandidateRepository
    {
        List<Candidate> GetAll();

        Candidate GetById(int id);

        Candidate AddCandidate(Candidate model);

        void Update(int id, Candidate model);

        void Delete(int id);
    }
}