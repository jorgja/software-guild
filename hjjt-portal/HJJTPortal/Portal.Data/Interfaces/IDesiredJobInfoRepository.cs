using System.Collections.Generic;
using Portal.Models;

namespace Portal.Data
{
    public interface IDesiredJobInfoRepository
    {
        List<DesiredJobInfo> GetAll();

        DesiredJobInfo GetById(int id);

        DesiredJobInfo Create(DesiredJobInfo model);

        void Update(int id, DesiredJobInfo model);

        void Delete(int id);
    }
}