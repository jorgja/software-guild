﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Portal.Data.FileRepos
{
    public class DesiredJobInfoFileRepo : IDesiredJobInfoRepository
    {
        private List<DesiredJobInfo> list { get; set; }
        public string FilePath { get; set; }

        public DesiredJobInfoFileRepo()
        {
            FilePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["DesiredJobInfo"]);
            list = Loadlist();
        }

        private List<DesiredJobInfo> Loadlist()
        {
            List<DesiredJobInfo> localList = new List<DesiredJobInfo>();

            var reader = File.ReadAllLines(FilePath);

            for (int i = 0; i < reader.Length; i++)
            {
                var columns = reader[i].Split('#');

                var d = new DesiredJobInfo();
                d.CandidateId = int.Parse(columns[0]);
                d.StartDate = DateTime.Parse(columns[1]);
                d.YearlySalary = int.Parse(columns[2]);
                d.DesiredJob = columns[3];

                localList.Add(d);
            }
            return localList;
        }

        public List<DesiredJobInfo> GetAll()
        {
            return list;
        }

        public List<DesiredJobInfo> GetByCandidateById(int id)
        {
            return new List<DesiredJobInfo>(list.Where(h => h.CandidateId == id));
        }

        public DesiredJobInfo GetById(int id)
        {
            return list.FirstOrDefault(r => r.CandidateId == id);
        }

        public DesiredJobInfo Create(DesiredJobInfo model)
        {
            list.Add(model);
            Save();
            return model;
        }

        public void Update(int id, DesiredJobInfo model)
        {
            throw new NotImplementedException();
            //need unique identifiers... otherwise this would overwrite all the eduhx
        }

        public void Delete(int id)
        {
            //foreach (var h in list)
            //{
            //    if (h.CandidateId == id)
            //    {
            //        list.Remove(h);
            //    }

            //}
            throw new NotImplementedException();
        }

        private void Save()
        {
            var writer = new StreamWriter(FilePath, false);

            using (writer)
            {
                foreach (var h in list)
                    writer.WriteLine("{0}#{1}#{2}#{3}", h.CandidateId, h.StartDate.Date,
                        h.YearlySalary, h.DesiredJob);
            }
        }
    }
}