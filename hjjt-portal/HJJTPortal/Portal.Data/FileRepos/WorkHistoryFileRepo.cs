﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Portal.Data.FileRepos
{
    public class WorkHistoryFileRepo : IWorkHistoryRepository
    {
        private List<WorkHistory> Histories { get; set; }
        public string FilePath { get; set; }

        public WorkHistoryFileRepo()
        {
            FilePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["WorkHistories"]);
            Histories = LoadHistories();
        }

        private List<WorkHistory> LoadHistories()
        {
            List<WorkHistory> histories = new List<WorkHistory>();

            var reader = File.ReadAllLines(FilePath);

            if (reader.Any())
            {
                for (int i = 0; i < reader.Length; i++)
                {
                    var columns = reader[i].Split('#');
                    var h = new WorkHistory();
                    h.CandidateId = int.Parse(columns[0]);
                    h.EmployerName = columns[1];
                    h.City = columns[2];
                    h.State = columns[3];
                    h.StartDate = DateTime.Parse(columns[4]);
                    h.EndDate = DateTime.Parse(columns[5]);
                    h.Duties = columns[6];
                    h.SupervisorName = columns[7];
                    h.SupervisorPhone = columns[8];
                    h.CurrentlyWorking = bool.Parse(columns[9]);

                    if (h.EndDate.ToString() == "01/01/1900")
                    {
                        h.EndDate = null;
                    }

                    histories.Add(h);
                }
                return histories;
            }

            return histories;
        }

        public List<WorkHistory> GetAll()
        {
            return Histories;
        }

        public List<WorkHistory> GetByCandidateById(int id)
        {
            var returnList = new List<WorkHistory>();

            foreach (var h in Histories)
            {
                if (h.CandidateId == id)
                {
                    returnList.Add(h);
                }
            }
            return returnList;
        }

        public WorkHistory GetById(int id)
        {
            return Histories.FirstOrDefault(r => r.CandidateId == id);
        }

        public WorkHistory Create(WorkHistory model)
        {
            Histories.Add(model);
            Save();
            return model;
        }

        public void Update(int id, WorkHistory model)
        {
            throw new NotImplementedException();
            //need unique identifiers... otherwise this would overwrite all the eduhx
        }

        public void Delete(int id)
        {
            //foreach (var h in Histories)
            //{
            //    if (h.CandidateId == id)
            //    {
            //        Histories.Remove(h);
            //    }

            //}
            throw new NotImplementedException();
        }

        private void Save()
        {
            var writer = new StreamWriter(FilePath, false);

            using (writer)
            {
                foreach (var h in Histories)
                {
                    if (h.EndDate == null)
                    {
                        h.EndDate = DateTime.Parse("01/01/1900");
                    }

                    writer.WriteLine("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}#{9}", h.CandidateId, h.EmployerName, h.City,
                        h.State,
                        h.StartDate, h.EndDate, h.Duties, h.SupervisorName, h.SupervisorPhone, h.CurrentlyWorking);
                }
            }
        }
    }
}