﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Portal.Models;

namespace Portal.Data.FileRepos
{
    public class EducationHistoryFileRepo : IEducationHistoryRepository

    {
        private List<EducationHistory> Histories { get; set; }
        public string FilePath { get; set; }

        public EducationHistoryFileRepo()
        {
            FilePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["EducationHistories"]);
            Histories = LoadHistories();
        }

        private List<EducationHistory> LoadHistories()
        {
            List<EducationHistory> histories = new List<EducationHistory>();

            var reader = File.ReadAllLines(FilePath);

            for (int i = 0; i < reader.Length; i++)
            {
                var columns = reader[i].Split('#');

                var h = new EducationHistory();
                h.CandidateId = int.Parse(columns[0]);
                h.InstitutionName = columns[1];
                h.InstitutionCity = columns[2];
                h.InstitutionState = columns[3];
                h.DegreeEarned = columns[4];
                h.FieldOfStudy = columns[5];
                h.IsGraduated = bool.Parse(columns[6]);

                histories.Add(h);
            }
            return histories;
        }

        public List<EducationHistory> GetAll()
        {
            return Histories;
        }

        public List<EducationHistory> GetByCandidateById(int id)
        {
            return new List<EducationHistory>(Histories.Where(h=>h.CandidateId == id));
        }

        public EducationHistory GetById(int id)
        {
            return Histories.FirstOrDefault(r => r.CandidateId == id);
        }

        public EducationHistory Create(EducationHistory model)
        {
            Histories.Add(model);
            Save();
            return model;
        }

        public void Update(int id, EducationHistory model)
        {
            throw new NotImplementedException();
            //need unique identifiers... otherwise this would overwrite all the eduhx
        }

        public void Delete(int id)
        {
            //foreach (var h in Histories)
            //{
            //    if (h.CandidateId == id)
            //    {
            //        Histories.Remove(h);
            //    }

            //}
            throw new NotImplementedException();
        }

        private void Save()
        {
            var writer = new StreamWriter(FilePath, false);

            using (writer)
            {
                foreach (var h in Histories)
                    writer.WriteLine("{0}#{1}#{2}#{3}#{4}#{5}#{6}", h.CandidateId, h.InstitutionName, h.InstitutionCity, h.InstitutionState,
                        h.DegreeEarned, h.FieldOfStudy, h.IsGraduated);
            }
        }
    }
}
