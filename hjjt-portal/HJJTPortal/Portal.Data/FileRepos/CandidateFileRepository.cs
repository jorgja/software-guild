﻿using Portal.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Portal.Data.Interfaces;

namespace Portal.Data.FileRepos
{
    public class CandidateFileRepository : ICandidateRepository
    {
        private List<Candidate> Candidates { get; set; }
        public string FilePath { get; set; }

        public CandidateFileRepository()
        {
            FilePath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Cantidates"]);
            Candidates = LoadCandidates();
        }

        private List<Candidate> LoadCandidates()
        {
            List<Candidate> candidates = new List<Candidate>();

            var reader = File.ReadAllLines(FilePath);

            for (int i = 0; i < reader.Length; i++)
            {
                var columns = reader[i].Split('#');

                var c = new Candidate();
                c.CandidateId = int.Parse(columns[0]);
                c.FirstName = columns[1];
                c.LastName = columns[2];
                c.EMail = columns[3];
                c.PhoneNumber = columns[4];
                c.BirthDate = DateTime.Parse(columns[5]);
                c.Address = new Address()
                {
                    City = columns[6],
                    State = columns[7],
                    Street = columns[8],
                    Zipcode = columns[9]
                };
                c.CandidateStatus = (CandidateStatus)int.Parse(columns[10]);

                candidates.Add(c);
            }
            return candidates;
        }

        public List<Candidate> GetAll()
        {
            return Candidates;
        }

        public Candidate GetById(int id)
        {
            return Candidates.FirstOrDefault(r => r.CandidateId == id);
        }

        public Candidate AddCandidate(Candidate model)
        {
            var nextId = Candidates.Any() ? Candidates.Max(c => c.CandidateId) + 1 : 1;
            model.CandidateId = nextId;
            Candidates.Add(model);
            Save();
            return model;
        }

        private void Save()
        {
            var writer = new StreamWriter(FilePath, false);

            using (writer)
            {
                foreach (var c in Candidates)
                    writer.WriteLine("{0}#{1}#{2}#{3}#{4}#{5}#{6}#{7}#{8}#{9}#{10}", c.CandidateId, c.FirstName, c.LastName,
                        c.EMail, c.PhoneNumber, c.BirthDate.Date, c.Address.City, c.Address.State, c.Address.Street, c.Address.Zipcode, (int)c.CandidateStatus);
            }
        }

        public void Update(int id, Candidate model)
        {
            Candidates[id] = model;
            Save();
        }

        public void Delete(int id)
        {
            Candidates[id].CandidateStatus = CandidateStatus.InActive;
            Save();
        }
    }
}