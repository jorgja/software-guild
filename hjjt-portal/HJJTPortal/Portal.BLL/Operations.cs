﻿using Portal.BLL.Responses;
using Portal.Data;
using Portal.Data.Interfaces;
using Portal.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Portal.BLL
{
    public class Operations
    {
        private ICandidateRepository _candidateRepository { get; set; }
        private IEducationHistoryRepository _educationHistoryRepository { get; set; }
        private IWorkHistoryRepository _workHistoryRepository { get; set; }
        private IDesiredJobInfoRepository _desiredJobInfoRepository { get; set; }
        private IPolicyRepository _policyRepository { get; set; }
        private ICategoryRepository _categoryRepository { get; set; }

        public Operations()
        {
            _candidateRepository = RepoFactory.CreateCandidateRepository();
            _educationHistoryRepository = RepoFactory.CreateEducationHistoryRepository();
            _workHistoryRepository = RepoFactory.CreateWorkHistoryRepository();
            _desiredJobInfoRepository = RepoFactory.CreateDesiredJobInfoRepository();
            _policyRepository = RepoFactory.CreatePolicyRepository();
            _categoryRepository = RepoFactory.CreateCategoryRepository();
        }

        public CandidateResponse CreateCandidate(Candidate c)
        {
            CandidateResponse response = new CandidateResponse();
            response.Success = true;
            response.Message = "Your application has been created";
            _candidateRepository.AddCandidate(c);
            return response;
        }

        public void DeleteCandidate(Candidate c)
        {
            CandidateResponse response = new CandidateResponse();
            response.Success = true;
            response.Message = "You succesfully deleted that candidate";
            _candidateRepository.Delete(c.CandidateId);
        }

        public EducationHistoryResponse CreateEducationHistory(EducationHistory e)
        {
            EducationHistoryResponse response = new EducationHistoryResponse();
            response.Success = true;
            response.Message = "You succesfully submitted our Education History";
            _educationHistoryRepository.Create(e);
            return response;
        }

        public WorkHistoryResponse CreateWorkHistory(WorkHistory w)
        {
            WorkHistoryResponse response = new WorkHistoryResponse();
            response.Success = true;
            response.Message = "You succesfully submitted your Work History";
            _workHistoryRepository.Create(w);
            return response;
        }

        public void CreateDesiredJobInfo(DesiredJobInfo d)
        {
            DesiredJobInformationResponse response = new DesiredJobInformationResponse();
            response.Success = true;
            response.Message = "You succesfully submitted your Desired Job Information";
            _desiredJobInfoRepository.Create(d);
        }

        public List<EducationHistory> GetEducationHistoryByID(int id)
        {
            CandidateResponse response = new CandidateResponse();
            response.Success = true;
            response.Message = "Please see the listed candidate's input for Education History";
            return _educationHistoryRepository.GetByCandidateById(id);
        }

        public List<WorkHistory> GetHistoryByID(int id)
        {
            CandidateResponse response = new CandidateResponse();
            response.Success = true;
            response.Message = "Please see the listed candidate's input for Work History";
            return _workHistoryRepository.GetByCandidateById(id);
        }

        public DesiredJobInfo GetDesiredJobInfoByID(int id)
        {
            CandidateResponse response = new CandidateResponse();
            response.Success = true;
            response.Message = "Please see the listed candidate's input for Desired Job";
            return _desiredJobInfoRepository.GetById(id);
        }

        public Candidate GetCandidateByID(int id)
        {
            //CandidateResponse response = new CandidateResponse();
            //var application = GetCandidateByID(id);
            //if (application != null)
            //{
            //    response.Success = true;
            //    response.Message = "Candidate is found";
            //}
            //else
            //{
            //    response.Success = false;
            //    response.Message = "The candidate you were searching for was not found";
            //}

            CandidateResponse response = new CandidateResponse();
            response.Success = true;
            response.Message = "Candidate is found";

            return _candidateRepository.GetById(id);
        }

        public List<Policy> GetAllPolicies()
        {
            CandidateResponse response = new CandidateResponse();
            response.Success = true;
            response.Message = "You have successfully pulled all policies";
            return _policyRepository.GetAll();
        }

        public List<Category> GetAllCategories()
        {
            CandidateResponse response = new CandidateResponse();
            response.Success = true;
            response.Message = "Here are the policy categories";
            return _categoryRepository.GetAll();
        }

        public List<Candidate> GetAllCandidates()
        {
            CandidateResponse response = new CandidateResponse();
            response.Success = true;
            response.Message = "You have successfully pulled all candidates";
            return _candidateRepository.GetAll();
        }
    }
}