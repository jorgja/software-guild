using System.Collections.Generic;
using Portal.Models;

namespace Portal.BLL.Responses
{
    public class PoliciesResponse : Response
    {
        public List<Policy> Data { get; set; }
    }
}