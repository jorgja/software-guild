using Portal.Models;

namespace Portal.BLL.Responses
{
    public class CandidateResponse : Response
    {
        public Candidate Data { get; set; }
    }
}