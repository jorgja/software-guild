using System.Collections.Generic;
using Portal.Models;

namespace Portal.BLL.Responses
{
    public class CandidatesResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<Candidate> Candidates { get; set; }
    }
}