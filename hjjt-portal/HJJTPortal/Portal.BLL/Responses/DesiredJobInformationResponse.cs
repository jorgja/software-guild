using Portal.Models;

namespace Portal.BLL.Responses
{
    public class DesiredJobInformationResponse : Response
    {
        public DesiredJobInfo Data { get; set; }
    }
}