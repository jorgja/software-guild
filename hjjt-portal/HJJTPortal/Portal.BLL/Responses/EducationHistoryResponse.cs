using Portal.Models;

namespace Portal.BLL.Responses
{
    public class EducationHistoryResponse : Response
    {
        public EducationHistory Data { get; set; }
    }
}