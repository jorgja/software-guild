using Portal.Models;

namespace Portal.BLL.Responses
{
    public class WorkHistoryResponse : Response
    {
        public WorkHistory Data { get; set; }
    }
}