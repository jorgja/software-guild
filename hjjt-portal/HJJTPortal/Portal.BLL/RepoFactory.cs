using Portal.Data;
using Portal.Data.FileRepos;
using Portal.Data.Interfaces;
using Portal.Data.MockRepos;
using Portal.Data.Policies;
using System.Configuration;
using System.Web;

namespace Portal.BLL
{
    public class RepoFactory
    {
        public static ICandidateRepository CreateCandidateRepository()
        {
            string mode = ConfigurationManager.AppSettings["Mode"];

            if (mode.Contains("Test"))
            {
                return new CandidateMockRepository();
            }

            return new CandidateFileRepository();
        }

        public static IEducationHistoryRepository CreateEducationHistoryRepository()
        {
            string mode = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Mode"]);

            if (mode.Contains("Test"))
            {
                return new EducationHistoryMockRepository();
            }

            return new EducationHistoryFileRepo();
        }

        public static IWorkHistoryRepository CreateWorkHistoryRepository()
        {
            string mode = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Mode"]);

            if (mode.Contains("Test"))
            {
                return new WorkHistoryMockRepository();
            }

            return new WorkHistoryFileRepo();
        }

        public static IDesiredJobInfoRepository CreateDesiredJobInfoRepository()
        {
            string mode = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Mode"]);

            if (mode.Contains("Test"))
            {
                return new DesiredJobMockRepository();
            }

            return new DesiredJobInfoFileRepo();
        }

        public static IPolicyRepository CreatePolicyRepository()
        {
            string mode = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Mode"]);

            if (mode.Contains("Test"))
            {
                //  return new MockPolicyRepository();
            }

            return new PolicyRepository();
        }

        public static ICategoryRepository CreateCategoryRepository()
        {
            string mode = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["Mode"]);

            if (mode.Contains("Test"))
            {
                // return new MockCategoryRepository();
            }
            return new CategoryRepository();
        }
    }
}